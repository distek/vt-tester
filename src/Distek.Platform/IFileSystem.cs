﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Platform
{
   /// <summary>
   /// Abstraction interface for platform specific file management.
   /// </summary>
   public interface IFileSystem
   {
      /// <summary>
      /// Returns a listing of files for a given directory
      /// </summary>
      /// <param name="directory">Directory name to get a listing of files from</param>
      /// <returns></returns>
      IEnumerable<string> ListFiles(string directory);

      /// <summary>
      /// Reads a file for the given working set with the given name
      /// </summary>
      /// <param name="filename">File to read</param>
      /// <returns></returns>
      Stream ReadFile(string filename);

      /// <summary>
      /// Writes a file to disk.  Deletes existing file if it exists
      /// </summary>
      /// <param name="filename">File to write to</param>
      /// <param name="stream">Stream containing file data</param>
      void WriteFile(string filename, Stream stream);

      /// <summary>
      /// Appends to an existing file
      /// </summary>
      /// <param name="filename">File to write to</param>
      /// <param name="stream">Stream containing file data</param>
      void AppendFile(string filename, Stream stream);

      /// <summary>
      /// Deletes a file from the disk
      /// </summary>
      /// <param name="filename">File to delete</param>
      void DeleteFile(string filename);

      /// <summary>
      /// Determines whether or not a file exists
      /// </summary>
      /// <param name="filename">File to look for</param>
      /// <returns>TRUE if file exists</returns>
      bool FileExists(string filename);

      /// <summary>
      /// Creates a directory if one by the given name does not exist
      /// </summary>
      /// <param name="name"></param>
      void CreateDirectory(string name);

      /// <summary>
      /// Gets the newest file in the given directory
      /// </summary>
      /// <param name="directory"></param>
      /// <returns></returns>
      string Newest(string directory);

      /// <summary>
      /// Gets the full path of the given file name
      /// </summary>
      /// <param name="filename"></param>
      /// <returns></returns>
      string GetLocalPath(string filename);
   }
}
