﻿using Distek.Network;
using System;
using System.Collections.Generic;
using System.Text;

/*
   TODO: this is really messed up 
   1. it should be an interface
   2. it's not clear what the point of it is when it takes a CanMessage and returns a CanMessage
      - that part is related to package dependencies
      i'm trying to make this a general package, but the message reader is specific to the type of system
      we're implementing.  for a vt this will return an IsobusVtMessage
 */
namespace Distek.Platform
{
   /// <summary>
   /// Base class for message readers
   /// </summary>
   public interface IMessageReader
   {
      /// <summary>
      /// Convert a CAN message into an ISOBUS message
      /// </summary>
      /// <param name="message"></param>
      /// <returns></returns>
      CanMessage Read(CanMessage message);
   }
}
