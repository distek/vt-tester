﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Platform
{
   /// <summary>
   /// Contains platform specific configuration details
   /// </summary>
   public class PlatformConfig
   {
      /// <summary>
      /// Root path of the file system to place files.  Path must be readable
      /// and writable by the main process.
      /// </summary>
      public string BaseDirectory { get; set; }
   }
}
