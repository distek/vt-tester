﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Platform
{
   /// <summary>
   /// Base class for message handlers
   /// </summary>
   public abstract class MessageHandler
   {
      /// <summary>
      /// Filter to determine whether or not to execute this handler for a given message
      /// </summary>
      public virtual Func<CanMessage, bool> Filter { get; set; }

      /// <summary>
      /// Asynchronous handle to execute for the message
      /// </summary>
      public virtual Func<CanMessage, Task<IResponse>> AsyncHandler { get; set; }
   }
}
