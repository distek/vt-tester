﻿using Distek.Network;
using System;

namespace Distek.Platform
{
   /// <summary>
   /// Represents a connectionless session with a CAN interface
   /// </summary>
   public interface IConnection
   {
      /// <summary>
      /// The IConnection Send method sends a message on the currently connected <see cref="INetwork"/>
      /// </summary>
      /// <param name="message"></param>
      /// <returns></returns>
      bool Send(CanMessage message);

      /// <summary>
      /// The IConnection AddHandler method dynamically adds a new message handler for IConnect.Listen() to use
      /// </summary>
      /// <param name="handler">Message handler</param>
      /// <returns></returns>
      void AddHandler(MessageHandler handler);

      /// <summary>
      /// Used for other parts of the system to inject an CAN message into this connection
      /// </summary>
      /// <param name="message"></param>
      void Inject(CanMessage message);
    
      // Ideas here:
      // Any message that can be parsed as a type T will call x
      //void AddHandler<T>(Action<T> x);
      // Or, add handlers by PGN
      //void AddHandler<T>(int pgn, Action<T> x);
   }
}
