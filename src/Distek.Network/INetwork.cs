﻿using System;

namespace Distek.Network
{
   /// <summary>
   /// Represents a low level connection to a remote CAN interface
   /// </summary>
   public interface INetwork
   {
      /// <summary>
      /// Read-only key to select the current network on
      /// </summary>
      /// <example>'udp', 'valuecan3', 'pcan'</example>
      string Key { get; }

      /// <summary>
      /// Establishes a connection with the CAN network. 
      /// Use implementing class' constuctor to inject any dependencies (e.g. server IP address, port, etc) 
      /// </summary>
      void Connect();

      /// <summary>
      /// Transmits a CAN message over the network
      /// </summary>
      /// <param name="message"></param>
      void Send(CanMessage message);

      /// <summary>
      /// Receives messages over the network and executes a callback
      /// </summary>
      /// <param name="handler">Action to call when a message is received</param>
      void Receive(Action<CanMessage> handler);
   }
}
