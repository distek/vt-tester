﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Network
{
   /// <summary>
   /// Data object for transmitting and receiving CAN messages
   /// </summary>
   public class CanMessage
   {
      /// <summary>
      /// CAN Header
      /// </summary>
      public uint Header { get; set; }

      /// <summary>
      /// CAN packet data
      /// </summary>
      public byte[] Data { get; set; }

      /// <summary>
      /// Message source address
      /// </summary>
      public int SourceAddress
      {
         get { return (int)Header & 0x000000FF; }
         set { Header = Header | (uint)(value & 0x000000FF); }
      }

      /// <summary>
      /// Message parameter group number
      /// </summary>
      public int PGN
      {
         get
         {
            int pgn = ((int)Header & 0x03FFFF00) >> 8;

            if ((pgn & 0xFFFF) < 0xF000)
            {
               pgn &= 0x3FF00;
            }
            return pgn;
         }
      }

      /// <summary>
      /// Message priority
      /// </summary>
      public int Priority
      {
         get { return (int)(Header & 0x1C00); }
         set { Header = (uint)(Header | ((uint)value << 26)); }
      }

      /// <summary>
      /// Message extended data page
      /// </summary>
      public int ExtendedDataPage
      {
         get { return (int)(Header & 0x0200); }
         set { Header = (uint)(Header | (uint)value << 25); }
      }

      /// <summary>
      /// Message data page
      /// </summary>
      public int DataPage
      {
         get { return (int)(Header & 0x0100); }
         set { Header = (uint)(Header | (uint)value << 24); }
      }

      /// <summary>
      /// Message parameter group number
      /// </summary>
      public int ParameterGroupNumber
      {
         get { return (int)(Header & 0x00FFFF00); }
         set { Header = (uint)((Header & 0xFF00FFFF) | (uint)value << 8); }
      }

      /// <summary>
      /// Message destination address
      /// </summary>
      public int DestinationAddress
      {
         get
         {
            int pgn = (int)(Header & 0x03FFFF00) >> 8;
            int destination_address;
            if ((pgn & 0xFF00) < 0xF000)
            {
               destination_address = pgn & 0xFF;
            }
            else
            {
               destination_address = 0xFF;
            }
            return destination_address;
         }
         set { Header = Header | (uint)(value << 8); }  // todo  Don't set this if PGN is global...
      }

      /// <summary>
      /// CAN Name of the ECU that sent this message
      /// </summary>
      public string SourceCanName { get; set; }

      /// <summary>
      /// Default constructor for creating an empty CAN message
      /// </summary>
      public CanMessage()
      { }

      /// <summary>
      /// Creates a new CanMessage with a header value and empty data area
      /// </summary>
      /// <param name="header"></param>
      public CanMessage(uint header)
          : this(header, new byte[8])
      { }

      /// <summary>
      /// CanMessage constructor
      /// </summary>
      /// <param name="Header">32 bit header</param>
      /// <param name="Data"></param>
      public CanMessage(uint Header, byte[] Data)
      {
         this.Header = Header;
         this.Data = Data;
      }

      /// <summary>
      /// Converts a <see cref="CanMessage"/> to a human readable string for display
      /// </summary>
      /// <returns></returns>
      public override string ToString()
      {
         if (this.Data != null)
            return String.Format("0x{0:X}\t{1}\t{2}", this.Header, this.Data.Length, BitConverter.ToString(this.Data).Replace("-", " "));
         return string.Empty;
      }
   }

   /// <summary>
   /// Helper extension methods for dealing with CAN message data
   /// </summary>
   public static class CanMessageExtensions
   {
      /// <summary>
      /// Converts a CAN message object to an array
      /// </summary>
      /// <param name="msg"></param>
      /// <returns></returns>
      public static byte[] ToBytes(this CanMessage msg)
      {
         if (msg == null)
         {
            return null;
         }
         else
         {
            if (msg.Data.Length < 8)
               msg.Data = msg.Data.Take(msg.Data.Length).ToArray();
            // Manual conversion
            return new byte[]
            {
                    (byte)(msg.Header >> 24),
                    ((byte)((msg.Header >> 16) & 0xFF)),
                    ((byte)((msg.Header >> 8) & 0xFF)),
                    ((byte)(msg.Header & 0xFF)),
                    (byte)msg.Data.Length,
            }.Concat(msg.Data).ToArray();
         }
      }

      /// <summary>
      /// Converts an array of bytes to a CanMessage object
      /// </summary>
      /// <param name="bytes"></param>
      /// <returns></returns>
      public static CanMessage BytesToCanMessage(byte[] bytes)
      {
         // Need at least a 4 byte header plus 1 byte for DLC, which can be 0
         if (bytes.Length < 5)
            return default(CanMessage);

         // Grab header and data length code from byte array
         var header = ((uint)bytes[0] << 24) | ((uint)bytes[1] << 16) | ((uint)bytes[2] << 8) | (uint)bytes[3];
         var dlc = bytes[4];

         // Grab data from byte array
         var data = bytes.Skip(5).Take(dlc).ToArray();

         // return the CAN message
         return (new CanMessage(header, data));
      }
   }
}
