﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface IDeserialization
   {
      /// <summary>
      /// Reads an object pool from an IOP stream
      /// </summary>
      /// <param name="s"></param>
      /// <returns></returns>
      IEnumerable<IsoVtObject> DeserializeObjectPool(Stream s);
   }
}
