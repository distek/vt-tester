﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public enum DefaultTone
   {
      UnknownError,
      MissingWorkingSet,
      InvalidObjectPool,
   }

   public interface IAudio
   {
      /// <summary>
      /// Plays a built-in audio tone
      /// </summary>
      /// <param name="tone"></param>
      void PlayDefaultTone(DefaultTone tone);

      /// <summary>
      /// Gets or sets the current volume level for audio
      /// </summary>
      int Volume { get; set; }

      void PlayTone();
   }
}
