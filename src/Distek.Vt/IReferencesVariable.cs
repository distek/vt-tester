﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   /// <summary>
   /// Marker interface to signify an object references a variable object
   /// ie NumberVariable, StringVariable
   /// </summary>
   public interface IReferencesVariable
   {
      int VariableReference { get; set; }
   }
}
