﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.ObjectModel;

namespace Distek.Vt
{
   public class ObjectPool
   {
      public string WorkingSetMasterName { get; set; }
      public string VersionLabel { get; set; }
      public IList<IsoVtObject> Objects { get; set; }

      public ObjectPool()
      {
         Objects = new List<IsoVtObject>();
      }
   }
}
