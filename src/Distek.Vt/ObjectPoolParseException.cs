﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class ObjectPoolParseException : Exception
   {
      public int InvalidObjectId { get; set; }
      public IsobusObjectType InvalidObjectType { get; set; }

      public ObjectPoolParseException(string message) : base(message)
      { }
   }
}
