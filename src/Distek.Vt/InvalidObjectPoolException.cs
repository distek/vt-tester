﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class InvalidObjectPoolException : Exception
   {
      public InvalidObjectPoolException(string message) : base(message)
      { }
   }
}
