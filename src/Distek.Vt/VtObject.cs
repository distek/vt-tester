﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
	public abstract class VtObject
	{
		public int ObjectId { get; protected set; }

		public VtObject(int objectId)
		{
			ObjectId = objectId;
		}
	}

	public abstract class VtDrawableObject: VtObject
	{
		public int XLocation { get; set; }
		public int YLocation { get; set; }

		public VtDrawableObject(int objectId) : base(objectId)
		{}
	}
}
