﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt
{
    public class WorkingSetDisconnect
    {
      public string Type { get { return "working-set-disconnect"; } }
      public int MasterSourceAddress { get; set; }
    }
}
