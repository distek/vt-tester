﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;

// TODO: move this into its own project and create files for each command/response pair
namespace Distek.Vt
{
   // Annex D.3 get Memory Response
   [VtFunction(IsobusMessageId.GetMemory)]
   public class GetMemoryResponse : IsoVtMessage, IResponse
   {
      public GetMemoryResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }

      [ByteOrder(1, 1)]
      public int VersionNumber { get; set; }

      [ByteOrder(2, 1)]
      public int Status { get; set; }
   }

   // Annex D.5 Get Number of Soft Keys Response
   [VtFunction(IsobusMessageId.GetNumberOfSoftKeys)]
   public class GetNumberOfSoftKeysResponse : IsoVtMessage, IResponse
   {
      public GetNumberOfSoftKeysResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;

         Reserved = new byte[] { 0xFF, 0xFF };
      }

      [ByteOrder(1, 1)]
      public int NavigationSoftKeys { get; set; }

      [ByteOrder(2, 2)]
      public byte[] Reserved { get; private set; }

      [ByteOrder(4, 1)]
      public int XDots { get; set; }

      [ByteOrder(5, 1)]
      public int YDots { get; set; }

      [ByteOrder(6, 1)]
      public int VirtualSoftKeys { get; set; }

      [ByteOrder(7, 1)]
      public int PhysicalSoftKeys { get; set; }
   }

   // Annex D.6 Get Text Font Response
   [VtFunction(IsobusMessageId.GetTextFont)]
   public class GetTextFontResponse : IsoVtMessage, IResponse
   {
      public GetTextFontResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;

         Reserved = Enumerable.Repeat<byte>(0xFF, 4).ToArray();
      }

      [ByteOrder(1, 4)]
      public byte[] Reserved { get; private set; }

      [ByteOrder(5, 1)]
      public int SmallFontSizes { get; set; }

      [ByteOrder(6, 1)]
      public int LargeFontSizes { get; set; }

      [ByteOrder(7, 1)]
      public int TypeAttribute { get; set; }
   }

   // Annex D.9 Get Hardware Response
   [VtFunction(IsobusMessageId.GetHardware)]
   public class GetHardwareResponse : IsoVtMessage, IResponse
   {
      public GetHardwareResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }

      [ByteOrder(1, 1)]
      public int BootTime { get; set; }

      [ByteOrder(2, 1)]
      public int GraphicType { get; set; }

      [ByteOrder(3, 1)]
      public int Hardware { get; set; }

      [ByteOrder(4, 2)]
      public int XPixels { get; set; }

      [ByteOrder(6, 2)]
      public int YPixels { get; set; }
   }

   // Annex E.3 Get Versions Response
   [VtFunction(IsobusMessageId.GetVersionsResponse)]
   public class GetVersionsResponse : IsoVtMessage, IResponse
   {
      public GetVersionsResponse(int destination)
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
         VersionLabels = new List<byte[]>();
         DestinationAddress = destination;
      }

      [ByteOrder(1, 1)]
      public int VersionCount { get { return this.VersionLabels.Count(); } }

      [ByteOrder(2)]
      public IEnumerable<byte[]> VersionLabels { get; set; }
   }

   // Annex E.5 Store Version Response
   [VtFunction(IsobusMessageId.StoreVersion)]
   public class StoreVersionResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 4)]
      public byte[] Reserved {  get { return new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }; } }

      [ByteOrder(5, 1)]
      public StoreVersionError ErrorCode { get; set; }

      public StoreVersionResponse(int destination)
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
         DestinationAddress = destination;
      }
   }

   // Annex E.7 Load Version Response
   [VtFunction(IsobusMessageId.LoadVersion)]
   public class LoadVersionResponse : IsoVtMessage, IResponse
   {
      public LoadVersionResponse(int destination)
      {
         DestinationAddress = destination;
         Priority = 7;
         ParameterGroupNumber = 0xE600;
         Reserved = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF };
      }

      [ByteOrder(1, 4)]
      public byte[] Reserved { get; set; }

      [ByteOrder(5, 1)]
      public LoadVersionError ErrorCode { get; set; }
   }

   // Annex E.9 Delete Version Response
   [VtFunction(IsobusMessageId.DeleteVersion)]
   public class DeleteVersionResponse : IsoVtMessage, IResponse
   {
      public DeleteVersionResponse(int destination)
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
         DestinationAddress = destination;
         Reserved = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF };
      }

      [ByteOrder(1, 4)]
      public byte[] Reserved { get; set; }

      [ByteOrder(5, 1)]
      public DeleteVersionError ErrorCode { get; set; }
   }

   // Annex F.2 Hide/Show Object Command
   [VtFunction(IsobusMessageId.HideShowObject)]
   public class HideShowObjectCommand
   {
      [ByteOrder(3, 1)]
      public bool Hidden { get; set; }
   }

   // Annex F.3 Hide/Show Object Response
   [VtFunction(IsobusMessageId.HideShowObject)]
   public class HideShowObjectResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }

      [ByteOrder(3, 1)]
      public bool Hidden { get; set; }

      [ByteOrder(4, 1)]
      public HideShowObjectError ErrorCode { get; set; }

      public HideShowObjectResponse(int destination)
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
         DestinationAddress = destination;
      }
   }

   // Annex F.4 Enable/Disable Object Command
   [VtFunction(IsobusMessageId.EnableDisableObject)]
   public class EnableDisableObjectCommand
   {
      [ByteOrder(3, 1)]
      public int EnableOrDisable { get; set; }
   }

   // Annex F.5 Enable/Disable Object Response
   [VtFunction(IsobusMessageId.EnableDisableObject)]
   public class EnableDisableObjectResponse : IsoVtMessage, IResponse
   {
      public EnableDisableObjectResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }

      [ByteOrder(2, 1)]
      public int EnableOrDisable { get; set; }

      [ByteOrder(4, 1)]
      public EnableDisableObjectError ErrorCode { get; set; }
   }

   // Annex F.6 Select Input Object Command
   /*[VtFunction(IsobusMessageId.SelectInputObject)]
   public class SelectInputObjectCommand
   {
      [ByteOrder(3, 1)]
      public int Option { get; set; }
   }*/

   // Annex F.7 Select Input Object Response
   [VtFunction(IsobusMessageId.SelectInputObject)]
   public class SelectInputObjectResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public SelectInputObjectObjectResponses Response { get; set; }
      [ByteOrder(4, 1)]
      public SelectInputObjectError ErrorCode { get; set; }

      public SelectInputObjectResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.8 ESC Command
   /*[VtFunction(IsobusMessageId.Esc)]
   public class EscCommand
   {
   }*/

   // Annex F.9 ESC Response
   [VtFunction(IsobusMessageId.Esc)]
   public class EscResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public EscError ErrorCode { get; set; }
   }

   // Annex F.10 Control Audio Signal Command
   [VtFunction(IsobusMessageId.ControlAudioSignal)]
   public class ControlAudioSignalCommand
   {
      // TODO: this doesn't have an object id, might need special deserialization code
      [ByteOrder(1, 1)]
      public int Activations { get; set; }
      [ByteOrder(2, 2)]
      public int Frequency { get; set; }
      [ByteOrder(4, 2)]
      public int OnTimeDuration { get; set; }
      [ByteOrder(6, 2)]
      public int OffTimeDuration { get; set; }
   }

   // Annex F.11 Control Audio Signal Response
   [VtFunction(IsobusMessageId.ControlAudioSignal)]
   public class ControlAudioSignalResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 1)]
      public ControlAudioSignalError ErrorCode { get; set; }

      public ControlAudioSignalResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.12 Set Audio Volume Command
   [VtFunction(IsobusMessageId.SetAudioVolume)]
   public class SetAudioVolumeCommand
   {
      [ByteOrder(1, 1)]
      public int Percent { get; set; }
   }

   // Annex F.13 Set Audio Volume Response
   [VtFunction(IsobusMessageId.SetAudioVolume)]
   public class SetAudioVolumeResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 1)]
      public SetAudioVolumeError ErrorCode { get; set; }
   }

   // Annex F.14 Change Child Location Command
   [VtFunction(IsobusMessageId.ChangeChildLocation)]
   public class ChangeChildLocationCommand
   {
      [ByteOrder(3, 2)]
      public int ChildObjectId { get; set; }
      [ByteOrder(5, 1)]
      public int XLocation { get; set; }
      [ByteOrder(6, 1)]
      public int YLocation { get; set; }
   }

   // Annex F.15 Change Child Location Response
   [VtFunction(IsobusMessageId.ChangeChildLocation)]
   public class ChangeChildLocationResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ParentObjectId { get; set; }
      [ByteOrder(3, 2)]
      public int ChildObjectId { get; set; }
      [ByteOrder(5, 1)]
      public ChangeChildLocationPositionError ErrorCode { get; set; }

      public ChangeChildLocationResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.16 Change Child Position Command
   [VtFunction(IsobusMessageId.ChangeChildPosition)]
   public class ChangeChildPositionCommand
   {
      [ByteOrder(1, 2)]
      public int ParentObjectId { get; set; }
      [ByteOrder(3, 2)]
      public int ChildObjectId { get; set; }
      [ByteOrder(5, 2)]
      public int XPosition { get; set; }
      [ByteOrder(7, 2)]
      public int YPosition { get; set; }
   }

   // Annex F.17 Change Child Position Response
   [VtFunction(IsobusMessageId.ChangeChildPosition)]
   public class ChangeChildPositionResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ParentObjectId { get; set; }
      [ByteOrder(3, 2)]
      public int ChildObjectId { get; set; }
      [ByteOrder(5, 1)]
      public ChangeChildLocationPositionError ErrorCode { get; set; }
      [ByteOrder(6, 2)]
      public byte[] Reserved { get { return new byte[] { 0xFF, 0xFF }; } }

      public ChangeChildPositionResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.18 Change Size Command
   [VtFunction(IsobusMessageId.ChangeSize)]
   public class ChangeSizeCommand
   {
      [ByteOrder(3, 2)]
      public int Width { get; set; }
      [ByteOrder(5, 2)]
      public int Height { get; set; }
   }

   // Annex F.19 Change Size Response
   [VtFunction(IsobusMessageId.ChangeSize)]
   public class ChangeSizeResponse : IsoVtMessage, IResponse
   {
      public ChangeSizeResponse()
      {
         // TODO: faking the response address
         //DestinationAddress = 0x89;
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public ChangeSizeError ErrorCode { get; set; }
      [ByteOrder(4, 4)]
      public byte[] Reserved { get { return new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }; }  }
   }

   // Annex F.20 Change Background Colour Command
   [VtFunction(IsobusMessageId.ChangeBackgroundColour)]
   public class ChangeBackgroundColourCommand
   {
      [ByteOrder(3, 1)]
      public IsoColour BackgroundColour { get; set; }
   }

   // Annex F.21 Change Background Colour Response
   [VtFunction(IsobusMessageId.ChangeBackgroundColour)]
   public class ChangeBackgroundColourResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public IsoColour BackgroundColour { get; set; }
      [ByteOrder(4, 1)]
      public ChangeBackgroundColourError ErrorCode { get; set; }
   }


   // Annex F.23 Change Numeric Value Response
   [VtFunction(IsobusMessageId.ChangeNumericValue)]
   public class ChangeNumericValueResponse : IsoVtMessage, IResponse
   {
      public ChangeNumericValueResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600; 
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }

      [ByteOrder(3, 1)]
      public ChangeNumericValueError ErrorCode { get; set; }

      [ByteOrder(4, 4)]
      public int Value { get; set; }
   }


   // Annex F.25 Change String Value Response
   [VtFunction(IsobusMessageId.ChangeStringValue)]
   public class ChangeStringValueResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public byte[] Reserved {  get { return new byte[] { 0xFF, 0xFF }; } }
      [ByteOrder(3, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(5, 1)]
      public ChangeStringValueError ErrorCode { get; set; }

      public ChangeStringValueResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.26 Change End Point Command
   public class ChangeEndPointCommand
   {
      public int Width { get; set; }
      public int Height { get; set; }
      public int LineDirection { get; set; }
   }

   // Annex F.27 Change End Point Response
   [VtFunction(IsobusMessageId.ChangeEndPoint)]
   public class ChangeEndPointResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }

      [ByteOrder(3, 1)]
      public ChangeEndPointError ErrorCode { get; set; }
   }

   // Annex F.28 Change Font Attributes Command
   public class ChangeFontAttributesCommand
   {
      public IsoColour FontColour { get; set; }
      public int FontSize { get; set; }
      public int FontType { get; set; }
      public int FontStyle { get; set; }
   }

   // Annex F.29 Change Font Attributes Response
   [VtFunction(IsobusMessageId.ChangeFontAttributes)]
   public class ChangeFontAttributesResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public ChangeFontAttributesError ErrorCode { get; set; }
   }

   // Annex F.30 Change Line Attributes Command
   public class ChangeLineAttributesCommand
   {
      public IsoColour LineColour { get; set; }
      public int LineWidth { get; set; }
      public int LineArt { get; set; }
   }

   // Annex F.31 Change Line Attributes Response
   [VtFunction(IsobusMessageId.ChangeLineAttributes)]
   public class ChangeLineAttributesResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public ChangeLineAttributesError ErrorCode { get; set; }
   }

   // Annex F.32 Change Fill Attributes Command
   public class ChangeFillAttributesCommand
   {
      public int FillType { get; set; }
      public IsoColour FillColour { get; set; }
      public int FillPattern { get; set; }
   }

   // Annex F.33 Change Fill Attributes Response
   [VtFunction(IsobusMessageId.ChangeFillAttributes)]
   public class ChangeFillAttributesResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public ChangeFillAttributesError ErrorCode { get; set; }
   }

   // Annex F.35 Change Active Mask Response
   [VtFunction(IsobusMessageId.ChangeActiveMask)]
   public class ChangeActiveMaskResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ActiveMaskObjectId { get; set; }
      [ByteOrder(3, 1)]
      public ChangeActiveMaskError ErrorCode { get; set; }

      public ChangeActiveMaskResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.36 Change Soft Key Mask Command
   public class ChangeSoftKeyMaskCommand
   {
      public int SoftKeyMaskObjectId { get; set; }
   }

   // Annex F.37 Change Soft Key Mask Response
   [VtFunction(IsobusMessageId.ChangeSoftKeyMask)]
   public class ChangeSoftKeyMaskResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int DataAlarmMaskObjectId { get; set; }
      [ByteOrder(3, 2)]
      public int SoftKeyMaskObjectId { get; set; }
      [ByteOrder(5, 1)]
      public ChangeSoftKeyMaskError ErrorCode { get; set; }

      public ChangeSoftKeyMaskResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.38 Change Attribute Command
   public class ChangeAttributeCommand
   {
      public int AttributeId { get; set; }
      public byte[] Value { get; set; }
   }

   // Annex F.39 Change Attribute Response
   [VtFunction(IsobusMessageId.ChangeAttribute)]
   public class ChangeAttributeResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int AttributeId { get; set; }
      [ByteOrder(4, 1)]
      public ChangeAttributeError ErrorCode { get; set; }

      public ChangeAttributeResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.40 Change Priority Command
   public class ChangePriorityCommand
   {
      public int Priority { get; set; }
   }

   // Annex F.41 Change Priority Response
   [VtFunction(IsobusMessageId.ChangePriority)]
   public class ChangePriorityResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      new public int Priority { get; set; }
      [ByteOrder(4, 1)]
      public ChangePriorityError ErrorCode { get; set; }
   }

   // Annex F.42 Change List Item Command
   public class ChangeListItemCommand
   {
      public int ListIndex { get; set; }
      public int NewObjectId { get; set; }
   }

   // Annex F.43 Change List Item Response
   [VtFunction(IsobusMessageId.ChangeListItem)]
   public class ChangeListItemResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int ListIndex { get; set; }
      [ByteOrder(4, 2)]
      public int NewObjectId { get; set; }
      [ByteOrder(6, 1)]
      public ChangeListItemError ErrorCode { get; set; }

      public ChangeListItemResponse()
      {
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }
   }

   // Annex F.44 Delete Object Pool Command
   [VtFunction(IsobusMessageId.DeleteObjectPool)]
   public class DeleteObjectPoolCommand
   {
   }

   // Annex F.45 Delete Object Pool Response
   [VtFunction(IsobusMessageId.DeleteObjectPool)]
   public class DeleteObjectPoolResponse : IsoVtMessage, IResponse
   {
      public DeleteObjectPoolResponse(int destination)
      {
         DestinationAddress = destination;
         Priority = 7;
         ParameterGroupNumber = 0xE600;
      }

      [ByteOrder(1, 1)]
      public DeleteObjectPoolError ErrorCode { get; set; }
      [ByteOrder(2, 6)]
      public byte[] Reserved { get { return new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; } }
   }

   // Annex F.46 Lock/Unlock Mask Command
   public class LockUnlockMaskCommand
   {
      public int LockOrUnlock { get; set; }
      public int LockTimeout { get; set; }
   }

   // Annex F.47 Lock/Unlock Mask Response
   [VtFunction(IsobusMessageId.LockUnlockMask)]
   public class LockUnlockMaskResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 1)]
      public int LockOrUnlock { get; set; }
      [ByteOrder(2, 1)]
      public LockUnlockMaskError ErrorCode { get; set; }
   }

   // Annex F.48 Execute Macro Command
   public class ExecuteMacroCommand
   {
   }

   // Annex F.49 Execute Macro Response
   [VtFunction(IsobusMessageId.ExecuteMacro)]
   public class ExecuteMacroResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 1)]
      public int ObjectId { get; set; }
      [ByteOrder(2, 1)]
      public ExecuteMacroError ErrorCode { get; set; }
   }

   // Annex F.50 Change Object Label Command
   public class ChangeObjectLabelCommand
   {
      public int StringVariableObjectId { get; set; }
      public int FontType { get; set; }
      public int GraphicObjectId { get; set; }
   }

   // Annex F.51 Change Object Label Response
   [VtFunction(IsobusMessageId.ChangeObjectLabel)]
   public class ChangeObjectLabelResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 1)]
      public ChangeObjectLabelError ErrorCode { get; set; }
   }

   // Annex F.52 Change Polygon Point Command
   public class ChangePolygonPointCommand
   {
      public int PointIndex { get; set; }
      public int XLocation { get; set; }
      public int YLocation { get; set; }
   }

   // Annex F.53 Change Polygon Point Response
   [VtFunction(IsobusMessageId.ChangePolygonPoint)]
   public class ChangePolygonPointResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public ChangePolygonPointError ErrorCode { get; set; }
   }

   // Annex F.54 Change Polygon Scale Command
   public class ChangePolygonScaleCommand
   {
      public int WidthAttribute { get; set; }
      public int HeightAttribute { get; set; }
   }

   // Annex F.55 Change Polygon Scale Response
   [VtFunction(IsobusMessageId.ChangePolygonScale)]
   public class ChangePolygonScaleResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 2)]
      public int WidthAttribute { get; set; }
      [ByteOrder(5, 2)]
      public int HeightAttribute { get; set; }
      [ByteOrder(7, 1)]
      public ChangePolygonScaleError ErrorCode { get; set; }
   }

   // Annex F.56 Graphics Context Command
   public class GraphicsContextCommand
   {
      public int SumCommandId { get; set; }
      public int Parameters { get; set; }
   }

   // Annex F.57 Graphics Context Response
   [VtFunction(IsobusMessageId.GraphicsContext)]
   public class GraphicsContextResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int SubCommandId { get; set; }
      [ByteOrder(4, 1)]
      public GraphicsContextError ErrorCode { get; set; }
   }

   // Annex F.58 Get Attribute Value Message
   public class GetAttributeValueMessage
   {
      public int AttributeId { get; set; }
   }

   // Annex F.59 Get Attribute Value Response
   [VtFunction(IsobusMessageId.GetAttributeValue)]
   public class GetAttributeValueResponse : IsoVtMessage, IResponse
   {
      public int ObjectId { get; set; }
      public int AttributeId { get; set; }
      public int Value { get; set; }
      public GetAttributeValueError ErrorCode { get; set; }

      //public override byte[] Serialize()
      //{
      //   var byteList = new List<byte>();
      //   // TODO: This is a v4 message and needs custom serialization
      //   // why does this need custom serialization

      //   return byteList.ToArray();
      //}
   }

   // Annex F.60 Select Colour Map Command
   public class SelectColourMapCommand
   {
   }

   // Annex F.61 Select Colour Map Response
   [VtFunction(IsobusMessageId.SelectColourMap)]
   public class SelectColourMapResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public SelectColourMapError ErrorCode { get; set; }
   }

   // Annex F.62 Identify VT Message
   public class IdentifyVTMessage
   {
   }

   // Annex F.63 Identify VT Response
   [VtFunction(IsobusMessageId.IdentifyVt)]
   public class IdentifyVTResponse : IsoVtMessage, IResponse
   {
   }

   // Annex F.64 Execute Extended Macro Command
   public class ExecuteExtendedMacroCommand
   {
   }

   // Annex F.65 Execute Extended Macro Response
   [VtFunction(IsobusMessageId.ExecuteExtendedMacro)]
   public class ExecuteExtendedMacroResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      // i don't see this in the spec, but ???
      [ByteOrder(3, 1)]
      public ExecuteExtendedMacroError ErrorCode { get; set; }
   }

   // Annex F.66 Unsupported VT Function Message
   public class UnsupportedVTFunctionMessage
   {
      public int UnsupportedVTFunction { get; set; }
   }

   // Annex F.67 VT Unsupported VT Function Message
   public class VTUnsupportedVTFunctionMessage
   {
      public int UnsupportedVTFunction { get; set; }
   }

   /*[VtFunction(IsobusMessageId.VtChangeNumericValue)]
   // Annex H.12 VT Change Numeric Value Command
   public class VTChangeNumericValueCommand : IsoVtMessage
   {
      public VTChangeNumericValueCommand(int destination)
      {
         Priority = 8;
         ParameterGroupNumber = 0xE600;
         DestinationAddress = destination;
      }
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int Reserved { get { return 0xFF; } }
      [ByteOrder(4, 4)]
      public int Value { get; set; }
   }*/

   // Acknowledge Message
   public class Acknowledge : CanMessage, IResponse
   {
      public Acknowledge()
      {
         Priority = 6;
         ParameterGroupNumber = 0xE800;
      }

      [ByteOrder(0, 1)]
      public Acknowledegments ControlByte { get; set; }

      [ByteOrder(1, 1)]
      public int GroupFunctionValue { get; set; }

      [ByteOrder(2, 2)]
      public byte[] Reserved { get { return new byte[] { 0xFF, 0xFF }; } }

      [ByteOrder(4, 1)]
      public int AddressAcknowledgement { get; set; }

      [ByteOrder(5, 3)]
      public int RequestingPGN { get; set; }
   }
}