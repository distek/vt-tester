﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace Distek.Vt
{
   /// <summary>
   /// Main control point for the VT
   /// </summary>
   public class VirtualTerminal : INotifyPropertyChanged
   {
      /// <summary>
      /// Size of the mask to display - width and height are always the same
      /// </summary>
      public int MaskSize { get; private set; }

      public ObservableCollection<VtClient> ConnectedClients { get; set; }

      private VtClient _currentClient;
      public VtClient CurrentClient
      {
         get { return _currentClient; }
         set { _currentClient = value; OnPropertyChanged(nameof(CurrentClient)); }
      }

      public VtBusyCode Status { get; set; }

      public IList<byte> TemporaryObjectPoolData { get; set; }

      public VirtualTerminal()
      {
         Status = VtBusyCode.NotBusy;
         ConnectedClients = new ObservableCollection<VtClient>();
         TemporaryObjectPoolData = new List<byte>();
         //MaskSize = 700;
         MaskSize = 480;
         CurrentClient = new VtClient(0)
         {
            MasterSourceAddress = 0xFF,
            //ActiveMaskId = 1000,
            //ActiveSoftKeyMaskId = 2000
         };
      }

      public event PropertyChangedEventHandler PropertyChanged;
      protected void OnPropertyChanged(string name)
      {
         PropertyChangedEventHandler handler = PropertyChanged;
         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(name));
         }
      }
   }
}
