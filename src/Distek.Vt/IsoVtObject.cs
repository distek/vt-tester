﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public abstract class IsoVtObject : IsoObject, INotifyPropertyChanged
   {
      public int ObjectId { get; protected set; }

      public IsoVtObject(int objectId)
      {
         ObjectId = objectId;
      }

      public event PropertyChangedEventHandler PropertyChanged;

      public abstract IsoVtObject Read(BinaryReader reader, int objectId);

      public abstract void Write(BinaryWriter writer);

      protected void OnPropertyChanged(string name)
      {
         PropertyChangedEventHandler handler = PropertyChanged;
         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(name));
         }
      }
   }
}
