﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.20 Change Background Colour Command
    [VtFunction(IsobusMessageId.ChangeBackgroundColour)]
    public class ChangeBackgroundColour : IsoVtMessage
    {
        [ByteOrder(1, 2)]
        public int ObjectId { get; set; }
        [ByteOrder(3, 1)]
        public IsoColour BackgroundColour { get; set; }

        public ChangeBackgroundColour()
        {
            CommandId = IsobusMessageId.ChangeBackgroundColour;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeBackgroundColourReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var bgColour = new IsoColour(m.Data[3]);

            var message = new ChangeBackgroundColour() { ObjectId = objectId, BackgroundColour = bgColour};

            return message;
        }
    }
}
