﻿using Distek.Network;
using Distek.Platform;
using System;

namespace Distek.Vt.Commands
{
   // Annex F.22 Change Numeric Value Command
   [VtFunction(IsobusMessageId.ChangeNumericValue)]
   public class ChangeNumericValue : IsoVtMessage
   {
      //public ChangeNumericValue(int destination)
      public ChangeNumericValue()
      {
         CommandId = IsobusMessageId.ChangeNumericValue;
         Priority = 8;
         ParameterGroupNumber = 0xE700;
         //DestinationAddress = destination;
         DestinationAddress = 0xFF;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int Reserved { get { return 0xFF; } }
      [ByteOrder(4, 4)]
      public int Value { get; set; }
   }

   public class ChangeNumericValueCommandReader : IMessageReader
   {
      public const IsobusMessageId MessageId = IsobusMessageId.ChangeNumericValue;

      public CanMessage Read(CanMessage m)
      {
         // Extract the values from the raw bytes here - see the standard for byte meanings
         var changedObject = BitConverter.ToUInt16(m.Data, 1);

         // todo: are values signed or unsigned?  i think they're unsigned
         var newValue = BitConverter.ToInt32(m.Data, 4);

         // Create the specific message type to send to the vt objects
         //var message = new ChangeNumericValue(m.SourceAddress) { Value = newValue };
         // will need working set id
         var message = new ChangeNumericValue() { Value = newValue, ObjectId = changedObject };
         return message;
      }
   }
}
