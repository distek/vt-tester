﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   // Annex F.10 Control Audio Signal Command
   [VtFunction(IsobusMessageId.ControlAudioSignal)]
   public class ControlAudioSignal : IsoVtMessage
    {
        [ByteOrder(1,1)]
        public int Activations { get; set; }
        [ByteOrder(2,2)]
        public int Frequency { get; set; }
        [ByteOrder(4,2)]
        public int OnTimeDuration { get; set; }
        [ByteOrder(6,2)]
        public int OffTimeDuration { get; set; }

        public ControlAudioSignal()
        {
            CommandId = IsobusMessageId.ControlAudioSignal;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ControlAudioSignalReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var activations = m.Data[1];
            var frequency = BitConverter.ToUInt16(m.Data, 2);
            var onTimeDuration = BitConverter.ToUInt16(m.Data, 4);
            var offTimeDuration = BitConverter.ToUInt16(m.Data, 6);

            var message = new ControlAudioSignal() { Activations = activations, Frequency = frequency, OnTimeDuration = onTimeDuration, OffTimeDuration = offTimeDuration };

            return message;
        }
    }
}
