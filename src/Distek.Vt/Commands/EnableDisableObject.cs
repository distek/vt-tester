﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.4 Enable Disable Object Command
    [VtFunction(IsobusMessageId.EnableDisableObject)]
    public class EnableDisableObject : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public int EnableOrDisable { get; set; }

        public EnableDisableObject()
        {
            CommandId = IsobusMessageId.EnableDisableObject;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class EnableDisableObjectReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var enableOrDisable = m.Data[3];

            var message = new EnableDisableObject() { ObjectId = objectId, EnableOrDisable = enableOrDisable };

            return message;
        }
    }
}
