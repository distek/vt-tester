﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   // Annex F.36 Change Soft Key Mask Command
   [VtFunction(IsobusMessageId.ChangeSoftKeyMask)]
   public class ChangeSoftKeyMask : IsoVtMessage
   {
      [ByteOrder(1,1)]
      public int MaskType { get; set; }
      [ByteOrder(2,2)]
      public int DataAlarmMaskObjectId { get; set; }
      [ByteOrder(4,2)]
      public int SoftKeyMaskObjectId { get; set; }

      public ChangeSoftKeyMask()
      {
         CommandId = IsobusMessageId.ChangeSoftKeyMask;
         Priority = 8;
         ParameterGroupNumber = 0xE700;
         DestinationAddress = 0xFF;
      }
   }
   public class ChangeSoftKeyMaskReader : IMessageReader
   {
      public CanMessage Read(CanMessage m)
      {
         var maskType = m.Data[1];
         var dataAlarmMaskObjectId = BitConverter.ToUInt16(m.Data, 2);
         var softKeyMaskObjectId = BitConverter.ToUInt16(m.Data, 4);

         var message = new ChangeSoftKeyMask() { MaskType = maskType, DataAlarmMaskObjectId = dataAlarmMaskObjectId, SoftKeyMaskObjectId = softKeyMaskObjectId };

         return message;
      }
   }
}
