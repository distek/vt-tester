﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   // Annex F.24 Change String Value Command
   [VtFunction(IsobusMessageId.ChangeStringValue)]
   public class ChangeStringValue : IsoVtMessage
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int TotalBytes { get; set; }
      [ByteOrder(4)]
      public string Value { get; set; }

      public ChangeStringValue()
      {
         CommandId = IsobusMessageId.ChangeStringValue;
         Priority = 8;
         ParameterGroupNumber = 0xE700;
         //DestinationAddress = destination;
         DestinationAddress = 0xFF;
      }
   }

   public class ChangeStringValueReader : IMessageReader
   {
      public CanMessage Read(CanMessage m)
      {
         var objectId = BitConverter.ToUInt16(m.Data, 1);
         var byteCount = m.Data[3];
         var val = Encoding.UTF8.GetString(m.Data, 5, byteCount);
         Console.WriteLine("string value change: {0}", val);

         var message = new ChangeStringValue() { Value = val, TotalBytes = byteCount, ObjectId = objectId };
         return message;
      }
   }
}
