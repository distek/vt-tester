﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.28 Change Font Attributes Command
    [VtFunction(IsobusMessageId.ChangeFontAttributes)]
    public class ChangeFontAttributes : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public IsoColour FontColour { get; set; }
        [ByteOrder(4,1)]
        public int FontSize { get; set; }
        [ByteOrder(5,1)]
        public int FontType { get; set; }
        [ByteOrder(6,1)]
        public FontStyle FontStyle { get; set; }

        public ChangeFontAttributes()
        {
            CommandId = IsobusMessageId.ChangeFontAttributes;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeFontAttributesReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var fontColour = new IsoColour(m.Data[3]);
            var fontSize = m.Data[4];
            var fontType = m.Data[5];
            var fontStyle = (FontStyle)m.Data[6];

            var message = new ChangeFontAttributes() { ObjectId = objectId, FontColour = fontColour, FontSize = fontSize, FontType = fontType, FontStyle = fontStyle};

            return message;
        }
    }
}
