﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.18 Change Size Command
    [VtFunction(IsobusMessageId.ChangeSize)]
    public class ChangeSize : IsoVtMessage
    {
        [ByteOrder(1, 2)]
        public int ObjectId { get; set; }
        [ByteOrder(3, 2)]
        public int Width { get; set; }
        [ByteOrder(5, 2)]
        public int Height { get; set; }

        public ChangeSize()
        {
            CommandId = IsobusMessageId.ChangeSize;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeSizeReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var width = BitConverter.ToUInt16(m.Data, 3);
            var height = BitConverter.ToUInt16(m.Data, 5);

            var message = new ChangeSize() { ObjectId = objectId, Width = width, Height = height};

            return message;
        }
    }
}
