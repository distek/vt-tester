﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.26 Change End Point Command
    [VtFunction(IsobusMessageId.ChangeEndPoint)]
    public class ChangeEndPoint : IsoVtMessage
    {
        [ByteOrder(1, 2)]
        public int ObjectId { get; set; }
        [ByteOrder(3, 2)]
        public int Width { get; set; } 
        [ByteOrder(5, 2)]
        public int Height { get; set; }
        [ByteOrder(7, 1)]
        public int LineDirection { get; set; }

        public ChangeEndPoint()
        {
            CommandId = IsobusMessageId.ChangeEndPoint;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeEndPointReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var width = BitConverter.ToUInt16(m.Data, 3);
            var height = BitConverter.ToUInt16(m.Data, 5);
            var lineDir = m.Data[7];

            var message = new ChangeEndPoint() { ObjectId = objectId, Width = width, Height = height, LineDirection = lineDir };

            return message;
        }
    }
}
