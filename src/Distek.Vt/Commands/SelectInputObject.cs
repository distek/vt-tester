﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.6 Select Input Object Command
    [VtFunction(IsobusMessageId.SelectInputObject)]
    public class SelectInputObject : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public int Option { get; set; }

        public SelectInputObject()
        {
            CommandId = IsobusMessageId.SelectInputObject;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class SelectInputObjectReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var option = m.Data[3];

            var message = new SelectInputObject() { ObjectId = objectId, Option = option };

            return message;
        }
    }
}
