﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   // Annex F.38 Change Attribute Command
   [VtFunction(IsobusMessageId.ChangeAttribute)]
   public class ChangeAttribute : IsoVtMessage
   {
      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int AttributeId { get; set; }
      [ByteOrder(4, 4)]
      public AttributeUnion Value { get; set; }

      public ChangeAttribute()
      {
         CommandId = IsobusMessageId.ChangeAttribute;
         Priority = 8;
         ParameterGroupNumber = 0xE700;
         DestinationAddress = 0xFF;
      }
   }

   public class AttributeUnion
   {
      public float FloatValue { get { return BitConverter.ToSingle(_data, 0); } }
      public byte Uint8Value { get { return _data[0]; } }
      public UInt16 Uint16Value { get { return BitConverter.ToUInt16(_data, 0); } }
      public UInt32 Uint32Value { get { return BitConverter.ToUInt32(_data, 0); } }
      public int Int32Value { get { return BitConverter.ToInt32(_data, 0); } }

      public AttributeUnion(byte[] data)
      {
         _data = data;
      }
      private byte[] _data;
   }

   public class ChangeAttributeReader : IMessageReader
   {
      public CanMessage Read(CanMessage m)
      {
         var objectId = BitConverter.ToUInt16(m.Data, 1);
         var attributeId = m.Data[3];
         var value = m.Data.Skip(4).Take(4).ToArray();
         var message = new ChangeAttribute()
         {
            ObjectId = objectId,
            AttributeId = attributeId,
            Value = new AttributeUnion(value),
         };

         return message;
      }
   }
}
