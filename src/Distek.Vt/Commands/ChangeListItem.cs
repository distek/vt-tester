﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.42 Change List Item Command
    [VtFunction(IsobusMessageId.ChangeListItem)]
    public class ChangeListItem : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public int ListIndex { get; set; }
        [ByteOrder(4,2)]
        public int NewObjectId { get; set; }

        public ChangeListItem()
        {
            CommandId = IsobusMessageId.ChangeListItem;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeListItemReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var listIndex = m.Data[3];
            var newObjectId = BitConverter.ToUInt16(m.Data, 4);

            var message = new ChangeListItem() { ObjectId = objectId, ListIndex = listIndex, NewObjectId = newObjectId };

            return message;
        }
    }
}
