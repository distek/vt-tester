﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.12 Set Audio Volume Command
    [VtFunction(IsobusMessageId.SetAudioVolume)]
    public class SetAudioVolume : IsoVtMessage
    {
        [ByteOrder(1,1)]
        public int Percent { get; set; }

        public SetAudioVolume()
        {
            CommandId = IsobusMessageId.SetAudioVolume;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class SetAudioVolumeReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var percent = m.Data[1];

            var message = new SetAudioVolume() { Percent = percent };

            return message;
        }
    }
}
