﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.40 Change Priority Command
    [VtFunction(IsobusMessageId.ChangePriority)]
    public class ChangePriority : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public int NewPriority { get; set; }

        public ChangePriority()
        {
            CommandId = IsobusMessageId.ChangePriority;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangePriorityReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var newPriority = m.Data[3];

            var message = new ChangePriority() { ObjectId = objectId, NewPriority = newPriority };

            return message;
        }
    }
}
