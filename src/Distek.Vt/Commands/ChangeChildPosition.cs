﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.16 Change Child Position Command
    [VtFunction(IsobusMessageId.ChangeChildPosition)]
    public class ChangeChildPosition : IsoVtMessage
    {
        [ByteOrder(1, 2)]
        public int ParentObjectId { get; set; }
        [ByteOrder(3, 2)]
        public int ChildObjectId { get; set; }
        [ByteOrder(5, 2)]
        public int XPosition { get; set; }
        [ByteOrder(7, 2)]
        public int YPosition { get; set; }

        public ChangeChildPosition()
        {
            CommandId = IsobusMessageId.ChangeChildPosition;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
        }
    }

    public class ChangeChildPositionReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var parentObjectId = BitConverter.ToUInt16(m.Data, 1);
            var childObjectId = BitConverter.ToUInt16(m.Data, 3);
            var xPosition = BitConverter.ToUInt16(m.Data, 5);
            var yPosition = BitConverter.ToUInt16(m.Data, 7);

            var message = new ChangeChildPosition() { ParentObjectId = parentObjectId, ChildObjectId = childObjectId, XPosition = xPosition, YPosition = yPosition };

            return message;
        }
    }
}
