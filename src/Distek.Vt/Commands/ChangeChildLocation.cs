﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   [VtFunction(IsobusMessageId.ChangeChildPosition)]
   public class ChangeChildLocation : IsoVtMessage
   {
      [ByteOrder(1,2)]
      public int ParentObjectId { get; set; }
      [ByteOrder(3,2)]
      public int ChildObjectId { get; set; }
      [ByteOrder(5,1)]
      public int XLocation { get; set; }
      [ByteOrder(6,1)]
      public int YLocation { get; set; }

      public ChangeChildLocation()
      {
         CommandId = IsobusMessageId.ChangeChildLocation;
         Priority = 8;
         ParameterGroupNumber = 0xE700;
      }
   }

   public class ChangeChildLocationReader : IMessageReader
   {
      public CanMessage Read(CanMessage m)
      {
         var parentObjectId = BitConverter.ToUInt16(m.Data, 1);
         var childObjectId = BitConverter.ToUInt16(m.Data, 3);
         var xLocation = m.Data[5];
         var yLocation = m.Data[6];

         var message = new ChangeChildLocation() { ParentObjectId = parentObjectId, ChildObjectId = childObjectId, XLocation = xLocation, YLocation = yLocation };

         return message;
      }
   }
}
