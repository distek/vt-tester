﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.30 Change Line Attributes Command
    [VtFunction(IsobusMessageId.ChangeLineAttributes)]
    public class ChangeLineAttributes : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public IsoColour LineColour { get; set; }
        [ByteOrder(4,1)]
        public int LineWidth { get; set; }
        [ByteOrder(5,2)]
        public int LineArt { get; set; }

        public ChangeLineAttributes()
        {
            CommandId = IsobusMessageId.ChangeLineAttributes;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeLineAttributesReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var lineColour = new IsoColour(m.Data[3]);
            var lineWidth = m.Data[4];
            var lineArt = BitConverter.ToUInt16(m.Data, 5);

            var message = new ChangeLineAttributes() { ObjectId = objectId, LineColour = lineColour, LineWidth = lineWidth, LineArt = lineArt };

            return message;
        }
    }
}
