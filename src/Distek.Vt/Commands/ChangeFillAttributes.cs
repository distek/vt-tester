﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.32 Change Fill Attributes Command
    [VtFunction(IsobusMessageId.ChangeFillAttributes)]
    public class ChangeFillAttributes : IsoVtMessage
    {
        [ByteOrder(1, 2)]
        public int ObjectId { get; set; }
        [ByteOrder(3, 1)]
        public int FillType { get; set; }
        [ByteOrder(4, 1)]
        public IsoColour FillColour { get; set; }
        [ByteOrder(5, 2)]
        public int FillPattern { get; set; }

        public ChangeFillAttributes()
        {
            CommandId = IsobusMessageId.ChangeFillAttributes;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class ChangeFillAttributesReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var fillType = m.Data[3];
            var fillColour = new IsoColour(m.Data[4]);
            var fillPattern = BitConverter.ToUInt16(m.Data, 5);

            var message = new ChangeFillAttributes() { ObjectId = objectId, FillType = fillType, FillColour = fillColour, FillPattern = fillPattern };

            return message;
        }
    }
}
