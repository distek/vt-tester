﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.8 Esc Command
    [VtFunction(IsobusMessageId.Esc)]
    public class Esc : IsoVtMessage
    {
        public Esc()
        {
            CommandId = IsobusMessageId.Esc;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class EscReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var message = new Esc() { };

            return message;
        }
    }
}
