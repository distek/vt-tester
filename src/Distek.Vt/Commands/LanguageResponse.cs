﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   public class LanguageResponse : CanMessage, IResponse
   {
      public LanguageResponse()
      {
         Priority = 6;
         ParameterGroupNumber = 0xFE0F;

         // Defaults
         LanguageCode = "en";
         DecimalSymbol = DecimalSymbol.PointIsUsed;
         TimeFormat = TimeFormat.TwelveHour;
         DistanceUnits = DistanceUnits.Imperial;
         AreaUnits = AreaUnits.Imperial;
         VolumeUnits = VolumeUnits.Imperial;
         MassUnits = MassUnits.Imperial;
         TemperatureUnits = TemperatureUnits.Imperial;
         PressureUnits = PressureUnits.Imperial;
         ForceUnits = ForceUnits.Imperial;
         UnitsSystem = UnitsSystem.US;
      }

      public string LanguageCode { get; set; }
      public DecimalSymbol DecimalSymbol { get; set; }
      public TimeFormat TimeFormat { get; set; }
      public DistanceUnits DistanceUnits { get; set; }
      public AreaUnits AreaUnits { get; set; }
      public VolumeUnits VolumeUnits { get; set; }
      public MassUnits MassUnits { get; set; }
      public TemperatureUnits TemperatureUnits { get; set; }
      public PressureUnits PressureUnits { get; set; }
      public ForceUnits ForceUnits { get; set; }
      public UnitsSystem UnitsSystem { get; set; }

      public CanMessage ToBytes()
      {
         var b = new byte[8];
         var s = Encoding.ASCII.GetBytes(this.LanguageCode);
         b[0] = s[0];
         b[1] = s[1];
         b[2] = (byte)(((byte)DecimalSymbol << 6) | ((byte)TimeFormat << 4) | 0x0F);
         // hardcoding date format for now
         b[3] = 2;
         b[4] = (byte)(((byte)DistanceUnits << 6) | ((byte)AreaUnits << 4) | ((byte)VolumeUnits << 2) | ((byte)MassUnits));
         b[5] = (byte)(((byte)TemperatureUnits << 6) | ((byte)PressureUnits << 4) | ((byte)ForceUnits << 2) | ((byte)UnitsSystem));
         b[6] = 0xFF;
         b[7] = 0xFF;

         this.Data = b;
         return this;
      }
   }

   [Flags]
   public enum DecimalSymbol
   {
      CommaIsUsed = 0,
      PointIsUsed = 1,
      Reserved = 2,
      NoAction = 3
   }

   [Flags]
   public enum TimeFormat
   {
      TwentyFourHour = 0,
      TwelveHour = 1,
      Reserved = 2,
      NoAction = 3
   }

   [Flags]
   public enum DistanceUnits
   {
      Metric = 0,
      Imperial = 1,
      Reserved = 2,
      NoAction = 3
   }

   [Flags]
   public enum AreaUnits
   {
      Metric = 0,
      Imperial = 1,
      Reserved = 2,
      NoAction = 3
   }

   [Flags]
   public enum VolumeUnits
   {
      Metric = 0,
      Imperial = 1,
      Reserved = 2,
      NoAction = 3
   }

   [Flags]
   public enum MassUnits
   {
      Metric = 0,
      Imperial = 1,
      US = 2,
      NoAction = 3
   }

   public enum TemperatureUnits
   {
      Metric = 0,
      Imperial = 1,
      Reserved = 2,
      NoAction = 3
   }

   public enum PressureUnits
   {
      Metric = 0,
      Imperial = 1,
      Reserved = 2,
      NoAction = 3
   }

   public enum ForceUnits
   {
      Metric = 0,
      Imperial = 1,
      Reserved = 2,
      NoAction = 3
   }

   public enum UnitsSystem
   {
      Metric = 0,
      Imperial = 1,
      US = 2,
      NoAction = 3
   }
}
