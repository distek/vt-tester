﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
   // Annex F.34 Change Active Mask Command
   [VtFunction(IsobusMessageId.ChangeActiveMask)]
   public class ChangeActiveMask : IsoVtMessage
   {
      [ByteOrder(1,2)]
      public int WorkingSetObjectId { get; set; }
      [ByteOrder(3,2)]
      public int ActiveMaskObjectId { get; set; }

      public ChangeActiveMask()
      {
         CommandId = IsobusMessageId.ChangeActiveMask;
         Priority = 8;
         ParameterGroupNumber = 0xE700;
         DestinationAddress = 0xFF;
      }
   }

   public class ChangeActiveMaskReader : IMessageReader
   {
      public CanMessage Read(CanMessage m)
      {
         var workingSetObjectId = BitConverter.ToUInt16(m.Data, 1);
         var newMaskId = BitConverter.ToUInt16(m.Data, 3);

         // Create the specific message type to send to the vt objects
         var message = new ChangeActiveMask() { WorkingSetObjectId = workingSetObjectId, ActiveMaskObjectId = newMaskId };

         return message;

      }
   }
}
