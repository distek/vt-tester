﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.2 Hide Show Object Command
    [VtFunction(IsobusMessageId.HideShowObject)]
    public class HideShowObject : IsoVtMessage
    {
        [ByteOrder(1,2)]
        public int ObjectId { get; set; }
        [ByteOrder(3,1)]
        public bool Visible { get; set; }

        public HideShowObject()
        {
            CommandId = IsobusMessageId.HideShowObject;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }

    public class HideShowObjectReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var visible = !BitConverter.ToBoolean(m.Data, 3);

            var message = new HideShowObject() { ObjectId = objectId, Visible = visible };

            return message;
        }
    }
}
