﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Commands
{
    // Annex F.44 Delete Object Pool Command
    [VtFunction(IsobusMessageId.DeleteObjectPool)]
    public class DeleteObjectPool : IsoVtMessage
    {
        public DeleteObjectPool()
        {
            CommandId = IsobusMessageId.DeleteObjectPool;
            Priority = 8;
            ParameterGroupNumber = 0xE700;
            DestinationAddress = 0xFF;
        }
    }
    public class DeleteObjectPoolReader : IMessageReader
    {
        public CanMessage Read(CanMessage m)
        {
            var message = new DeleteObjectPool() { };

            return message;
        }
    }
}
