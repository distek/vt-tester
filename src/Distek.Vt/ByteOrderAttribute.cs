﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   /*
     These attributes are used to reflect over different IsoObjects to serialize them.  If we run
     into performance issues, they can be removed and replace with [type, value] dictionaries
   */
   public class ByteOrderAttribute : Attribute
   {
      public int Order { get; protected set; }

      public int Length { get; protected set; }

      /// <summary>
      /// Attribute for constant length message fields
      /// </summary>
      /// <param name="order"></param>
      /// <param name="length"></param>
      public ByteOrderAttribute(int order, int length)
      {
         this.Order = order;
         this.Length = length;
      }

      /// <summary>
      /// Attribute for variable length message fields
      /// </summary>
      /// <param name="order"></param>
      public ByteOrderAttribute(int order)
      {
         this.Order = order;
         this.Length = -1;
      }
   }

   public class VtFunctionAttribute : Attribute
   {
      public IsobusMessageId MessageId { get; set; }

      public VtFunctionAttribute(IsobusMessageId messageId)
      {
         this.MessageId = messageId;
      }
   }

   public class ObjectTypeAttribute : Attribute
   {
      public IsobusObjectType ObjectType { get; set; }

      public ObjectTypeAttribute(IsobusObjectType objectType)
      {
         this.ObjectType = objectType;
      }
   }
}
