﻿using Distek.Network;

namespace Distek.Vt
{
   /*
     Here's the object hierarchy:
     isoobject:			abstract class, empty
     -isovtobject			abstract, has object id
     -isovtmessage			abstract, empty, type flag 
   */
   public abstract class IsoObject
   { }

   public abstract class IsoVtMessage : CanMessage
   {
      /// <summary>
      /// Command Number for this message.  Corresponds to byte 0
      /// of a CAN message
      /// </summary>
      public IsobusMessageId CommandId { get; protected set; }

   }
}
