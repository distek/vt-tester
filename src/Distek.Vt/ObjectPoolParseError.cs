﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class ObjectPoolParseError
   {
      public int ParentObjectId { get; set; }
      public int FaultyObjectId { get; set; }
      public int ErrorCode { get; set; }
   }
}
