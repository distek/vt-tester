﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Distek.Vt.Objects
{
   [ObjectType(IsobusObjectType.AuxObjectPointer)]
   public class AuxObjectPointer : IsoVtObject
   {
      public AuxObjectPointer(int objectId) : base(objectId)
      {
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         var a = reader.ReadByte();
         var functionType = reader.ReadUInt16();
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         throw new NotImplementedException();
      }
   }
}
