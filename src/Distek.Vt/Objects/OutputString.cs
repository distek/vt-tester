﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Distek.Platform;
using Distek.Vt.Commands;

namespace Distek.Vt.Objects
{
   // Annex B.9.2 Output String Object
   [ObjectType(IsobusObjectType.OutputString)]
   public class OutputString : IsoVtObject, IReferencesMacros 
   {
      public string Type { get { return "iso-outputstring"; } }
      
      public int Width { get; set; }
      public int Height { get; set; }
      public IsoColour BackgroundColour { get; set; }
      public int FontAttributes { get; set; }
      public int Options { get; set; }
      public int VariableReference { get; set; }
      public int Justification { get; set; }
      public int Length { get; set; }
      public string Value { get; set; }
      public bool Transparent { get; set; }
      public bool AutoWrap { get; set; }
      public bool WrapOnHyphen { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputString(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Width = reader.ReadUInt16();
         this.Height = reader.ReadUInt16();
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         this.FontAttributes = reader.ReadUInt16();
         this.Options = reader.ReadByte();
         this.VariableReference = reader.ReadUInt16();
         this.Justification = reader.ReadByte();
         this.Length = reader.ReadUInt16();
         var bytes = reader.ReadBytes(this.Length);
         this.Value = Encoding.ASCII.GetString(bytes);
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }

         this.Transparent = (this.Options & 0x01) == 0x01;
         this.AutoWrap = (this.Options & 0x02) == 0x02;
         this.WrapOnHyphen = (this.Options & 0x04) == 0x04;
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.Width);
         writer.Write((UInt16)this.Height);
         writer.Write((byte)this.BackgroundColour.ColourIndex);
         writer.Write((UInt16)this.FontAttributes);
         writer.Write((byte)this.Options);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((byte)this.Justification);
         writer.Write((UInt16)this.Length);
         var b = Encoding.UTF8.GetBytes(this.Value);
         writer.Write(b);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
