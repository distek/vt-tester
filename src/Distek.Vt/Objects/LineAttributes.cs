﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Objects
{
   // Annex B.14.3 LineAttributes Object
   [ObjectType(IsobusObjectType.LineAttributes)]
   public class LineAttributes : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-lineattributes"; } }
      
      public IsoColour LineColour { get; set; }
      public int LineWidth { get; set; }
      public int LineArt { get; set; }
 
      public IList<ReferencedMacro> Macros { get; set; }

      public LineAttributes(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.LineColour = new IsoColour(reader.ReadByte());
         this.LineWidth = reader.ReadByte();
         this.LineArt = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.LineColour.ColourIndex);
         writer.Write((byte)this.LineWidth);
         writer.Write((UInt16)this.LineArt);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
