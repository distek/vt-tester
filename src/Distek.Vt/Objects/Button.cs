using Distek.Platform;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Objects
{
   // Annex B.7 Button Object
   [ObjectType(IsobusObjectType.Button)]
   public class Button : IsoVtObject, IReferencesObjects, IReferencesMacros
   {
      public string Type { get { return "iso-button"; } }

      public int Width { get; set; }
      public int Height { get; set; }
      public IsoColour BackgroundColour { get; set; }
      public IsoColour BorderColour { get; set; }
      public int KeyCode { get; set; }
      public int Options { get; set; }
      public IList<ReferencedObject> ChildObjects { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public Button(int objectId) : base(objectId)
      {
         ChildObjects = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Width = reader.ReadUInt16();
         this.Height = reader.ReadUInt16();
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         this.BorderColour = new IsoColour(reader.ReadByte());
         this.KeyCode = reader.ReadByte();
         this.Options = reader.ReadByte();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.ChildObjects.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.Width);
         writer.Write((UInt16)this.Height);
         writer.Write((byte)this.BackgroundColour.ColourIndex);
         writer.Write((byte)this.BorderColour.ColourIndex);
         writer.Write((byte)this.KeyCode);
         writer.Write((byte)this.Options);
         writer.Write((byte)this.ChildObjects.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.ChildObjects)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
