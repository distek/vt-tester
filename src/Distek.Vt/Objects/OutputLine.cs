﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Distek.Platform;

namespace Distek.Vt.Objects
{
   // Annex B.10.2 Output Line Object
   [ObjectType(IsobusObjectType.OutputLine)]
   public class OutputLine : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-outputline"; } }

      public int LineAttributes { get; set; }
      public int Width { get; set; }
      public int Height { get; set; }
      public int LineDirection { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputLine(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.LineAttributes = reader.ReadUInt16();
         this.Width = reader.ReadUInt16();
         this.Height = reader.ReadUInt16();
         this.LineDirection = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.LineAttributes);
         writer.Write((UInt16)this.Width);
         writer.Write((UInt16)this.Height);
         writer.Write((byte)this.LineDirection);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
