﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Objects
{
   [ObjectType(IsobusObjectType.ExternalObjectDefinition)]
   public class AuxInType1 : IsoVtObject
   {
      public AuxInType1(int objectId) : base(objectId)
      {
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         var bgColour = reader.ReadByte();
         var functionType = reader.ReadByte();
         var inputId = reader.ReadByte();
         var num = reader.ReadByte();
         for (var i = 0; i < num; i++)
         {
            reader.ReadUInt16();
            reader.ReadInt16();
            reader.ReadInt16();
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
      }
   }
}
