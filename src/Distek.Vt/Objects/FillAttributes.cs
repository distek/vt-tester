﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Objects
{
   // Annex B.14.4 Fill Attributes Object
   [ObjectType(IsobusObjectType.FillAttributes)]
   public class FillAttributes : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-fillattributes"; } }

      public FillType FillType { get; set; }
      public IsoColour FillColour { get; set; }
      public int FillPattern { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public FillAttributes(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.FillType = (FillType)reader.ReadByte();
         this.FillColour = new IsoColour(reader.ReadByte());
         this.FillPattern = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.FillType);
         writer.Write((byte)this.FillColour.ColourIndex);
         writer.Write((UInt16)this.FillPattern);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
