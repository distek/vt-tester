﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Distek.Vt.Commands;

namespace Distek.Vt.Objects
{
   // Annex B.15 Object Pointer Object
   [ObjectType(IsobusObjectType.ObjectPointer)]
   public class ObjectPointer : IsoVtObject
   {
      public string Type { get { return "iso-objectpointer"; } }
      
      public int Value { get; set; }

      public ObjectPointer(int objectId) : base(objectId)
      {
      }


      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Value = reader.ReadUInt16();
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.Value);
      }
   }
}
