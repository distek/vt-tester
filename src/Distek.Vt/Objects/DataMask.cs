using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Objects
{
   // Annex B.2 Data Mask Object
   [ObjectType(IsobusObjectType.DataMask)]
   public class DataMask : IsoVtObject, IReferencesObjects, IReferencesMacros
   {
      public string Type { get { return "iso-datamask"; } }

      public IsoColour BackgroundColour { get; set; }
      public int SoftKeyMaskObjectId { get; set; }

      public IList<ReferencedObject> ChildObjects { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public DataMask(int objectId) : base(objectId)
      {
         ChildObjects = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         this.SoftKeyMaskObjectId = reader.ReadUInt16();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         System.Diagnostics.Debug.WriteLine(string.Format("position: {0}, length: {1}", reader.BaseStream.Position, reader.BaseStream.Length));
         if (reader.BaseStream.Position - 5 >= reader.BaseStream.Length)
            return this;
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.ChildObjects.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.BackgroundColour.ColourIndex);
         writer.Write((UInt16)this.SoftKeyMaskObjectId);
         writer.Write((byte)this.ChildObjects.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.ChildObjects)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
