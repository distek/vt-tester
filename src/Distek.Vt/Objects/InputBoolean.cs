﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Objects
{
   [ObjectType(IsobusObjectType.InputBoolean)]
   public class InputBoolean : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-inputboolean"; } }

      public IsoColour BackgroundColour { get; set; }
      public IsoColour ForegroundColour { get; set; }
      public int Width { get; set; }
      public int VariableReference { get; set; }
      public bool Value { get; set; }
      public bool Enabled { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public InputBoolean(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         this.Width = reader.ReadUInt16();
         this.ForegroundColour = new IsoColour(reader.ReadUInt16());
         this.VariableReference = reader.ReadUInt16();
         this.Value = reader.ReadBoolean();
         this.Enabled = reader.ReadBoolean();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.BackgroundColour.ColourIndex);
         writer.Write((UInt16)this.Width);
         // Very confusing - the spec calls this ForegroundColour, but it's a pointer to a font attributes object
         writer.Write((UInt16)this.ForegroundColour.ColourIndex);
         writer.Write((UInt16)this.VariableReference);
         writer.Write(this.Value);
         writer.Write(this.Enabled);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
