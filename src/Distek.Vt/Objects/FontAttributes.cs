﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Objects
{
   // Annex B.14.2 Font Attributes Object
   [ObjectType(IsobusObjectType.FontAttributes)]
   public class FontAttributes : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-fontattributes"; } }

      public IsoColour FontColour { get; set; }
      public int FontSize { get; set; }
      public int FontType { get; set; }
      public FontStyle FontStyle { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public FontAttributes(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.FontColour = new IsoColour(reader.ReadByte());
         this.FontSize = reader.ReadByte();
         this.FontType = reader.ReadByte();
         this.FontStyle = (FontStyle)(reader.ReadByte());
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.FontColour.ColourIndex);
         writer.Write((byte)this.FontSize);
         writer.Write((byte)this.FontType);
         writer.Write((byte)this.FontStyle);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
