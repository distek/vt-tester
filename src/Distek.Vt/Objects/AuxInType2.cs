﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Objects
{
   [ObjectType(IsobusObjectType.AuxInputType2)]
   public class AuxInType2 : IsoVtObject
   {
      public string Type { get { return "iso-aux-input-2"; } }

      public IsoColour BackgroundColour { get; set; }
      public int FunctionAttributes { get; set; }
      public IList<ReferencedObject> ChildObjects { get; set; }

      public AuxInType2(int objectId) : base(objectId)
      {
         ChildObjects = new List<ReferencedObject>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         this.FunctionAttributes = reader.ReadByte();
         var num = reader.ReadByte();
         for (var i = 0; i < num; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.ChildObjects.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
      }
   }
}
