﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Objects
{
   // Annex B.13.2 Number Variable Object
   [ObjectType(IsobusObjectType.NumberVariable)]
   public class NumberVariable : IsoVtObject
   {
      public string Type { get { return "iso-numbervariable"; } }

      public int Value { get; set; }

      public NumberVariable(int objectId) : base(objectId)
      { }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Value = reader.ReadInt32();
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write(this.Value);
      }

      public IResponse HandleMessage(CanMessage message)
      {
         throw new NotImplementedException();
      }
   }
}
