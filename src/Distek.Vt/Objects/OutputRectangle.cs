﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Distek.Platform;

namespace Distek.Vt.Objects
{
   [Flags]
   public enum LineSuppression
   {
      SuppressTop = 0x01,
      SuppressRight = 0x02,
      SuppressBottom = 0x04,
      SuppressLeft = 0x08
   }

   // Annex B.10.3 Output Rectangle Object
   [ObjectType(IsobusObjectType.OutputRectangle)]
   public class OutputRectangle : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-outputrectangle"; } }
      
      public int LineAttributes { get; set; }
      public int Width { get; set; }
      public int Height { get; set; }
      public LineSuppression LineSuppression { get; set; }
      public int FillAttributes { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputRectangle(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.LineAttributes = reader.ReadUInt16();
         this.Width = reader.ReadUInt16();
         this.Height = reader.ReadUInt16();
         this.LineSuppression = (LineSuppression)reader.ReadByte();
         this.FillAttributes = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.LineAttributes);
         writer.Write((UInt16)this.Width);
         writer.Write((UInt16)this.Height);
         writer.Write((byte)this.LineSuppression);
         writer.Write((UInt16)this.FillAttributes);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
