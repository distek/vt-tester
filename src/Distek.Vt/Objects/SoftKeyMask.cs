using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Distek.Vt.Objects
{
   // Annex B.5 Soft Key Mask Object
   [ObjectType(IsobusObjectType.SoftKeyMask)]
   public class SoftKeyMask : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-softkeymask"; } }

      public IsoColour BackgroundColour { get; set; }

      public ObservableCollection<ReferencedObject> ChildObjects { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public SoftKeyMask(int objectId) : base(objectId)
      {
         // default background color
         BackgroundColour = new IsoColour(7);
         //ChildObjects = new List<ReferencedObject>();
         ChildObjects = new ObservableCollection<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               // softkeys don't have a location
               //XLocation = reader.ReadInt16(),
               //YLocation = reader.ReadInt16()
            };
            this.ChildObjects.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.BackgroundColour.ColourIndex);
         writer.Write((byte)this.ChildObjects.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.ChildObjects)
         {
            writer.Write((UInt16)c.ObjectId);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
