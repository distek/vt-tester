using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Objects
{
   // Annex B.3 Alarm Mask Object
   [ObjectType(IsobusObjectType.AlarmMask)]
   public class AlarmMask : IsoVtObject, IReferencesObjects, IReferencesMacros
   {
      public string Type { get { return "iso-alarmmask"; } }

      public IsoColour BackgroundColour {get;set;}
      public int SoftKeyMaskObjectId {get;set;}
      public int Priority {get; set; }
      public int AcousticSignal { get; set; }

      public IList<ReferencedObject> ChildObjects { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public AlarmMask(int objectId) : base(objectId)
      {
         ChildObjects = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.BackgroundColour = new IsoColour(reader.ReadByte());
         this.SoftKeyMaskObjectId = reader.ReadUInt16();
         this.Priority = reader.ReadByte();
         this.AcousticSignal = reader.ReadByte();
         var objectCount = reader.ReadByte();

         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.ChildObjects.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((byte)this.BackgroundColour.ColourIndex);
         writer.Write((UInt16)this.SoftKeyMaskObjectId);
         writer.Write((byte)this.Priority);
         writer.Write((byte)this.AcousticSignal);
         writer.Write((byte)this.ChildObjects.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.ChildObjects)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
