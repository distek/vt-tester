﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Distek.Platform;
using Distek.Vt.Commands;
using System.Diagnostics;

namespace Distek.Vt.Objects
{
   // Annex B.13 StringVariable Object
   [ObjectType(IsobusObjectType.StringVariable)]
   public class StringVariable : IsoVtObject
   {
      public string Type { get { return "iso-stringvariable"; } }

      public int Length { get; set; }
      public string Value { get; set; }

      public StringVariable(int objectId) : base(objectId)
      { }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Length = reader.ReadUInt16();
         var b = reader.ReadBytes(this.Length);
         if (b.Length > 2 && b[0] == 0xFF && b[1] == 0xFE)
         {
            //   var buf = new byte[this.Length];
            //   for (var i = 0; i < buf.Length; i++)
            //      buf[i] = reader.ReadByte();
            //   this.Value = Encoding.Unicode.GetString(buf, 0, this.Length);
            System.Diagnostics.Debug.WriteLine("widechar???");
         }
         else
         {
            // TODO: we'll need to support different encodings, and changes to the current encoding (i think?)
            // 4.6.19.7: 
            // v3 only supports latin-1 through latin-9
            // "The character set is indicated by the Font type attribute of the Font Attributes object."
            var e = Encoding.GetEncoding(28591);
            this.Value = e.GetString(b, 0, this.Length);
         }

         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         if (this.Length != this.Value.ToCharArray().Length)
            System.Diagnostics.Debug.Write("inconsistent string length");
         writer.Write((byte)this.Length);
         writer.Write('\0');
         writer.Write(this.Value.ToCharArray().Take(this.Length).ToArray());
      }
   }

}
