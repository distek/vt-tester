﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Distek.Platform;
using Newtonsoft.Json;

namespace Distek.Vt.Objects
{
   [Flags]
   public enum PictureGraphicOptions
   {
      Transparent = 0x01,
      Flashing = 0x02,
      RunLengthEncoded = 0x04
   }

   [Flags]
   public enum PictureGraphicFormat
   {
      Monochrome = 0x00,
      FourBitColor = 0x01,
      EightBitColor = 0x02,
   }

   // Annex B.12 Picture Graphic Object
   [ObjectType(IsobusObjectType.PictureGraphic)]
   public class PictureGraphic : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-picturegraphic"; } }

      public int Width { get; set; }
      public int ActualWidth { get; set; }
      public int ActualHeight { get; set; }
      public PictureGraphicFormat Format { get; set; }
      public PictureGraphicOptions Options { get; set; }
      public IsoColour TransparencyColour { get; set; }
      [JsonIgnore]
      public IList<byte> RawData { get; set; }

      /// <summary>
      /// URL of image rendered to a file
      /// </summary>
      public string ImgLink { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public PictureGraphic(int objectId) : base(objectId)
      {
         RawData = new List<byte>();
         Macros = new List<ReferencedMacro>();
         // Setting this to empty, otherwise the js doesn't parse the pool correctly
         // when it's null
         ImgLink = string.Empty;
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Width = reader.ReadUInt16();
         this.ActualWidth = reader.ReadUInt16();
         this.ActualHeight = reader.ReadUInt16();
         this.Format = (PictureGraphicFormat)reader.ReadByte();
         this.Options = (PictureGraphicOptions)reader.ReadByte();
         this.TransparencyColour = new IsoColour(reader.ReadByte());
         var dataCount = reader.ReadUInt32();
         var bytesLeft = reader.BaseStream.Length - reader.BaseStream.Position;
         if (bytesLeft < dataCount)
         {
            reader.ReadBytes((int)bytesLeft);
            return this;
         }
         var macroCount = reader.ReadByte();
         for (var i = 0; i < dataCount; i++)
         {
            this.RawData.Add(reader.ReadByte());
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         // i don't want to do this every time we load an object pool probably
         //this.EncodedImage = PictureHelper.Render(this);
         //this.ImgLink = PictureProcessor.Render(this, )
         // could just set the path since we know the image is already created, but we dont' know the master name here
         // why aren't we just storing the json version?
         //var d = Directory.GetCurrentDirectory();
         //var dir = Path.Combine(d, "wwwroot", "images", masterName);
         //Directory.CreateDirectory(dir);
         //var path = Path.Combine(d, "wwwroot", "images", masterName, pg.ObjectId.ToString()) + ".png";

         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.Width);
         writer.Write((UInt16)this.ActualWidth);
         writer.Write((UInt16)this.ActualHeight);
         writer.Write((byte)this.Format);
         writer.Write((byte)this.Options);
         writer.Write((byte)this.TransparencyColour.ColourIndex);
         writer.Write(this.RawData.Count);
         writer.Write((byte)this.Macros.Count);
         foreach (var b in this.RawData)
            writer.Write((byte)b);
         foreach (var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }

   }

}
