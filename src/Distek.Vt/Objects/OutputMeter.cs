﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Objects
{
   [Flags]
   public enum OutputMeterOptions
   {
      DrawArc = 0x01,
      DrawBorder = 0x02,
      DrawTicks = 0x04,
      DeflectionDirection = 0x08
   }

   // Annex B.11.2 Output Meter Object
   [ObjectType(IsobusObjectType.OutputMeter)]
   public class OutputMeter : IsoVtObject, IReferencesMacros
   {
      public string Type { get { return "iso-outputmeter"; } }

      public int Width { get; set; }
      public int Height { get; set; }
      public IsoColour NeedleColour { get; set; }
      public IsoColour BorderColour { get; set; }
      public IsoColour ArcAndTickColour { get; set; }
      public OutputMeterOptions Options { get; set; }
      public int NumberOfTicks { get; set; }
      public int StartAngle { get; set; }
      public int EndAngle { get; set; }
      public int MinValue { get; set; }
      public int MaxValue { get; set; }
      public int VariableReference { get; set; }
      public int Value { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputMeter(int objectId) : base(objectId)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Width = reader.ReadUInt16();
         this.Height = this.Width;
         //this.Height = reader.ReadUInt16();
         this.NeedleColour = new IsoColour(reader.ReadByte());
         this.BorderColour = new IsoColour(reader.ReadByte());
         this.ArcAndTickColour = new IsoColour(reader.ReadByte());
         this.Options = (OutputMeterOptions)reader.ReadByte();
         this.NumberOfTicks = reader.ReadByte();
         this.StartAngle = reader.ReadByte();
         this.EndAngle = reader.ReadByte();
         this.MinValue = reader.ReadUInt16();
         this.MaxValue = reader.ReadUInt16();
         this.VariableReference = reader.ReadUInt16();
         this.Value = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.Width);
         writer.Write((byte)this.NeedleColour.ColourIndex);
         writer.Write((byte)this.BorderColour.ColourIndex);
         writer.Write((byte)this.ArcAndTickColour.ColourIndex);
         writer.Write((byte)this.Options);
         writer.Write((byte)this.NumberOfTicks);
         writer.Write((byte)this.StartAngle);
         writer.Write((byte)this.EndAngle);
         writer.Write((UInt16)this.MinValue);
         writer.Write((UInt16)this.MaxValue);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((UInt16)this.Value);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }

}
