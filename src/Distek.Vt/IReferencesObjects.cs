﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface IReferencesObjects
   {
      IList<ReferencedObject> ChildObjects { get; set; }
   }
}
