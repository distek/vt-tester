﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public enum IsobusMessageId
   {
      // remove this!!!
      TempLanguage = 0x65,

      // Activation Messages
      SoftKeyActivation = 0,
      ButtonActivation = 1,
      PointingEvent = 2,
      VtSelectInputObject = 3,
      VtEsc = 4,
      VtChangeNumericValue = 5,
      VtChangeActiveMask = 6,
      VtChangeSoftKeyMask = 7,
      VtChangeStringValue = 8,
      VtOnUserLayoutHideShow = 9,
      VtControlAudioSignalTermination = 10,

      // Aux control messages
      AuxAssignment = 36,

      // Command and Macro Messages
      ObjectPoolTransfer = 17,
      EndOfObjectPool = 18,
      Esc = 146,
      HideShowObject = 160,
      EnableDisableObject = 161,
      SelectInputObject = 162,
      ControlAudioSignal = 163,
      SetAudioVolume = 164,
      ChangeChildLocation = 165,
      ChangeSize = 166,
      ChangeBackgroundColour = 167,
      ChangeNumericValue = 168,
      ChangeEndPoint = 169,
      ChangeFontAttributes = 170,
      ChangeLineAttributes = 171,
      ChangeFillAttributes = 172,
      ChangeActiveMask = 173,
      ChangeSoftKeyMask = 174,
      ChangeAttribute = 175,
      ChangePriority = 176,
      ChangeListItem = 177,
      DeleteObjectPool = 178,
      ChangeStringValue = 179,
      ChangeChildPosition = 180,
      LockUnlockMask = 189,
      ExecuteMacro = 190,

      // Technical Data Messages
      GetMemory = 192,
      GetNumberOfSoftKeys = 194,
      GetTextFont = 195,
      GetHardware = 199,

      // Non-Volatile Memory Operations Commands
      StoreVersion = 208,
      LoadVersion = 209,
      DeleteVersion = 210,
      GetVersions = 223,
      GetVersionsResponse = 224,

      VtStatus = 254,
      WorkingSetMaintenance = 255,

      // v4 and above
      ChangePolygonPoint = 182,
      ChangePolygonScale = 183,
      GraphicsContext = 184,
      GetAttributeValue = 185,
      SelectColourMap = 186,
      IdentifyVt = 187,
      ChangeObjectLabel = 191,
      GetSupportedWideChars = 193,
      GetWindowMaskData = 196,
      GetSupportedObjects = 197,

      // v5 and above
      ExecuteExtendedMacro = 188,
      ExtendedGetVersions = 211,
      ExtendedStoreVersion = 212,
      ExtendedLoadVersion = 213,
      ExtendedDeleteVersion = 214,
      UnsupportedVtFunction = 253,
   }

   public enum IsobusObjectType
   {
      WorkingSet = 0,
      DataMask = 1,
      AlarmMask = 2,
      Container = 3,
      SoftKeyMask = 4,
      SoftKey = 5,
      Button = 6,
      InputBoolean = 7,
      InputString = 8,
      InputNumber = 9,
      InputList = 10,
      OutputString = 11,
      OutputNumber = 12,
      OutputList = 37,
      OutputLine = 13,
      OutputRectangle = 14,
      OutputEllipse = 15,
      OutputPolygon = 16,
      OutputMeter = 17,
      OutputLinearBarGraph = 18,
      OutputArchedBarGraph = 19,
      PictureGraphic = 20,
      NumberVariable = 21,
      StringVariable = 22,
      FontAttributes = 23,
      LineAttributes = 24,
      FillAttributes = 25,
      InputAttributes = 26,
      ExtendedInputAttributes = 38,
      ObjectPointer = 27,
      Macro = 28,
      AuxFunctionType2 = 31,
      AuxInputType2 = 32,
      ColourMap = 39,
      GraphicsContext = 36,
      AuxObjectPointer = 33,
      WindowMask = 34,
      KeyGroup = 35,
      ObjectLabelReferenceList = 40,
      ExternalObjectDefinition = 41,
      ExternalReferenceName = 42,
      ExternalObjectPointer = 43,
      Animation = 44,
   };

   public enum ChangeNumericValueError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidValue = 0x02,
      ValueInUse = 0x04,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum StoreVersionError
   {
      NoError = 0x00,
      IncorrectVersionLabel = 0x02,
      InsufficientMemory = 0x04,
      AnyOtherError = 0x08,
   }

   public enum LoadVersionError
   {
      NoError = 0x00,
      FileSystemError = 0x01,
      IncorrectVersionLabel = 0x02,
      InsufficientMemory = 0x04,
      AnyOtherError = 0x08,
   }

   public enum DeleteVersionError
   {
      NoError = 0x00,
      IncorrectVersionLabel = 0x02,
      AnyOtherError = 0x08,
   }

   public enum VtBusyCode
   {
      NotBusy = 0x00,
      BusyUpdatingVisibleMask = 0x01,
      BusySavingToNonVolatileMemory = 0x02,
      BusyExecutingACommand = 0x04,
      BusyExecutingAMacro = 0x08,
      BusyParsingObjectPool = 0x10,
      AuxControlsLearnMode = 0x40,
      OutOfMemory = 0x80,
   }

   public enum IsoFillType
   {
      /// <summary>
      /// No Fill
      /// </summary>
      NoFill = 0,
      /// <summary>
      /// Fill with line colour
      /// </summary>
      FillWithLineColour = 1,
      /// <summary>
      /// Fill with specified colour in fill colour attribute
      /// </summary>
      FillWithColour = 2,
      /// <summary>
      /// Fill with pattern given by fill pattern attribute
      /// </summary>
      FillWithPattern = 3,
   }

   public enum ChangeActiveMaskError
   {
      NoError = 0x00,
      InvalidWorkingSetObjectId = 0x01,
      InvalidActiveMaskObjectId = 0x02,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ChangeSizeError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      Undefined = 0x00,
      AnyOtherError = 0x08,
   }

   public enum ChangeBackgroundColourError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidColourCode = 0x02,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ChangeChildLocationPositionError
   {
      NoError = 0x00,
      InvalidParentObjectId = 0x01,
      InvalidObjectId = 0x02,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ChangeSoftKeyMaskError
   {
      NoError = 0x00,
      InvalidDataAlarmObjectId = 0x01,
      InvalidSoftKeyObjectId = 0x02,
      MissingObjects = 0x04,
      MaskChildObjectError = 0x08,
      AnyOtherError = 0x10,
   }

   public enum ChangeAttributeError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidAttributeId = 0x02,
      InvalidValue = 0x04,
      ValueInUse = 0x08,
      AnyOtherError = 0x10,
   }

   public enum ChangePriorityError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidPriority = 0x02,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum HideShowObjectError
   {
      NoError = 0x00,
      ReferencesToMissingObjects = 0x01,
      InvalidObjectId = 0x02,
      CommandError = 0x04,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum EnableDisableObjectError
   {
      NoError = 0x00,
      Undefined = 0x00,
      InvalidObjectId = 0x02,
      CommandError = 0x04,
      OperatorInputIsActiveOnObject = 0x08,
      AnyOtherError = 0x10,
   }

   public enum SelectInputObjectObjectResponses
   {
      ObjectNotSelectedOrNull = 0x00,
      ObjectIsSelected = 0x01,
      ObjectIsOpenForEdit = 0x02,
   }

   public enum SelectInputObjectError
   {
      NoError = 0x00,
      ObjectDisabled = 0x01,
      InvalidObjectId = 0x02,
      ObjectNotOnMaskOrHidden = 0x04,
      OtherInputBeingModified = 0x08,
      AnyOtherError = 0x10,
   }

   public enum EscError
   {
      NoError = 0x00,
      AudioDeviceIsBusy = 0x01,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ControlAudioSignalError
   {
      NoError = 0x00,
      AudioDeviceIsBusy = 0x01,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum SetAudioVolumeError
   {
      NoError = 0x00,
      ObjectDisabled = 0x01,
      CommandNotSupported = 0x02,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ChangeStringValueError
   {
      NoError = 0x00,
      Undefined = 0x00,
      InvalidObjectId = 0x02,
      StringTooLong = 0x04,
      AnyOtherError = 0x10,
   }

   public enum ChangeEndPointError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidLineDirection = 0x02,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ChangeFontAttributesError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidColour = 0x02,
      InvalidSize = 0x04,
      InvalidType = 0x08,
      InvalidStyle = 0x10,
      AnyOtherError = 0x20,
   }

   public enum ChangeLineAttributesError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidColour = 0x02,
      InvalidWidth = 0x04,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum ChangeFillAttributesError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidType = 0x02,
      InvalidColour = 0x04,
      InvalidPattern = 0x08,
      AnyOtherError = 0x10,
   }

   public enum ChangeListItemError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidListIndex = 0x02,
      InvalidNewObjectId = 0x04,
      ValueInUse = 0x08,
      AnyOtherError = 0x10,
   }

   public enum DeleteObjectPoolError
   {
      NoError = 0x00,
      DeletionError = 0x01,
      Undefined = 0x00,
      AnyOtherError = 0x10,
   }

   public enum LockUnlockMaskError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      AlreadyLocked = 0x02,
      AlreadyUnlocked = 0x04,
      AlarmMaskIsActive = 0x08,
      Timeout = 0x10,
      MaskHidden = 0x20,
      OperatorInduced = 0x40,
      AnyOtherError = 0x80,
   }

   public enum ExecuteMacroError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      NotMacroObject = 0x02,
      AnyOtherError = 0x04,
   }

   public enum ChangeObjectLabelError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidStringVariableObjectId = 0x02,
      InvalidFontType = 0x04,
      NoLabelReferenceListObject = 0x08,
      DesignatorReferencesInvalidObjects = 0x10,
      AnyOtherError = 0x20,
   }

   public enum ChangePolygonPointError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidPointIndex = 0x02,
      AnyOtherError = 0x04,
   }

   public enum ChangePolygonScaleError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      undefined = 0x00,
      AnyOtherError = 0x04,
   }

   public enum GraphicsContextError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidSubCommandId = 0x02,
      InvalidParameter = 0x04,
      InvalidResults = 0x08,
      AnyOtherError = 0x10,
   }

   public enum GetAttributeValueError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidAttributeId = 0x02,
      Reserved = 0x00,
      AnyOtherError = 0x10,
   }

   public enum SelectColourMapError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidColourMap = 0x02,
      AnyOtherError = 0x04,
   }

   public enum ExecuteExtendedMacroError
   {
      NoError = 0x00,
      InvalidObjectId = 0x01,
      InvalidMacroObject = 0x02,
      AnyOtherError = 0x04,
   }

   public enum Acknowledegments
   {
      PositiveAcknowledgement = 0x00,
      NegativeAcknowledgement = 0x01,
      AccessDenied = 0x02,
      CannotRespond = 0x03,
   }

   [Flags]
   public enum FillType
   {
      NoFill = 0,
      FillWithLineColor = 1,
      FillWithFillColor = 2,
      FillWithPattern = 3
   }
}
