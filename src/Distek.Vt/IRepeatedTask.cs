﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface IRepeatedTask
   {
      /// <summary>
      /// Interval to run this task at in seconds
      /// </summary>
      int Interval { get; }

      /// <summary>
      /// Executes the task one time
      /// </summary>
      void Tick();
   }
}
