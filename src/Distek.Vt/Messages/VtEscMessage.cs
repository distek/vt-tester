﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.VtEsc)]
   public class VtEscMessage : CanMessage
   {
      public VtEscMessage()
      {
         this.ParameterGroupNumber = 0xE600;
         this.Priority = 7;
         this.DataPage = 0;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int ErrorCodes { get; set; }
   }
}
