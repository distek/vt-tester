﻿using Distek.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.VtSelectInputObject)]
   public class VtSelectInputObjectMessage : CanMessage
   {
      public VtSelectInputObjectMessage()
      {
         this.ParameterGroupNumber = 0xE600;
         this.Priority = 7;
         this.DataPage = 0;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public bool Selection { get; set; }
      [ByteOrder(4, 4)]
      public byte[] Reserved { get { return new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }; } }
   }
}
