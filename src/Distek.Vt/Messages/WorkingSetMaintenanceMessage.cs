﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.WorkingSetMaintenance)]
   public class WorkingSetMaintenanceMessage : IsoVtMessage
   {
      [ByteOrder(1, 1)]
      public bool FirstTime { get; set; }
      [ByteOrder(2, 1)]
      public int VersionNumber { get; set; }
      [ByteOrder(3, 4)]
      public byte[] Reserverd { get { return new byte[] { 0x00, 0x00, 0x00, 0x00 }; } }

      public WorkingSetMaintenanceMessage()
      {
         this.ParameterGroupNumber = 0xE700;
         this.Priority = 7;
         this.DataPage = 0;
      }
   }
}
