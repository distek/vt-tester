﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.EndOfObjectPool)]
   public class EndOfObjectPoolResponse : IsoVtMessage, IResponse
   {
      [ByteOrder(1, 1)]
      public int ErrorCode { get; set; }

      [ByteOrder(2, 2)]
      public int ParentObjectId { get; set; }

      [ByteOrder(4, 2)]
      public int FaultyObjectId { get; set; }

      [ByteOrder(6, 1)]
      public int ObjectPoolErrorCodes { get; set; }

      public EndOfObjectPoolResponse(int destination)
      {
         this.DestinationAddress = destination;
         this.ParameterGroupNumber = 0xE600;
         this.Priority = 7;

         // Default to no errors
         this.ErrorCode = 0;
         this.ObjectPoolErrorCodes = 0;
         this.FaultyObjectId = 0xFFFF;
         this.ParentObjectId = 0xFFFF;
      }
   }
}
