﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   [VtFunction(IsobusMessageId.VtStatus)]
   public class VtStatusMessage : IsoVtMessage
   {
      public VtStatusMessage()
      {
         Priority = 7;
         ExtendedDataPage = 0;
         DataPage = 0;
         ParameterGroupNumber = 0xE600;
         // Sent to global destination
         DestinationAddress = 0xFF;
      }

      /// <summary>
      /// Source address of the active working set master ("owns" the VT)
      /// </summary>
      [ByteOrder(1, 1)]
      public int ActiveWorkingSetMaster { get; set; }

      /// <summary>
      /// Object id of the visible data/alarm mask of the active working set
      /// </summary>
      [ByteOrder(2, 2)]
      public int VisibleMask { get; set; }

      /// <summary>
      /// Object is of the visible soft key mask of the active working set
      /// </summary>
      [ByteOrder(4, 2)]
      public int VisibleSoftKeyMask { get; set; }

      /// <summary>
      /// VT busy codes
      /// </summary>
      [ByteOrder(6, 1)]
      public VtBusyCode BusyCode { get; set; }

      /// <summary>
      /// VT function code of current command being executed (valid only if
      /// command or macro busy code is set)
      /// </summary>
      [ByteOrder(7, 1)]
      public int VtFunctionCode { get; set; }
   }
}
