﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.VtChangeNumericValue)]
   public class VtChangeNumericValueMessage : CanMessage
   {
      public VtChangeNumericValueMessage(int destination)
      {
         this.ParameterGroupNumber = 0xE600;
         this.DestinationAddress = destination;
         this.Priority = 7;
         this.DataPage = 0;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int Reserved { get { return 0xFF; } }
      [ByteOrder(4, 4)]
      public int Value { get; set; }
   }
}
