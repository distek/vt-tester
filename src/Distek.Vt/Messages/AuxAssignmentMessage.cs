﻿using Distek.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.AuxAssignment)]
    public class AuxAssignmentMessage : CanMessage
    {
      public AuxAssignmentMessage(int destination)
      {
         this.ParameterGroupNumber = 0xE600;
         this.DestinationAddress = destination;
         this.Priority = 7;
         this.DataPage = 0;
      }

      [ByteOrder(1, 9)]
      public byte[] CanName { get; set; }
      [ByteOrder(9, 1)]
      public int Flags { get { return 0xFF; } }
      [ByteOrder(10, 2)]
      public int InputId { get; set; }
      [ByteOrder(12, 2)]
      public int FunctionId { get; set; }
   }
}
