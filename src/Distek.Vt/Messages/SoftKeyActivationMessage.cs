﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.SoftKeyActivation)]
   public class SoftKeyActivationMessage : CanMessage
   {
      public SoftKeyActivationMessage()
      {
         this.ParameterGroupNumber = 0xE600;
         //this.DestinationAddress = destination;
         this.Priority = 7;
      }

      [ByteOrder(1, 1)]
      public int KeyActivationCode { get; set; }
      [ByteOrder(2, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(4, 2)]
      public int ParentObjectId { get; set; }
      [ByteOrder(6, 1)]
      public int KeyNumber { get; set; }
   }

   public class SoftKeyActivationReader : IMessageReader
   {
      public CanMessage Read(CanMessage m)
      {
         // Create the specific message type to send to the vt objects
         var message = new SoftKeyActivationMessage()
         {
            KeyActivationCode = m.Data[1],
            ObjectId = BitConverter.ToUInt16(m.Data, 2),
            ParentObjectId = BitConverter.ToUInt16(m.Data, 4),
            KeyNumber = m.Data[6],
         };

         return message;

      }
   }
}
