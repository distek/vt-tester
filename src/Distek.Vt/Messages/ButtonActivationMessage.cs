﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.ButtonActivation)]
   public class ButtonActivationMessage : CanMessage
   {
      public ButtonActivationMessage()
      {
         this.ParameterGroupNumber = 0xE600;
         this.Priority = 7;
         this.DataPage = 0;
      }

      [ByteOrder(1, 1)]
      public int KeyActivationCode { get; set; }
      [ByteOrder(2, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(4, 2)]
      public int ParentObjectId { get; set; }
      [ByteOrder(6, 1)]
      public int KeyNumber { get; set; }
   }
}
