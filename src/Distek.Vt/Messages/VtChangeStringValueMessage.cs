﻿using Distek.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Vt.Messages
{
   [VtFunction(IsobusMessageId.VtChangeStringValue)]
   public class VtChangeStringValueMessage : CanMessage
   {
      public VtChangeStringValueMessage(int destination)
      {
         this.ParameterGroupNumber = 0xE600;
         this.DestinationAddress = destination;
         this.Priority = 7;
         this.DataPage = 0;
      }

      [ByteOrder(1, 2)]
      public int ObjectId { get; set; }
      [ByteOrder(3, 1)]
      public int ByteCount { get { return Value.Length; } }
      [ByteOrder(4)]
      public string Value { get; set; }
   }
}
