﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Messages
{
	[VtFunction(IsobusMessageId.ObjectPoolTransfer)]
	public class ObjectPoolTransferMessage : CanMessage
	{
		[ByteOrder(1)]
		public Byte[] Objects { get; set; }

		public ObjectPoolTransferMessage()
		{
			ParameterGroupNumber = 0xE700;
			DestinationAddress = 0x26;
		}
	}
}
