﻿using Distek.Network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface ISerialization
   {
      /// <summary>
      /// Converts an object pool to bytes
      /// </summary>
      /// <param name="objects"></param>
      /// <returns></returns>
      Stream SerializeObjectPool(IList<IsoVtObject> objects);

      byte[] SerializeCanMessage(CanMessage message);
   }
}
