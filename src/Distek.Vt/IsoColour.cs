﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class IsoColour
   {
      public Color Color { get; set; }

      public int R { get { return Color.R; } protected set { } }
      public int G { get { return Color.G; } protected set { } }
      public int B { get { return Color.B; } protected set { } }

      public int ColourIndex { get; set; }

      [JsonConstructor]
      public IsoColour(int r, int g, int b)
      {
         Color = Color.FromArgb(r, g, b);
      }

      public IsoColour(int index)
      {
         ColourIndex = index;
         if (_indexedColors.ContainsKey(index))
         {
            Color = _indexedColors[index];
         }
         else
         {
            Color = Color.Transparent;
         }
      }

      // this is more gooder than using the constructor, but i don't think its used anywhere
      public static Color FromIndex(int i)
      {
         return Color.Black;
      }

      public static Color FromRGB(int r, int g, int b)
      {
         return Color.FromArgb(r, g, b);
      }

      private static Dictionary<int, Color> _indexedColors = new Dictionary<int, Color>()
        {
            { 0, Color.FromArgb(0 , 0, 0) },
            { 1, Color.FromArgb( 0xFF , 0xFF , 0xFF)},
            { 2, Color.FromArgb( 0x0  , 0x99 , 0x0)},
            { 3  ,Color.FromArgb( 0x0  , 0x99 , 0x99)},
            { 4  ,Color.FromArgb( 0x99 , 0x0  , 0x0)},
            { 5  ,Color.FromArgb( 0x99 , 0x0  , 0x99)},
            { 6  ,Color.FromArgb( 0x99 , 0x99 , 0x0)},
            { 7  ,Color.FromArgb( 0xCC , 0xCC , 0xCC)},
            { 8  ,Color.FromArgb( 0x99 , 0x99 , 0x99)},
            { 9  ,Color.FromArgb( 0x0  , 0x0  , 0xFF)},
            { 10 ,Color.FromArgb( 0x0  , 0xFF , 0x0)},
            { 11 ,Color.FromArgb( 0x0  , 0xFF , 0xFF)},
            { 12 ,Color.FromArgb( 0xFF , 0x0  , 0x0)},
            { 13 ,Color.FromArgb( 0xFF , 0x0  , 0xFF)},
            { 14 ,Color.FromArgb( 0xFF , 0xFF , 0x0)},
            { 15 ,Color.FromArgb( 0x0  , 0x0  , 0x99)},
            { 16 ,Color.FromArgb( 0x0  , 0x0  , 0x0)},
            { 17 ,Color.FromArgb( 0x0  , 0x0  , 0x33)},
            { 18 ,Color.FromArgb( 0x0  , 0x0  , 0x66)},
            { 19 ,Color.FromArgb( 0x0  , 0x0  , 0x99)},
            { 20 ,Color.FromArgb( 0x0  , 0x0  , 0xCC)},
            { 21 ,Color.FromArgb( 0x0  , 0x0  , 0xFF)},
            { 22 ,Color.FromArgb( 0x0  , 0x33 , 0x0)},
            {23 ,Color.FromArgb( 0x0  , 0x33 , 0x33)},
            {24 ,Color.FromArgb( 0x0  , 0x33 , 0x66)},
            {25 ,Color.FromArgb( 0x0  , 0x33 , 0x99)},
            {26 ,Color.FromArgb( 0x0  , 0x33 , 0xCC)},
            {27 ,Color.FromArgb( 0x0  , 0x33 , 0xFF)},
            {28 ,Color.FromArgb( 0x0  , 0x66 , 0x0)},
            {29 ,Color.FromArgb( 0x0  , 0x66 , 0x33)},
            {30 ,Color.FromArgb( 0x0  , 0x66 , 0x66)},
            {31 ,Color.FromArgb( 0x0  , 0x66 , 0x99)},
            {32 ,Color.FromArgb( 0x0  , 0x66 , 0xCC)},
            {33 ,Color.FromArgb( 0x0  , 0x66 , 0xFF)},
            {34 ,Color.FromArgb( 0x0  , 0x99 , 0x0)},
            {35 ,Color.FromArgb( 0x0  , 0x99 , 0x33)},
            {36 ,Color.FromArgb( 0x0  , 0x99 , 0x66)},
            {37 ,Color.FromArgb( 0x0  , 0x99 , 0x99)},
            {38 ,Color.FromArgb( 0x0  , 0x99 , 0xCC)},
            {39 ,Color.FromArgb( 0x0  , 0x99 , 0xFF)},
            {40 ,Color.FromArgb( 0x0  , 0xCC , 0x0)},
            {41 ,Color.FromArgb( 0x0  , 0xCC , 0x33)},
            {42 ,Color.FromArgb( 0x0  , 0xCC , 0x66)},
            {43 ,Color.FromArgb( 0x0  , 0xCC , 0x99)},
            {44 ,Color.FromArgb( 0x0  , 0xCC , 0xCC)},
            {45 ,Color.FromArgb( 0x0  , 0xCC , 0xFF)},
            {46 ,Color.FromArgb( 0x0  , 0xFF , 0x0)},
            {47 ,Color.FromArgb( 0x0  , 0xFF , 0x33)},
            {48 ,Color.FromArgb( 0x0  , 0xFF , 0x66)},
            {49 ,Color.FromArgb( 0x0  , 0xFF , 0x99)},
            {50 ,Color.FromArgb( 0x0  , 0xFF , 0xCC)},
            {51 ,Color.FromArgb( 0x0  , 0xFF , 0xFF)},
            {52 ,Color.FromArgb( 0x33 , 0x0  , 0x0)},
            {53 ,Color.FromArgb( 0x33 , 0x0  , 0x33)},
            {54 ,Color.FromArgb( 0x33 , 0x0  , 0x66)},
            {55 ,Color.FromArgb( 0x33 , 0x0  , 0x99)},
            {56 ,Color.FromArgb( 0x33 , 0x0  , 0xCC)},
            {57 ,Color.FromArgb( 0x33 , 0x0  , 0xFF)},
            {58 ,Color.FromArgb( 0x33 , 0x33 , 0x0)},
            {59 ,Color.FromArgb( 0x33 , 0x33 , 0x33)},
            {60 ,Color.FromArgb( 0x33 , 0x33 , 0x66)},
            {61 ,Color.FromArgb( 0x33 , 0x33 , 0x99)},
            {62 ,Color.FromArgb( 0x33 , 0x33 , 0xCC)},
            {63 ,Color.FromArgb( 0x33 , 0x33 , 0xFF)},
            {64 ,Color.FromArgb( 0x33 , 0x66 , 0x0)},
            {65 ,Color.FromArgb( 0x33 , 0x66 , 0x33)},
            {66 ,Color.FromArgb( 0x33 , 0x66 , 0x66)},
            {67 ,Color.FromArgb( 0x33 , 0x66 , 0x99)},
            {68 ,Color.FromArgb( 0x33 , 0x66 , 0xCC)},
            {69 ,Color.FromArgb( 0x33 , 0x66 , 0xFF)},
            {70 ,Color.FromArgb( 0x33 , 0x99 , 0x0)},
            {71 ,Color.FromArgb( 0x33 , 0x99 , 0x33)},
            {72 ,Color.FromArgb( 0x33 , 0x99 , 0x66)},
            {73 ,Color.FromArgb( 0x33 , 0x99 , 0x99)},
            {74 ,Color.FromArgb( 0x33 , 0x99 , 0xCC)},
            {75 ,Color.FromArgb( 0x33 , 0x99 , 0xFF)},
            {76 ,Color.FromArgb( 0x33 , 0xCC , 0x0)},
            {77 ,Color.FromArgb( 0x33 , 0xCC , 0x33)},
            {78 ,Color.FromArgb( 0x33 , 0xCC , 0x66)},
            {79 ,Color.FromArgb( 0x33 , 0xCC , 0x99)},
            {80 ,Color.FromArgb( 0x33 , 0xCC , 0xCC)},
            {81 ,Color.FromArgb( 0x33 , 0xCC , 0xFF)},
            {82 ,Color.FromArgb( 0x33 , 0xFF , 0x0)},
            {83 ,Color.FromArgb( 0x33 , 0xFF , 0x33)},
            {84 ,Color.FromArgb( 0x33 , 0xFF , 0x66)},
            {85 ,Color.FromArgb( 0x33 , 0xFF , 0x99)},
            {86 ,Color.FromArgb( 0x33 , 0xFF , 0xCC)},
            {87 ,Color.FromArgb( 0x33 , 0xFF , 0xFF)},
            {88 ,Color.FromArgb( 0x66 , 0x0  , 0x0)},
            {89 ,Color.FromArgb( 0x66 , 0x0  , 0x33)},
            {90 ,Color.FromArgb( 0x66 , 0x0  , 0x66)},
            {91 ,Color.FromArgb( 0x66 , 0x0  , 0x99)},
            {92 ,Color.FromArgb( 0x66 , 0x0  , 0xCC)},
            {93 ,Color.FromArgb( 0x66 , 0x0  , 0xFF)},
            {94 ,Color.FromArgb( 0x66 , 0x33 , 0x0)},
            {95 ,Color.FromArgb( 0x66 , 0x33 , 0x33)},
            {96 ,Color.FromArgb( 0x66 , 0x33 , 0x66)},
            {97 ,Color.FromArgb( 0x66 , 0x33 , 0x99)},
            {98 ,Color.FromArgb( 0x66 , 0x33 , 0xCC)},
            {99 ,Color.FromArgb( 0x66 , 0x33 , 0xFF)},
            {100,Color.FromArgb( 0x66 , 0x66 , 0x0)},
            {101,Color.FromArgb( 0x66 , 0x66 , 0x33)},
            {102,Color.FromArgb( 0x66 , 0x66 , 0x66)},
            {103,Color.FromArgb( 0x66 , 0x66 , 0x99)},
            {104,Color.FromArgb( 0x66 , 0x66 , 0xCC)},
            {105,Color.FromArgb( 0x66 , 0x66 , 0xFF)},
            {106,Color.FromArgb( 0x66 , 0x99 , 0x0)},
            {107,Color.FromArgb( 0x66 , 0x99 , 0x33)},
            {108,Color.FromArgb( 0x66 , 0x99 , 0x66)},
            {109,Color.FromArgb( 0x66 , 0x99 , 0x99)},
            {110,Color.FromArgb( 0x66 , 0x99 , 0xCC)},
            {111,Color.FromArgb( 0x66 , 0x99 , 0xFF)},
            {112,Color.FromArgb( 0x66 , 0xCC , 0x0)},
            {113,Color.FromArgb( 0x66 , 0xCC , 0x33)},
            {114,Color.FromArgb( 0x66 , 0xCC , 0x66)},
            {115,Color.FromArgb( 0x66 , 0xCC , 0x99)},
            {116,Color.FromArgb( 0x66 , 0xCC , 0xCC)},
            {117,Color.FromArgb( 0x66 , 0xCC , 0xFF)},
            {118,Color.FromArgb( 0x66 , 0xFF , 0x0)},
            {119,Color.FromArgb( 0x66 , 0xFF , 0x33)},
            {120,Color.FromArgb( 0x66 , 0xFF , 0x66)},
            {121,Color.FromArgb( 0x66 , 0xFF , 0x99)},
            {122,Color.FromArgb( 0x66 , 0xFF , 0xCC)},
            {123,Color.FromArgb( 0x66 , 0xFF , 0xFF)},
            {124,Color.FromArgb( 0x99 , 0x0  , 0x0)},
            {125,Color.FromArgb( 0x99 , 0x0  , 0x33)},
            {126,Color.FromArgb( 0x99 , 0x0  , 0x66)},
            {127,Color.FromArgb( 0x99 , 0x0  , 0x99)},
            {128,Color.FromArgb( 0x99 , 0x0  , 0xCC)},
            {129,Color.FromArgb( 0x99 , 0x0  , 0xFF)},
            {130,Color.FromArgb( 0x99 , 0x33 , 0x0)},
            {131,Color.FromArgb( 0x99 , 0x33 , 0x33)},
            {132,Color.FromArgb( 0x99 , 0x33 , 0x66)},
            {133,Color.FromArgb( 0x99 , 0x33 , 0x99)},
            {134,Color.FromArgb( 0x99 , 0x33 , 0xCC)},
            {135,Color.FromArgb( 0x99 , 0x33 , 0xFF)},
            {136,Color.FromArgb( 0x99 , 0x66 , 0x0)},
            {137,Color.FromArgb( 0x99 , 0x66 , 0x33)},
            {138,Color.FromArgb( 0x99 , 0x66 , 0x66)},
            {139,Color.FromArgb( 0x99 , 0x66 , 0x99)},
            {140,Color.FromArgb( 0x99 , 0x66 , 0xCC)},
            {141,Color.FromArgb( 0x99 , 0x66 , 0xFF)},
            {142,Color.FromArgb( 0x99 , 0x99 , 0x0)},
            {143,Color.FromArgb( 0x99 , 0x99 , 0x33)},
            {144,Color.FromArgb( 0x99 , 0x99 , 0x66)},
            {145,Color.FromArgb( 0x99 , 0x99 , 0x99)},
            {146,Color.FromArgb( 0x99 , 0x99 , 0xCC)},
            {147,Color.FromArgb( 0x99 , 0x99 , 0xFF)},
            {148,Color.FromArgb( 0x99 , 0xCC , 0x0)},
            {149,Color.FromArgb( 0x99 , 0xCC , 0x33)},
            {150,Color.FromArgb( 0x99 , 0xCC , 0x66)},
            {151,Color.FromArgb( 0x99 , 0xCC , 0x99)},
            {152,Color.FromArgb( 0x99 , 0xCC , 0xCC)},
            {153,Color.FromArgb( 0x99 , 0xCC , 0xFF)},
            {154,Color.FromArgb( 0x99 , 0xFF , 0x0)},
            {155,Color.FromArgb( 0x99 , 0xFF , 0x33)},
            {156,Color.FromArgb( 0x99 , 0xFF , 0x66)},
            {157,Color.FromArgb( 0x99 , 0xFF , 0x99)},
            {158,Color.FromArgb( 0x99 , 0xFF , 0xCC)},
            {159,Color.FromArgb( 0x99 , 0xFF , 0xFF)},
            {160,Color.FromArgb( 0xCC , 0x0  , 0x0)},
            {161,Color.FromArgb( 0xCC , 0x0  , 0x33)},
            {162,Color.FromArgb( 0xCC , 0x0  , 0x66)},
            {163,Color.FromArgb( 0xCC , 0x0  , 0x99)},
            {164,Color.FromArgb( 0xCC , 0x0  , 0xCC)},
            {165,Color.FromArgb( 0xCC , 0x0  , 0xFF)},
            {166,Color.FromArgb( 0xCC , 0x33 , 0x0)},
            {167,Color.FromArgb( 0xCC , 0x33 , 0x33)},
            {168,Color.FromArgb( 0xCC , 0x33 , 0x66)},
            {169,Color.FromArgb( 0xCC , 0x33 , 0x99)},
            {170,Color.FromArgb( 0xCC , 0x33 , 0xCC)},
            {171,Color.FromArgb( 0xCC , 0x33 , 0xFF)},
            {172,Color.FromArgb( 0xCC , 0x66 , 0x0)},
            {173,Color.FromArgb( 0xCC , 0x66 , 0x33)},
            {174,Color.FromArgb( 0xCC , 0x66 , 0x66)},
            {175,Color.FromArgb( 0xCC , 0x66 , 0x99)},
            {176,Color.FromArgb( 0xCC , 0x66 , 0xCC)},
            {177,Color.FromArgb( 0xCC , 0x66 , 0xFF)},
            {178,Color.FromArgb( 0xCC , 0x99 , 0x0)},
            {179,Color.FromArgb( 0xCC , 0x99 , 0x33)},
            {180,Color.FromArgb( 0xCC , 0x99 , 0x66)},
            {181,Color.FromArgb( 0xCC , 0x99 , 0x99)},
            {182,Color.FromArgb( 0xCC , 0x99 , 0xCC)},
            {183,Color.FromArgb( 0xCC , 0x99 , 0xFF)},
            {184,Color.FromArgb( 0xCC , 0xCC , 0x0)},
            {185,Color.FromArgb( 0xCC , 0xCC , 0x33)},
            {186,Color.FromArgb( 0xCC , 0xCC , 0x66)},
            {187,Color.FromArgb( 0xCC , 0xCC , 0x99)},
            {188,Color.FromArgb( 0xCC , 0xCC , 0xCC)},
            {189,Color.FromArgb( 0xCC , 0xCC , 0xFF)},
            {190,Color.FromArgb( 0xCC , 0xFF , 0x0)},
            {191,Color.FromArgb( 0xCC , 0xFF , 0x33)},
            {192,Color.FromArgb( 0xCC , 0xFF , 0x66)},
            {193,Color.FromArgb( 0xCC , 0xFF , 0x99)},
            {194,Color.FromArgb( 0xCC , 0xFF , 0xCC)},
            {195,Color.FromArgb( 0xCC , 0xFF , 0xFF)},
            {196,Color.FromArgb( 0xFF , 0x0  , 0x0)},
            {197,Color.FromArgb( 0xFF , 0x0  , 0x33)},
            {198,Color.FromArgb( 0xFF , 0x0  , 0x66)},
            {199,Color.FromArgb( 0xFF , 0x0  , 0x99)},
            {200,Color.FromArgb( 0xFF , 0x0  , 0xCC)},
            {201,Color.FromArgb( 0xFF , 0x0  , 0xFF)},
            {202,Color.FromArgb( 0xFF , 0x33 , 0x0)},
            {203,Color.FromArgb( 0xFF , 0x33 , 0x33)},
            {204,Color.FromArgb( 0xFF , 0x33 , 0x66)},
            {205,Color.FromArgb( 0xFF , 0x33 , 0x99)},
            {206,Color.FromArgb( 0xFF , 0x33 , 0xCC)},
            {207,Color.FromArgb( 0xFF , 0x33 , 0xFF)},
            {208,Color.FromArgb( 0xFF , 0x66 , 0x0)},
            {209,Color.FromArgb( 0xFF , 0x66 , 0x33)},
            {210,Color.FromArgb( 0xFF , 0x66 , 0x66)},
            {211,Color.FromArgb( 0xFF , 0x66 , 0x99)},
            {212,Color.FromArgb( 0xFF , 0x66 , 0xCC)},
            {213,Color.FromArgb( 0xFF , 0x66 , 0xFF)},
            {214,Color.FromArgb( 0xFF , 0x99 , 0x0)},
            {215,Color.FromArgb( 0xFF , 0x99 , 0x33)},
            {216,Color.FromArgb( 0xFF , 0x99 , 0x66)},
            {217,Color.FromArgb( 0xFF , 0x99 , 0x99)},
            {218,Color.FromArgb( 0xFF , 0x99 , 0xCC)},
            {219,Color.FromArgb( 0xFF , 0x99 , 0xFF)},
            {220,Color.FromArgb( 0xFF , 0xCC , 0x0)},
            {221,Color.FromArgb( 0xFF , 0xCC , 0x33)},
            {222,Color.FromArgb( 0xFF , 0xCC , 0x66)},
            {223,Color.FromArgb( 0xFF , 0xCC , 0x99)},
            {224,Color.FromArgb( 0xFF , 0xCC , 0xCC)},
            {225,Color.FromArgb( 0xFF , 0xCC , 0xFF)},
            {226,Color.FromArgb( 0xFF , 0xFF , 0x0)},
            {227,Color.FromArgb( 0xFF , 0xFF , 0x33)},
            {228,Color.FromArgb( 0xFF , 0xFF , 0x66)},
            {229,Color.FromArgb( 0xFF , 0xFF , 0x99)},
            {230,Color.FromArgb( 0xFF , 0xFF , 0xCC)},
            {231,Color.FromArgb( 0xFF , 0xFF , 0xFF)}


        };
   }
}
