﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   [Flags]
   public enum FontStyle
   {
      Normal = 0,
      Bold = 1,
      CrossedOut = 2,
      Underlined = 4,
      Italic = 8,
      Inverted = 16,
      FlashingInverted = 32,
      FlashingBackground = 64,
      Proportional = 128,
   }
}
