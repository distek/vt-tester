﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class ReferencedObject : INotifyPropertyChanged
   {
      // ng: I don't think we need to notify anybody of this object id changing since it should never change (after init)
      public int ObjectId { get; set; }

      private int _xLocation;
      public int XLocation
      {
         get { return _xLocation; }
         set { _xLocation = value; OnPropertyChanged(nameof(XLocation)); }
      }

      private int _yLocation;
      public int YLocation
      {
         get { return _yLocation; }
         set { _yLocation = value; OnPropertyChanged(nameof(YLocation)); }
      }

      public ReferencedObject()
      {
      }

      public ReferencedObject(int objectId)
      {
         ObjectId = objectId;
      }

      public event PropertyChangedEventHandler PropertyChanged;
      protected void OnPropertyChanged(string name)
      {
         PropertyChangedEventHandler handler = PropertyChanged;
         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(name));
         }
      }
   }

   public class ReferencedMacro
   {
      public int EventId { get; set; }
      public int MacroId { get; set; }

      public ReferencedMacro()
      {
      }
   }
}
