﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface INonVolatileMemory
   {
      /// <summary>
      /// Saves an object pool to non-volatile memory
      /// </summary>
      /// <param name="objectPool"></param>
      /// <returns>TRUE if the object was saved, FALSE if not</returns>
      bool SaveObjectPool(ObjectPool objectPool);

      /// <summary>
      /// Loads an object pool from non-volatile memory
      /// </summary>
      /// <param name="workingSetMasterName"></param>
      /// <param name="versionLabel"></param>
      /// <returns></returns>
      ObjectPool LoadObjectPool(string workingSetMasterName, string versionLabel);

      /// <summary>
      /// Deletes an object pool from non-volatile memory
      /// </summary>
      /// <param name="workingSetMasterName"></param>
      /// <param name="versionLabel"></param>
      /// <returns>TRUE if the version was found, FALSE otherwise</returns>
      bool DeleteObjectPoolVersion(string workingSetMasterName, string versionLabel);

      /// <summary>
      /// Queries the system for what version labels are saved for a working set master
      /// </summary>
      /// <param name="workingSetMasterName"></param>
      /// <returns></returns>
      IEnumerable<string> GetVersionLabels(string workingSetMasterName);
   }
}
