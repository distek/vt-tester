﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface IErrorHandler
   {
      //void RaiseError(Exception e);
      void RaiseError<T>(T error);
      void RaiseError(ObjectPool op, int objectid);
   }
}
