﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Distek.Vt
{
   public enum WorkingSetState
   {
      // When the client connects with a working set master message
      New,
      // When the pool transfer has completed and the pool needs to be parsed
      PoolTransferred,
      // When the pool has been parsed
      ObjectPoolReady,
      // When problems
      Error,
      // After the pool has been deleted - client is disconnected (is this expected or unexpected disconnect?)
      ObjectPoolDeleted,
   }

    public class VtClient : INotifyPropertyChanged
    {
      public DateTime LastSeen { get; set; }

      public int MasterSourceAddress { get; set; }

      public ObjectPool ObjectPool { get; set; }

      /// <summary>
      /// NAME of the working set master
      /// </summary>
      public string Name { get; set; }
      /// <summary>
      /// NAMEs of the members of this working set
      /// </summary>
      public IEnumerable<string> MemberNames { get; set; }

      public int ActiveMaskId { get; set; }
      public int ActiveSoftKeyMaskId { get; set; }

      private WorkingSetState _state;
      public WorkingSetState State
      {
         get { return _state; }
         set { _state = value; OnPropertyChanged(nameof(State)); }
      }

      public VtClient(int masterAddress)
      {
         this.MasterSourceAddress = masterAddress;
      }

      // it makes more sense to me to have this here, but it's defined on the
      // working set object, so i'll leave it there for now.  maybe add
      // a convenience method here for it
      //private int _activeMaskId { get; set; }
      //public int ActiveMaskId
      //{
      //   get { return _activeMaskId; }
      //   set { _activeMaskId = value; OnPropertyChanged(nameof(ActiveMaskId)); }
      //}

      public event PropertyChangedEventHandler PropertyChanged;

      protected void OnPropertyChanged(string name)
      {
         PropertyChangedEventHandler handler = PropertyChanged;
         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(name));
         }
      }
    }
}
