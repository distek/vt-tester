﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class UnexpectedWorkingSetShutdownError
   {
      public string Type { get { return "unexpected-shutdown"; } }
      public int MasterSourceAddress { get; set; }
      public string Message { get; set; }

      public UnexpectedWorkingSetShutdownError()
      {
      }
   }
}
