﻿using Distek.Vt.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class AppSettings
   {
      public int SoftKeyCount { get; set; }
      /// <summary>
      /// Width and Height of displayed softkeys
      /// </summary>
      public int SoftKeySize { get; set; }
      /// <summary>
      /// Amount to scale the displayed UI by
      /// </summary>
      public float Scale { get; set; }

      /// <summary>
      /// Language and units settings
      /// </summary>
      public LanguageResponse LocalizationSettings { get; set; }

      public AppSettings()
      {
         LocalizationSettings = new LanguageResponse();
      }
   }

   public interface ISettings
   {
      AppSettings AppSettings { get; }
      IEnumerable<string> SupportedLanguages { get; }
      void Save();
   }
}
