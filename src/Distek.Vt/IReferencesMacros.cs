﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public interface IReferencesMacros
   {
      IList<ReferencedMacro> Macros { get; set; }
   }
}
