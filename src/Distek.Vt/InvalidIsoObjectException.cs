﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt
{
   public class InvalidIsoObjectException : Exception
   {
      public InvalidIsoObjectException(string message) : base(message)
      {
      }
   }
}
