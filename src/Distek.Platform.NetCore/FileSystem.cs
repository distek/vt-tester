﻿using System;
using Distek.Platform;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Distek.Platform.NetCore
{
    public class FileSystem : IFileSystem
    {
      public FileSystem(PlatformConfig config)
      {
         _config = config;
         if (Directory.Exists(_config.BaseDirectory) == false)
            Directory.CreateDirectory(_config.BaseDirectory);
      }

      public void CreateDirectory(string name)
      {
         Directory.CreateDirectory(Path.Combine(_config.BaseDirectory, name));
      }

      public void DeleteFile(string filename)
      {
         File.Delete(Path.Combine(_config.BaseDirectory, filename));
      }

      public bool FileExists(string filename)
      {
         return File.Exists(Path.Combine(_config.BaseDirectory, filename));
      }

      public string GetLocalPath(string filename)
      {
         return Path.Combine(_config.BaseDirectory, filename);
      }

      public IEnumerable<string> ListFiles(string directory)
      {
         return Directory.GetFiles(Path.Combine(_config.BaseDirectory, directory));
      }

      public string Newest(string directory)
      {
         return "";
      }

      public Stream ReadFile(string filename)
      {
         return File.OpenRead(Path.Combine(_config.BaseDirectory, filename));
      }

      public void WriteFile(string filename, Stream stream)
      {
         // rewind the stream to the start, otherwise we'll end up writing 0 bytes
         stream.Seek(0, SeekOrigin.Begin);
         var f = Path.Combine(_config.BaseDirectory, filename);
         if (File.Exists(f))
            File.Delete(f);
         using (var fileStream = File.OpenWrite(f))
         {
            stream.CopyTo(fileStream);
         }
      }

      public void AppendFile(string filename, Stream stream)
      {
         stream.Seek(0, SeekOrigin.Begin);
         var f = Path.Combine(_config.BaseDirectory, filename);
         using (var fileStream = File.OpenWrite(f))
         {
            stream.CopyTo(fileStream);
         }
      }
#if __IOS__
      //private readonly string _baseDir = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
#else
      //private readonly string _baseDir = "data";
#endif
      private readonly PlatformConfig _config;
   }
}
