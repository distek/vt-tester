﻿
#if USE_AUTOFAC
using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Platform.NetCore
{
    public class Installer : Module
    {
      protected override void Load(ContainerBuilder builder)
      {
         builder.RegisterType<FileSystem>().As<IFileSystem>();
      }
    }
}
#endif
