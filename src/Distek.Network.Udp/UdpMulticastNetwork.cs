﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net;
using Distek.Network;

namespace Distek.Network.Udp
{
   public class UdpMulticastNetwork : INetwork
   {
      string INetwork.Key
      {
         get { return "udp"; }
      }

      public UdpMulticastNetwork(UdpConfiguration config)
      {
         _socket = new UdpClient();
         _multicastAddress = IPAddress.Parse(config.RemoteAddress);
         _endPoint = new IPEndPoint(_multicastAddress, config.RemotePort);
      }

      void INetwork.Connect()
      {
         var localEndPoint = new IPEndPoint(IPAddress.Any, 25000);
         _socket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
         // Note: this isn't supported on osx or linux-arm
         _socket.Client.ExclusiveAddressUse = false;
         _socket.Client.Bind(localEndPoint);

         // if i set this, i don't get messages from other clients on the same machine
         // if i do set it, i don't see myself trying to claim my own address
			//_socket.MulticastLoopback = false;

         _socket.JoinMulticastGroup(_multicastAddress, 2);

      }

      async void INetwork.Send(CanMessage message)
      {
         //Console.WriteLine("TX: {0}", message.ToString());
         byte[] data = message.ToBytes();
         await _socket.SendAsync(data, data.Length, _endPoint);
      }

      async void INetwork.Receive(Action<CanMessage> handler)
      {
         await Task.Run(async () =>
         {
            while (true)
            {
               var recv = await _socket.ReceiveAsync();
               var msg = CanMessageExtensions.BytesToCanMessage(recv.Buffer);
               if (msg.SourceAddress == 0x80)
                  continue;
               //Console.WriteLine("RX: {0}", msg.ToString());
               handler(msg);
            }
         });
      }

      private UdpClient _socket;
      private IPAddress _multicastAddress;
      private IPEndPoint _endPoint;
   }
}
