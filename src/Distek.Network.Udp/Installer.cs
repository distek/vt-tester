﻿
#if USE_AUTOFAC
using Autofac;
using Distek.Network;

using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.Network.Udp
{
   public class Installer : Module
   {
      protected override void Load(ContainerBuilder builder)
      {
         // the ip address here should be the ip that messages are coming from (ie the current pc if you're sending messages from a test app)
         builder.RegisterInstance<UdpConfiguration>(new UdpConfiguration()
         {
            // vt on mac, ecu on pc
            //RemoteAddress = "192.168.200.18",
            // ad-hoc connection
            //RemoteAddress = "192.168.137.1",
            RemotePort = 25000,
            //LocalPort = 25001,
            // running sample ecu on mac, vt on pc
            //RemoteAddress = "192.168.200.62",
            //RemotePort = 25000,
            //LocalPort = 25001,
            // local p2p udp - doesnt work
            //RemoteAddress = "127.0.0.1",
            //LocalPort = 25001,
            //RemotePort = 25000,
            // multicast -- only uses remote port
            RemoteAddress = "239.0.0.222",
            //RemotePort = 25000,
            //LocalPort = 0,
         }).SingleInstance();

         builder.RegisterType<UdpMulticastNetwork>().As<INetwork>().SingleInstance();
      }
   }
}
#endif
