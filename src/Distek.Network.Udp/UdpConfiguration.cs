﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Network.Udp
{
   public class UdpConfiguration
   {
      /// <summary>
      /// IP address or hostname of the remote device
      /// </summary>
      public string RemoteAddress { get; set; }

      /// <summary>
      /// Port to connect to on the remote device
      /// </summary>
      public int RemotePort { get; set; }
   }
}
