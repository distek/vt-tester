﻿using System;
using Avalonia;
using Avalonia.Logging.Serilog;
using Distek.VtTester.ViewModels;
using Distek.VtTester.Views;

namespace Distek.VtTester
{
    class Program
    {
        static void Main(string[] args)
        {
            BuildAvaloniaApp().Start<MainWindow>(() => new MainWindowViewModel());
        }

        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .UseReactiveUI()
                .LogToDebug();
    }
}
