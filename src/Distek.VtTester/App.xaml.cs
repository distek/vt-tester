﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace Distek.VtTester
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
