﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Distek.BusLog;
using Distek.BusLog.File;
using Distek.DataLink;
using Distek.Network;
using Distek.Network.Pcan;
using Distek.Network.Udp;
using Distek.Platform;
using Distek.Platform.Impl;
using Distek.Platform.NetCore;
using Distek.Vt;
using Distek.Vt.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;

namespace Distek.VtClient.Web
{
   public class Startup
   {
      //public IConfigurationRoot Configuration { get; }

      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      // This method gets called by the runtime. Use this method to add services to the services.
      //public IServiceProvider ConfigureServices(IServiceCollection services)
      public void ConfigureServices(IServiceCollection services)
      {
         services.AddMvc();
         services.AddCors();

         services.AddSingleton<IConnection, DefaultConnection>();
         services.AddSingleton<BusLoad>();
         services.AddSingleton<PeriodicValueChange>();
         services.AddSingleton<VtClient>();

         var adapter = "udp";
         if (adapter == "pcan")
         {
            services.AddSingleton<INetwork, PcanNetwork>();
         }
         else if (adapter == "udp")
         {
            services.AddSingleton<INetwork, UdpMulticastNetwork>();
            var config = new UdpConfiguration()
            {
               RemotePort = 25000,
               RemoteAddress = "239.0.0.222"
            };
            services.AddSingleton<UdpConfiguration>(config);
         }
         else if (adapter == "vc")
         {
            Console.WriteLine("VC adapter is weird, build for 32-bit");
            //services.AddSingleton<INetwork, ValueCanNetwork>(Reuse.Singleton);
         }

         services.AddSingleton<INameTable, NameTable>();
         services.AddSingleton<SessionManager>();
         services.AddSingleton<MessageHandler, RequestForAddressClaimedHandler>();
         services.AddSingleton<MessageHandler, AddressClaimedHandler>();

         services.AddSingleton<MessageHandler, TransportClearToSendHandler>();
         services.AddSingleton<MessageHandler, TransportEndOfMessageHandler>();
         services.AddSingleton<MessageHandler, ExtendedDataPacketOffsetHandler>();
         services.AddSingleton<MessageHandler, ExtendedDataTransferHandler>();
         services.AddSingleton<MessageHandler, ExtendedRequestToSendHandler>();
         services.AddSingleton<MessageHandler, ExtendedTransportAbortHandler>();

         services.AddSingleton<ISerialization, Serializer>();
         services.AddSingleton<IDeserialization, ObjectPoolReader>();
         services.AddSingleton<VirtualTerminal>();

         services.AddSingleton<IRepeatedTask, AddressClaimTask>();
         services.AddSingleton<WorkingSetMaintenanceTask>();
         services.AddSingleton<IRepeatedTask>(f => f.GetService<WorkingSetMaintenanceTask>());
         //services.AddSingleton<IRepeatedTask>(f => services.Resolve<WorkingSetMaintenanceTask>());
         //services.AddSingleton<IRepeatedTask, WorkingSetMaintenanceTask>();

         var logger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .WriteTo.Trace()
            .CreateLogger();
         services.AddSingleton<Serilog.ILogger>(logger);

         var progressSubject = new Subject<int>();
         services.AddSingleton<Subject<int>>(progressSubject);

         services.AddSingleton<IBusLogger, FileLog>();
         services.AddSingleton<IFileSystem, FileSystem>();
         services.AddSingleton<PlatformConfig>(new PlatformConfig() { BaseDirectory = "data" });
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IHostingEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }

         app.UseMvc();

         app.UseCors(builder =>
            builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
         );
      }
   }
}
