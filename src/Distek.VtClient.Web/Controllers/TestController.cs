﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Distek.Platform;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Distek.VtClient.Web.Controllers
{
   [Produces("application/json")]
   [Route("api/Test")]
   public class TestController : Controller
   {
      public TestController(VtClient client)
      {
         _client = client;
         _steps = new[]
         {
            new TestStep() { Id = 0, Name = "Claim Address", FailureMessage = "Failed to claim an address", Action = () => _client.StartTestClient() },
            new TestStep() { Id = 1, Name = "Find VT", FailureMessage = "Did not detect a display on the bus", Action = () => true },
            new TestStep() { Id = 2, Name = "Get VT Metrics", FailureMessage = "Could not retrieve VT metrics", Action = () => true },
            new TestStep() { Id = 3, Name = "Upload Object Pool", FailureMessage = "Object pool upload failed", Action = () => true },
            new TestStep() { Id = 4, Name = "Change Numeric Value", FailureMessage = "Could not send change numeric value message to display", Action = () => false },
         };
      }

      [HttpGet]
      public IEnumerable<TestStep> Get()
      {
         return _steps;
      }

      [HttpPost]
      // Starts a specific test step
      public TestResult Post(int id)
      {
         var a = _steps.FirstOrDefault(s => s.Id == id);
         if(a != default(TestStep))
         {
            var passed = a.Action();
            return new TestResult()
            {
               Passed = passed,
               Message = a.FailureMessage,
            };
         }
         else
         {
            return new TestResult() { Passed = false, Message = "Unknown test id" };
         }
      }

      private readonly VtClient _client;
      private IEnumerable<TestStep> _steps;
   }

   public class TestResult
   {
      public bool Passed { get; set; }
      public string Message { get; set; }
   }

   public class TestStep
   {
      // this is also the order to display tests in
      public int Id { get; set; }
      public string Name { get; set; }
      public bool Result { get; set; }
      public string FailureMessage { get; set; }
      [JsonIgnore]
      public Func<bool> Action { get; set; }
   }
}