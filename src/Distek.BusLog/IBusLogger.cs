﻿using Distek.Network;
using System;

// TODO: do i even need this interface?  can i just create a message handler and log messages?
// it might be nice to be able to stop and start the log
namespace Distek.BusLog
{
   /// <summary>
   /// Mesage direction signifier
   /// </summary>
   public enum MessageDirection
   {
      /// <summary>
      /// Transmit message
      /// </summary>
      TX,
      /// <summary>
      /// Received message
      /// </summary>
      RX
   }

   /// <summary>
   /// Interface for bus logging
   /// </summary>
   public interface IBusLogger
   {
      /// <summary>
      /// Start a new bus logging session
      /// </summary>
      /// <param name="listener"></param>
      void Start(Action<string> listener);

      /// <summary>
      /// Stop the current logging session
      /// </summary>
      void Stop();

      /// <summary>
      /// Logs a single message
      /// </summary>
      /// <param name="direction"></param>
      /// <param name="message"></param>
      void Log(MessageDirection direction, CanMessage message);

      /// <summary>
      /// Saves current session to disk
      /// </summary>
      void Save();
   }
}
