﻿
#if USE_AUTOFAC
using Autofac;
using Serilog;

namespace Distek.Platform.Impl
{
   public class Installer : Module
   {
      protected override void Load(ContainerBuilder builder)
      {
         builder.RegisterType<DefaultConnection>().As<IConnection>().SingleInstance();
      }
   }
}
#endif
