﻿using Distek.BusLog;
using Distek.DataLink;
using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Vt;
using Distek.Vt.CommandHandlers;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Platform.Impl
{
   /// <summary>
   /// <see cref="IConnection"/> implementation for using a raw connection to a CAN bus
   /// </summary>
   public class DefaultConnection : IConnection
   {
      /// <summary>
      /// DefaultConnection constructor
      /// </summary>
      /// <param name="handlers">Collection of message handlers to use with this connection</param>
      public DefaultConnection(INetwork network, IEnumerable<MessageHandler> handlers, IBusLogger busLog,
         ISerialization serialization, ILogger logger, INameTable nameTable, SessionManager sessionManager)
      {
         _network = network;
         _handlers = handlers.ToList();
         _busLog = busLog;
         _serialization = serialization;
         _logger = logger;
         _incoming = new ConcurrentQueue<CanMessage>();
         _nameTable = nameTable;
         _sessionManager = sessionManager;

         var s = new Subject<CanMessage>();
         s.Subscribe(
            async x => await ProcessIncomingMessages(x),
            () => Console.WriteLine("Sequence completed")
         );

         // Might be a good idea to connect the network elsewhere, but the is the last possible place it can be done
         _network.Connect();
         _network.Receive((msg) =>
         {
            s.OnNext(msg);
         });
      }

      bool IConnection.Send(CanMessage message)
      {
         return SendMessage(message);
      }

      void IConnection.AddHandler(MessageHandler handler)
      {
         _handlers.Add(handler);
      }

      void IConnection.Inject(CanMessage message)
      {
         _incoming.Enqueue(message);
      }

      private async Task ProcessIncomingMessages(CanMessage msg)
      {
         _busLog.Log(MessageDirection.RX, msg);

         // this doesn't work with how i'm doing macros - not sure what address to send a macro request from
         if (msg.SourceAddress == _nameTable.SourceAddress)
            return;

         if (_nameTable.CanNames.ContainsKey(msg.SourceAddress))
            msg.SourceCanName = _nameTable.CanNames[msg.SourceAddress].ToString();

         // Handle TP/ETP messages before they get to the VT handlers
         // Passing msg by ref so that when the session manager creates a long message we can pass
         // it through the vt message handlers
         var tpResponse = _sessionManager.ProcessIncoming(ref msg);
         if (tpResponse != null)
            this.SendMessage((CanMessage)tpResponse);

         // Not using readers here yet - they're used in the web handlers to
         // send messages to the browser.  It'd be good to have that working here
         // though and send parsed messages to objects

         foreach (var match in _handlers.Where(h => h.Filter(msg)))
         {
            //_logger.Debug("Calling handler {0}", match.GetType().Name);
            var response = await match.AsyncHandler(msg);
            if (response is CanMessage canMessage)
               this.SendMessage(canMessage);
         }
      }

      // should this queue messages??
      private bool SendMessage(CanMessage message)
      {
         message.Header = message.Header | (uint)_nameTable.SourceAddress;
         // If the messasge is already serialized, don't serialize it
         if (message.Data == null)
            message.Data = _serialization.SerializeCanMessage(message);
         // If this is a TP/ETP sequence, create the session and send the RTS to start it
         if (message.Data.Length > 8)
         {
            message = _sessionManager.CreateTransmit(message);
            // this is really annoying to have to do it twice - figure out how to refactor this
            message.Header = message.Header | (uint)_nameTable.SourceAddress;
            // removed this for etp sessions to send, probably shouldn't do that
            // yes, need it for tp session
            if (message.Data == null)
               message.Data = _serialization.SerializeCanMessage(message);
         }
         if (message.Data == null)
            throw new Exception(string.Format("Error parsing CAN message: {0}", message.ToString()));

         _busLog.Log(MessageDirection.TX, message);

         _network.Send(message);
         return true;
      }

      private INetwork _network;
      // use container IIndex here if possible, not sure if we'd be able to filter correctly with that
      private IList<MessageHandler> _handlers;
      private ISerialization _serialization;
      private ILogger _logger;
      private static ConcurrentQueue<CanMessage> _incoming;
      private INameTable _nameTable;
      private SessionManager _sessionManager;
      private IBusLogger _busLog;
   }
}
