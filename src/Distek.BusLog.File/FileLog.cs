﻿using System;
using System.Linq;
using Distek.Network;
using Distek.Platform;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Collections.Concurrent;

namespace Distek.BusLog.File
{
   public class FileLog : IBusLogger
   {
      public FileLog(IFileSystem fileSystem)
      {
         _fileSystem = fileSystem;
         _items = new ConcurrentQueue<string>();
         _start = DateTime.Now;
      }

      void IBusLogger.Log(MessageDirection direction, CanMessage message)
      {
         var timeDelta = DateTime.Now - _start;
         var time = timeDelta.TotalSeconds.ToString("000000.000");
         var s = string.Format("{0}  1   {1}    {2}", time, direction.ToString(), message.ToString());
         _items.Enqueue(s);
      }

      void IBusLogger.Save()
      {
         var stream = new MemoryStream();
         var writer = new StreamWriter(stream);
         foreach (var i in _items)
         {
            var m = string.Empty;
            if (_items.TryDequeue(out m))
               writer.WriteLine(m);
         }
         _fileSystem.AppendFile("buslog.txt", stream);
      }

      void IBusLogger.Start(Action<string> listener)
      {
      }

      void IBusLogger.Stop()
      {
      }

      private IFileSystem _fileSystem;
      private ConcurrentQueue<string> _items;
      private DateTime _start;
   }
}
