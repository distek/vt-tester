﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Impl
{
   public class PoolValidation : IPoolValidation
   {
      public PoolValidation(ILogger log)
      {
         _log = log;
      }

      ObjectPoolParseError IPoolValidation.PoolIsValid(IEnumerable<IsoVtObject> objectPool)
      {
         try
         {
            // returning 'any other error' for this - would probably mean the transfer didn't work correctly
            if (objectPool == null)
               return new ObjectPoolParseError() { ErrorCode = 4 };

            // verify the object pool doesn't have any references to objects that don't exist
            foreach (var o in objectPool.OfType<IReferencesObjects>())
            {
               var childIds = o.ChildObjects.Select(c => c.ObjectId).Where(x => x != 0xFFFF);
               var m = childIds.Except(objectPool.Select(p => p.ObjectId));
               // TODO: there doesn't seem to be a way to return multiple errors in the end of object pool response
               if (m.Count() > 0)
                  return new ObjectPoolParseError() { ErrorCode = 1, ParentObjectId = ((IsoVtObject)o).ObjectId, FaultyObjectId = m.First() };
            }
            // verify there are no references to missing macros
            foreach (var o in objectPool.OfType<IReferencesMacros>())
            {
               var m = o.Macros.Select(c => c.MacroId).Except(objectPool.Select(p => p.ObjectId));
               if (m.Count() > 0)
                  return new ObjectPoolParseError() { ErrorCode = 1, ParentObjectId = ((IsoVtObject)o).ObjectId, FaultyObjectId = m.First() };
            }
            return null;
         }
         catch (Exception e)
         {
            _log.Error(e, "Exception validating object pool");
            // return any other error
            return new ObjectPoolParseError() { ErrorCode = 0xFFFF, ParentObjectId = 0, FaultyObjectId = 0 };
         }
      }

      private ILogger _log;
   }
}
