﻿using Distek.Platform;
using Distek.Vt.Commands;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.Impl
{
   public class Settings : ISettings
   {
      public AppSettings AppSettings => _settings;

      public IEnumerable<string> SupportedLanguages
      {
         get { return new[] { "en", "de", "fr" }; }
      }

      public Settings(IFileSystem fs)
      {
         _fs = fs;
         if (_fs.FileExists("settings.json"))
         {
            var x = new JsonSerializer();
            using (var sr = new StreamReader(_fs.ReadFile("settings.json")))
            {
               using (var reader = new JsonTextReader(sr))
               {
                  _settings = x.Deserialize<AppSettings>(reader);
               }
            }
         }
         else
         {
            _settings = new AppSettings();
         }
      }

      void ISettings.Save()
      {
         var content = JsonConvert.SerializeObject(_settings);
         _fs.WriteFile("settings.json", new MemoryStream(Encoding.UTF8.GetBytes(content)));
      }

      private readonly IFileSystem _fs;
      private AppSettings _settings;

   }

}
