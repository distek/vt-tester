﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Distek.Vt;

namespace Distek.Vt.Impl
{
   /// <summary>
   /// Reads an object pool from an IOP stream
   /// </summary>
   public class ObjectPoolReader : Distek.Vt.IDeserialization
   {
      IEnumerable<IsoVtObject> IDeserialization.DeserializeObjectPool(Stream s)
      {
         var objects = new List<IsoVtObject>();
         using (var reader = new BinaryReader(s))
         {
            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
               var objectId = reader.ReadUInt16();
               var objectType = (IsobusObjectType)reader.ReadByte();
               if (_typeMap.ContainsKey(objectType))
               {
                  //Console.WriteLine(string.Format("Parsing object type {0} with id {1}", objectType.ToString(), objectId));
                  var t = _typeMap[objectType];
                  var parser = Activator.CreateInstance(t, objectId) as IsoVtObject;
                  if (parser != null)
                  {
                     var obj = parser.Read(reader, objectId);
                     // If we already have this object, it has to be replaced
                     var existing = objects.FirstOrDefault(x => x.ObjectId == obj.ObjectId);
                     if (existing != default(IsoVtObject))
                     {
                        objects.Remove(existing);
                     }
                     objects.Add(obj);
                  }
               }
               else
               {
                  throw new ObjectPoolParseException(string.Format("Unable to parse object type {0}", objectType.ToString()))
                  {
                     InvalidObjectId = objectId,
                     InvalidObjectType = objectType
                  };
               }
            }
         }
         return objects;
      }

      private Dictionary<IsobusObjectType, Type> _typeMap = new Dictionary<IsobusObjectType, Type>()
      {
         { IsobusObjectType.AlarmMask, typeof(Objects.AlarmMask) },
         { IsobusObjectType.Button, typeof(Objects.Button) },
         { IsobusObjectType.Container, typeof(Objects.Container) },
         { IsobusObjectType.DataMask, typeof(Objects.DataMask) },
         { IsobusObjectType.FillAttributes, typeof(Objects.FillAttributes) },
         { IsobusObjectType.FontAttributes, typeof(Objects.FontAttributes) },
         { IsobusObjectType.InputAttributes, typeof(Objects.InputAttributes) },
         { IsobusObjectType.SoftKey, typeof(Objects.Key) },
         { IsobusObjectType.LineAttributes, typeof(Objects.LineAttributes) },
         { IsobusObjectType.Macro, typeof(Objects.Macro) },
         { IsobusObjectType.NumberVariable, typeof(Objects.NumberVariable) },
         { IsobusObjectType.ObjectPointer, typeof(Objects.ObjectPointer) },
         { IsobusObjectType.OutputArchedBarGraph, typeof(Objects.OutputArchedBarGraph) },
         { IsobusObjectType.OutputEllipse, typeof(Objects.OutputEllipse) },
         { IsobusObjectType.OutputLine, typeof(Objects.OutputLine) },
         { IsobusObjectType.OutputLinearBarGraph, typeof(Objects.OutputLinearBarGraph) },
         { IsobusObjectType.OutputMeter, typeof(Objects.OutputMeter) },
         { IsobusObjectType.OutputNumber, typeof(Objects.OutputNumber) },
         { IsobusObjectType.OutputPolygon, typeof(Objects.OutputPolygon) },
         { IsobusObjectType.OutputRectangle, typeof(Objects.OutputRectangle) },
         { IsobusObjectType.OutputString, typeof(Objects.OutputString) },
         { IsobusObjectType.PictureGraphic, typeof(Objects.PictureGraphic) },
         { IsobusObjectType.SoftKeyMask, typeof(Objects.SoftKeyMask) },
         { IsobusObjectType.StringVariable, typeof(Objects.StringVariable) },
         // NOTE: changing this back to an object, not sure of the consequences
         //{ IsobusObjectType.WorkingSet, typeof(WorkingSet) },
         { IsobusObjectType.WorkingSet, typeof(Objects.WorkingSet) },
         { IsobusObjectType.InputBoolean, typeof(Objects.InputBoolean) },
         { IsobusObjectType.InputString, typeof(Objects.InputString) },
         { IsobusObjectType.InputNumber, typeof(Objects.InputNumber) },
         { IsobusObjectType.InputList, typeof(Objects.InputList) },
         { (IsobusObjectType)29, typeof(Objects.AuxType1Object) },
         { (IsobusObjectType)30, typeof(Objects.AuxInType1) },
         { IsobusObjectType.AuxFunctionType2, typeof(Objects.AuxType2Object) },
         { IsobusObjectType.AuxInputType2, typeof(Objects.AuxInType2) },
         { (IsobusObjectType)33, typeof(Objects.AuxObjectPointer) },

        };
   }
}
