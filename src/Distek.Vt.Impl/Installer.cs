﻿
#if USE_AUTOFAC
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Distek.Vt;
using Distek.Vt.Commands;
using Distek.Platform;
using Distek.Vt.Messages;

namespace Distek.Vt.Impl
{
   public class Installer : Module
   {
      protected override void Load(ContainerBuilder builder)
      {
         builder.RegisterType<VirtualTerminal>().As<VirtualTerminal>().SingleInstance();
         builder.RegisterType<Serializer>().As<ISerialization>();
         builder.RegisterType<FileNonVolatile>().As<INonVolatileMemory>();
         builder.RegisterType<ObjectPoolReader>().As<IDeserialization>();
         builder.RegisterType<PoolValidation>().As<IPoolValidation>();

         // TEST: remove this when you need to debug
         //builder.RegisterType<WorkingSetMaintenanceTask>().As<IRepeatedTask>();
         builder.RegisterType<VtStatusTask>().As<IRepeatedTask>();
         builder.RegisterType<AddressClaimTask>().As<IRepeatedTask>();
         builder.RegisterType<PoolParsingTask>().As<IRepeatedTask>();

         //builder.RegisterType<Settings>().As<ISettings>().SingleInstance();

         // Message readers
         builder.RegisterType<ChangeNumericValueCommandReader>().Keyed<IMessageReader>(ChangeNumericValueCommandReader.MessageId);
         builder.RegisterType<ChangeStringValueReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeStringValue);
         builder.RegisterType<ChangeActiveMaskReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeActiveMask);
         builder.RegisterType<HideShowObjectReader>().Keyed<IMessageReader>(IsobusMessageId.HideShowObject);
         builder.RegisterType<EnableDisableObjectReader>().Keyed<IMessageReader>(IsobusMessageId.EnableDisableObject);
         builder.RegisterType<SelectInputObjectReader>().Keyed<IMessageReader>(IsobusMessageId.SelectInputObject);
         builder.RegisterType<EscReader>().Keyed<IMessageReader>(IsobusMessageId.Esc);
         builder.RegisterType<ControlAudioSignalReader>().Keyed<IMessageReader>(IsobusMessageId.ControlAudioSignal);
         builder.RegisterType<SetAudioVolumeReader>().Keyed<IMessageReader>(IsobusMessageId.SetAudioVolume);
         builder.RegisterType<ChangeChildPositionReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeChildPosition);
         builder.RegisterType<ChangeSizeReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeSize);
         builder.RegisterType<ChangeBackgroundColourReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeBackgroundColour);
         builder.RegisterType<ChangeEndPointReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeEndPoint);
         builder.RegisterType<ChangeFontAttributesReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeFontAttributes);
         builder.RegisterType<ChangeLineAttributesReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeLineAttributes);
         builder.RegisterType<ChangeFillAttributesReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeFillAttributes);
         builder.RegisterType<ChangeSoftKeyMaskReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeSoftKeyMask);
         builder.RegisterType<ChangeAttributeReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeAttribute);
         builder.RegisterType<ChangePriorityReader>().Keyed<IMessageReader>(IsobusMessageId.ChangePriority);
         builder.RegisterType<ChangeListItemReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeListItem);
         builder.RegisterType<DeleteObjectPoolReader>().Keyed<IMessageReader>(IsobusMessageId.DeleteObjectPool);
         builder.RegisterType<ChangeChildLocationReader>().Keyed<IMessageReader>(IsobusMessageId.ChangeChildLocation);
         // Activation messages
         //builder.RegisterType<SoftKeyActivationReader>().Keyed<IMessageReader>(IsobusMessageId.SoftKeyActivation);
      }
   }
}
#endif
