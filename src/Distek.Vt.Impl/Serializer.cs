﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Distek.Platform;
using System.IO;
using Newtonsoft.Json;
using Distek.Network;

namespace Distek.Vt.Impl
{
   public class Serializer : ISerialization
   {
      private void SerializeObject(IsoVtObject obj, BinaryWriter writer)
      {
         // Write the info every object must have
         writer.Write((UInt16)obj.ObjectId);
         var typeInfo = obj.GetType().GetTypeInfo();
         var typeAttribute = typeInfo.GetCustomAttribute<ObjectTypeAttribute>();
         if (typeAttribute == null)
            throw new InvalidIsoObjectException(string.Format("{0} is missing an ObjectTypeAttribute", obj.GetType().Name));
         writer.Write((byte)typeAttribute.ObjectType);

         // Let the object write the rest of it's data
         obj.Write(writer);
      }

      Stream ISerialization.SerializeObjectPool(IList<IsoVtObject> objects)
      {
         var s = JsonConvert.SerializeObject(objects);
         return new MemoryStream(Encoding.UTF8.GetBytes(s));
      }

      byte[] ISerialization.SerializeCanMessage(CanMessage message)
      {
         var bytes = new List<byte>();
         var typeInfo = message.GetType().GetTypeInfo();

         var functionAttribute = typeInfo.GetCustomAttribute<VtFunctionAttribute>();
         if (functionAttribute != null)
            bytes.Insert(0, (byte)functionAttribute.MessageId);

         foreach (var property in typeInfo.DeclaredProperties)
         {
            var attr = property.GetCustomAttribute<ByteOrderAttribute>();
            if (attr == null)
               throw new InvalidIsoObjectException(string.Format("Property {0} of type {1} is missing a byte order attribute", property.Name, typeInfo.Name));
            var index = attr.Order;
            var length = attr.Length;
            if (property.PropertyType == typeof(Int32))
            {
               if (length == 1)
                  bytes.Insert(index, Convert.ToByte(property.GetValue(message)));
               else if (length == 2)
                  bytes.InsertRange(index, BitConverter.GetBytes(Convert.ToUInt16(property.GetValue(message))));
               // Some ETP messages use 3 byte numbers
               else if(length == 3)
                  bytes.InsertRange(index, BitConverter.GetBytes(Convert.ToUInt32(property.GetValue(message))).Take(3));
               else if (length == 4)
                  bytes.InsertRange(index, BitConverter.GetBytes(Convert.ToUInt32(property.GetValue(message))));
               else if (length == 0)
                  throw new ArgumentException("Message property length of 0 is invalid");
            }
            else if (property.PropertyType == typeof(string))
            {
               var str = property.GetValue(message) as string;
               var stringBytes = Encoding.UTF8.GetBytes(str.ToCharArray(0, str.Length));
               bytes.InsertRange(index, stringBytes);
            }
            else if(property.PropertyType == typeof(bool))
            {
               bytes.Insert(index, ((bool)property.GetValue(message)) == true ? (byte)1 : (byte)0);
            }
            else if(property.PropertyType == typeof(byte[]))
            {
               bytes.InsertRange(index, ((byte[])property.GetValue(message)));
            }
            else if (typeof(Enum).GetTypeInfo().IsAssignableFrom(property.PropertyType.GetTypeInfo()))
            {
               // All enums are 8 bits
               bytes.Insert(index, Convert.ToByte(property.GetValue(message)));
            }
            else if(property.PropertyType == typeof(IEnumerable<string>))
            {
               var strings = property.GetValue(message) as IEnumerable<string>;

               foreach(var s in strings)
               {
                  var stringBytes = Encoding.ASCII.GetBytes(s);
                  bytes.InsertRange(index, stringBytes);
               }
               // TODO: do we need to update index here?  we will if there are fields that
               // come after the variable length field
            }
            else if(property.PropertyType == typeof(IEnumerable<byte[]>))
            {
               var byteArrays = property.GetValue(message) as IEnumerable<byte[]>;
               foreach (var arr in byteArrays)
                  bytes.InsertRange(index, arr);
            }
            else if(property.PropertyType == typeof(IEnumerable<>))
            {
               throw new Exception("Open generics are not supported in variable length fields");
            }
         }

         if (bytes.Count() < 8)
            bytes = bytes.Concat(Enumerable.Repeat<byte>(0xFF, 8 - bytes.Count())).ToList();
         return bytes.ToArray();
      }

   }
}
