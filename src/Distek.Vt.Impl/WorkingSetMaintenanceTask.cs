﻿using Distek.DataLink;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distek.Vt.Impl
{
   public class WorkingSetMaintenanceTask : IRepeatedTask
   {
      int IRepeatedTask.Interval { get { return 1000; } }

      public WorkingSetMaintenanceTask(VirtualTerminal vt, IErrorHandler errorHandler, INameTable nameTable, ILogger log)
      {
         _vt = vt;
         _errorHandler = errorHandler;
         _nameTable = nameTable;
         _log = log;
      }

      void IRepeatedTask.Tick()
      {
         lock (_vt.ConnectedClients)
         {
            var toRemove = default(VtClient);
            // don't do this for the internal working set because it doesn't send messages
            foreach (var ws in _vt.ConnectedClients.Where(s => s.MasterSourceAddress != _nameTable.SourceAddress))
            {
               // Check if it's been longer than 3 seconds since the last maintenance message and if the object pool has not been deleted
               if ((DateTime.Now - ws.LastSeen > TimeSpan.FromSeconds(3)) &&
                   (ws.ObjectPool != null))
               {
                  _log.Warning("Working set disconnected from timeout. SA: {0}, last seen: {1}", ws.MasterSourceAddress, ws.LastSeen.ToString());
                  _errorHandler.RaiseError(new UnexpectedWorkingSetShutdownError()
                  {
                     Message = "Working Set shutdown unexpectedly",
                     MasterSourceAddress = ws.MasterSourceAddress
                  });
                  toRemove = ws;
               }
            }
            // remove this client so we can let it re-connect 
            _vt.ConnectedClients.Remove(toRemove);
         }
      }

      private VirtualTerminal _vt;
      private IErrorHandler _errorHandler;
      private INameTable _nameTable;
      private ILogger _log;
   }
}
