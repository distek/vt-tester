﻿using Distek.Platform;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Impl
{
   /*
     Notes:
     - We'll probably end up using SQLite for storage later on, but using files will make debugging 
        and testing slightly easier
     - Akavache may also be worth looking into, although it's not meant for exactly what we're trying to do
     - If we ever decide to keep using this class, System.IO.Abstractions would be a good idea
     - System.IO isn't in the pcl, so we'll need to abstract file system operations
     - The responsibility of this interface is to take care of logic related to keeping track of
        .iop files, not saving them.  That part is taken care of in IFileSystem implementations
   */
   public class FileNonVolatile : INonVolatileMemory
   {
      public FileNonVolatile(IFileSystem fileSystem, IDeserialization iopReader, ILogger logger, ISerialization serializer)
      {
         _fileSystem = fileSystem;
         _iopReader = iopReader;
         _logger = logger;
         _serializer = serializer;
      }

      bool INonVolatileMemory.DeleteObjectPoolVersion(string workingSetMasterName, string versionLabel)
      {
         var fileName = System.IO.Path.Combine(workingSetMasterName, versionLabel) + ".iop";
         if (_fileSystem.FileExists(fileName))
         {
            _fileSystem.DeleteFile(fileName);
            return true;
         }
         return false;
      }

      IEnumerable<string> INonVolatileMemory.GetVersionLabels(string workingSetMasterName)
      {
         var files = _fileSystem.ListFiles(workingSetMasterName);
         // remove the file extension
         return files.Select(f => Path.GetFileNameWithoutExtension(f));
      }

      ObjectPool INonVolatileMemory.LoadObjectPool(string workingSetMasterName, string versionLabel)
      {
         var fileName = System.IO.Path.Combine(workingSetMasterName, versionLabel) + ".iop";
         _logger.Information("Loading {0}", fileName);
         if (_fileSystem.FileExists(fileName))
         {
            using (var s = _fileSystem.ReadFile(fileName))
            {
               var poolString = new StreamReader(s).ReadToEnd();
               var op = JsonConvert.DeserializeObject<ObjectPool>(poolString, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All });
               return op;
            }
         }
         return null;
      }

      bool INonVolatileMemory.SaveObjectPool(ObjectPool objectPool)
      {
         if(objectPool.VersionLabel.Equals("20-20-20-20-20-20-20"))
         {
            if(_fileSystem.ListFiles(objectPool.WorkingSetMasterName).Count() == 0)
            {
               // return an error here
               return false;
            }
            else
            {
               // overwrite the newest version of the object pool
               var toOverwrite = _fileSystem.Newest(objectPool.WorkingSetMasterName);
               objectPool.VersionLabel = toOverwrite;
            }
         }

         var fileName = System.IO.Path.Combine(objectPool.WorkingSetMasterName, objectPool.VersionLabel) + ".iop";
         _logger.Information("Saving {0}", fileName);

         //var bytes = _serializer.SerializeObjectPool(objectPool.Objects);
         var bytes = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(objectPool, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All })));
         _fileSystem.WriteFile(fileName, bytes);
         return true;
      }

      private readonly IFileSystem _fileSystem;
      private readonly IDeserialization _iopReader;
      private readonly ILogger _logger;
      private ISerialization _serializer;
   }
}
