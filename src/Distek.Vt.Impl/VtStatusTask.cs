﻿using Distek.DataLink;
using Distek.Platform;
using Distek.Vt.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distek.Vt.Impl
{
   public class VtStatusTask : IRepeatedTask
   {
      int IRepeatedTask.Interval { get { return 1000; } }

      public VtStatusTask(IConnection connection, VirtualTerminal vt, INameTable nameTable)
      {
         _connection = connection;
         _vt = vt;
         _nameTable = nameTable;
      }

      void IRepeatedTask.Tick()
      {
         // TODO: The VT status message also needs to be sent when its data changes
         // VT status doesn't start until 3 seconds after startup
         //await Task.Delay(TimeSpan.FromSeconds(3));
         if (_nameTable.ClaimComplete)
         {
            var vtStatusMessage = new VtStatusMessage()
            {
               ActiveWorkingSetMaster = _vt.CurrentClient.MasterSourceAddress,
               VisibleMask = _vt.CurrentClient.ActiveMaskId,
               VisibleSoftKeyMask = _vt.CurrentClient.ActiveSoftKeyMaskId,
               BusyCode = _vt.Status,
               VtFunctionCode = 0,
            };
            _connection.Send(vtStatusMessage);
         }
      }

      private IConnection _connection;
      private VirtualTerminal _vt;
      private INameTable _nameTable;
   }
}
