﻿using Distek.Platform;
using Distek.Vt.Objects;
using Nine.Imaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.Vt.Impl
{
   public class PictureProcessor
   {
      public static string Render(PictureGraphic pg, string masterName, IFileSystem fileSystem)
      {
         var d = Directory.GetCurrentDirectory();
         var dir = Path.Combine(d, "wwwroot", "images", masterName);
         Directory.CreateDirectory(dir);
         //var path = Path.Combine(d, "wwwroot", "images", masterName, pg.ObjectId.ToString()) + ".png";
         var path = Path.Combine("images", masterName, pg.ObjectId.ToString()) + ".png";
         fileSystem.CreateDirectory(Path.Combine("images", masterName));
         //lock (path)
         //{
            //if (File.Exists(path) == false)
            if(fileSystem.FileExists(path) == false)
            {
               //using (var output = System.IO.File.OpenWrite(path))
               //{
                  var colors = DecodeImage(pg);
                  // Clients might send pictures that are 0x0
                  if (colors.Count == 0)
                     return "/images/404.png";

                  var bytes = new List<byte>();
                  foreach (var c in colors)
                  {
                     // Nine.Imaging stores pixels a BGRa I guess
                     bytes.Add(c.B);
                     bytes.Add(c.G);
                     bytes.Add(c.R);
                     bytes.Add(c.A);
                  }
                  var img = new Image(pg.ActualWidth, pg.ActualHeight, bytes.ToArray());
               //img.SaveAsPng(output);
               var outputPath = fileSystem.GetLocalPath(path);
            img.SaveAsPng(outputPath);
               //}
            }
         //}
         return string.Format("/images/{0}/{1}.png", masterName, pg.ObjectId);
      }

      private static List<Color> DecodeImage(PictureGraphic pg)
      {
         var p = new List<Color>();
         var transparencyColour = default(Color);
         var transparent = false;
         // handle transparency color if option is set
         if ((pg.Options & PictureGraphicOptions.Transparent) != 0)
         {
            transparent = true;
            transparencyColour = pg.TransparencyColour.ToNine();
         }

         if (pg.Format == PictureGraphicFormat.EightBitColor)
         {
            if ((pg.Options & PictureGraphicOptions.RunLengthEncoded) != 0)
            {
               for (var i = 0; i < pg.RawData.Count; i += 2)
               {
                  var num = pg.RawData[i];
                  var color = new IsoColour(pg.RawData[i + 1]).ToNine();
                  if (transparent && color == transparencyColour)
                     color = Color.Transparent;
                  p.AddRange(Enumerable.Repeat(color, num));
               }
            }
            else
            {
               for (var i = 0; i < pg.RawData.Count; i++)
               {
                  var color = new IsoColour(pg.RawData[i]).ToNine();
                  if (transparent && color == transparencyColour)
                     color = Color.Transparent;
                  p.Add(color);
               }
            }
         }
         else if (pg.Format == PictureGraphicFormat.FourBitColor)
         {
            p = ProcessFourBit(pg, transparent, transparencyColour);
         }
         else
         {
            // monochrome
            p = ProcessMonochrome(pg, transparent, transparencyColour);
         }

         return p.ToList();
      }

      private static List<Color> ProcessFourBit(PictureGraphic pg, bool transparent, Color transparencyColour)
      {
         var p = new List<Color>();

         var rawData = pg.RawData;
         // decode RLE into hi/lo bytes so the normal rendering can work
         if ((pg.Options & PictureGraphicOptions.RunLengthEncoded) != 0)
         {
            rawData = new List<byte>();
            for (var i = 0; i < pg.RawData.Count(); i += 2)
            {
               var num = pg.RawData[i];
               var c = pg.RawData[i + 1];
               ((List<byte>)rawData).AddRange(Enumerable.Repeat<byte>(c, num));
            }
         }

         var remainder = 0;
         if (pg.ActualWidth % 2 != 0)
            remainder = 1;
         var bytesPerLine = (pg.ActualWidth / 2) + remainder;

         for (var y = 0; y < pg.ActualHeight; y++)
         {
            var row = new List<Color>();
            //var bytes = pg.RawData.Skip(y * bytesPerLine).Take(bytesPerLine).ToArray();
            var bytes = rawData.Skip(y * bytesPerLine).Take(bytesPerLine).ToArray();
            for (var x = 0; x < bytes.Count(); x++)
            {
               var idxHi = (bytes[x] & 0xF0) >> 4;
               var idxLo = (bytes[x] & 0x0F);
               var cHi = new IsoColour(idxHi).ToNine();
               if (transparent && (cHi.Equals(transparencyColour)))
                  cHi = Color.Transparent;
               var cLo = new IsoColour(idxLo).ToNine();
               if (transparent && (cLo.Equals(transparencyColour)))
                  cLo = Color.Transparent;
               row.Add(cHi);
               row.Add(cLo);
            }
            p.AddRange(row.Take(pg.ActualWidth));
         }
         return p;
      }

      private static List<Color> ProcessMonochrome(PictureGraphic pg, bool transparent, Color transparencyColour)
      {
         var p = new List<Color>();

         var rawData = pg.RawData;
         if ((pg.Options & PictureGraphicOptions.RunLengthEncoded) != 0)
         {
            rawData = new List<byte>();
            for (var i = 0; i < pg.RawData.Count(); i += 2)
            {
               var num = pg.RawData[i];
               var c = pg.RawData[i + 1];
               ((List<byte>)rawData).AddRange(Enumerable.Repeat<byte>(c, num));
            }
         }

         var remainder = 0;
         if (pg.ActualWidth % 8 != 0)
            remainder = 1;

         var bytesPerLine = (pg.ActualWidth / 8) + remainder;
         for (var y = 0; y < pg.ActualHeight; y++)
         {
            var row = new List<Color>();
            var bytes = rawData.Skip(y * bytesPerLine).Take(bytesPerLine);
            foreach (var d in bytes)
            {
               for (var i = 7; i >= 0; i--)
               {
                  var shifted = d >> i;
                  var masked = shifted & 0x01;
                  var color = masked == 0x01 ? Color.White : Color.Black;
                  if (transparent && color.Equals(transparencyColour))
                     row.Add(Color.Transparent);
                  else
                     row.Add(color);
               }
            }
            // ignore pixels that are after the edge of the image
            p.AddRange(row.Take(pg.ActualWidth));
         }
         return p;
      }
   }

   public static class NineColourExtensions
   {
      public static Color ToNine(this IsoColour colour)
      {
         return new Color((byte)colour.R, (byte)colour.G, (byte)colour.B);
      }
   }
}
