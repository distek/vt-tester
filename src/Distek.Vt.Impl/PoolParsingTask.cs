﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Distek.Vt.Messages;
using System.Collections.ObjectModel;
using Distek.Platform;
using Serilog;

namespace Distek.Vt.Impl
{
   public class PoolParsingTask : IRepeatedTask
   {
      int IRepeatedTask.Interval => 100;

      public PoolParsingTask(VirtualTerminal vt, IFileSystem fileSystem, IDeserialization reader,
         IConnection connection, ILogger log, IPoolValidation validator, IErrorHandler errorHandler)
      {
         _vt = vt;
         _fileSystem = fileSystem;
         _reader = reader;
         _connection = connection;
         _log = log;
         _validator = validator;
         _errorHandler = errorHandler;
      }

      void IRepeatedTask.Tick()
      {
         // Check if there is a pool to be parsed.  If not, just return
         var client = _vt.ConnectedClients.FirstOrDefault(c => c.State == WorkingSetState.PoolTransferred);
         if (client == default(VtClient))
            return;

         _log.Debug("Parsing object pool");
         _log.Debug("vt status: {0}", _vt.Status);

         _log.Debug("Parsing object pool with {0} bytes", _vt.TemporaryObjectPoolData.Count);
         _vt.Status = VtBusyCode.BusyParsingObjectPool;

         var objectPoolBytes = _vt.TemporaryObjectPoolData;
         _fileSystem.WriteFile("raw-pool.iop", new MemoryStream(objectPoolBytes.ToArray()));

         var op = default(IEnumerable<IsoVtObject>);
         try
         {
            op = _reader.DeserializeObjectPool(new MemoryStream(objectPoolBytes.ToArray()));
         }
         catch (ObjectPoolParseException oppe)
         {
            client.State = WorkingSetState.Error;

            // respond to the client with the error
            _connection.Send(new EndOfObjectPoolResponse(client.MasterSourceAddress)
            {
               ErrorCode = 1,
               // I don't know the parent object
               ParentObjectId = 0,
               FaultyObjectId = oppe.InvalidObjectId,
               ObjectPoolErrorCodes = 1,
            });
            // TODO: Send a message to the front-end
            _errorHandler.RaiseError(oppe);

            return;
         }
         var errors = _validator.PoolIsValid(op);

         if (errors == null)
         {
            client.ObjectPool = new ObjectPool() { Objects = new List<IsoVtObject>(op) };
            foreach (var pg in client.ObjectPool.Objects.OfType<Objects.PictureGraphic>())
            {
               pg.ImgLink = PictureProcessor.Render(pg, client.Name, _fileSystem);
            }

            _vt.Status = VtBusyCode.NotBusy;
            client.State = WorkingSetState.ObjectPoolReady;
            var response = new EndOfObjectPoolResponse(client.MasterSourceAddress);
            _connection.Send(response);

            _log.Debug("vt status: {0}", _vt.Status);
            _log.Debug("Done parsing object pool");
         }
         else
         {
            client.State = WorkingSetState.Error;
            _errorHandler.RaiseError(new InvalidObjectPoolException("Object pool is invalid"));
            _vt.Status = VtBusyCode.NotBusy;
            var response = new EndOfObjectPoolResponse(client.MasterSourceAddress)
            {
               ErrorCode = 1,
               ObjectPoolErrorCodes = 2,
               ParentObjectId = errors.ParentObjectId,
               FaultyObjectId = errors.FaultyObjectId,
            };
            _connection.Send(response);
         }
      }

      private readonly VirtualTerminal _vt;
      private readonly IFileSystem _fileSystem;
      private readonly IDeserialization _reader;
      private readonly IConnection _connection;
      private readonly ILogger _log;
      private readonly IPoolValidation _validator;
      private readonly IErrorHandler _errorHandler;
   }
}
