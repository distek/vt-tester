﻿using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class TransportDataTransferHandler //: MessageHandler
   {
      public bool Handle(CanMessage m, Session currentSession)
      {
         var d = new DataMessage(m);
         if (d != null)
            currentSession.Buffer.Add(d);
         if (d.SequenceNumber == currentSession.PacketCount)
         {
            Console.WriteLine("TP session complete");
            if (d.SequenceNumber != currentSession.Buffer.Count())
            {
               var expected = Enumerable.Range(1, d.SequenceNumber);
               var actual = currentSession.Buffer.Select(x => x.SequenceNumber);
               var missing = expected.Except(actual);
               Console.WriteLine("Missed {0}", String.Join(",", missing.ToArray()));
               if (missing.Any())
               {
                  //return Task.FromResult<IResponse>(new EndOfMessage(m.SourceAddress)
                  //{
                  //   TotalMessageSize = currentSession.ByteCount,
                  //   TotalPacketCount = currentSession.PacketCount,
                  //   // TODO: use the actual PGN - need to fix serializer
                  //   Pgn = new byte[] { 0x00, 0xE7, 0x00 },
                  //});
               }
            }
            var opBytes = currentSession.Reassemble();
            // Remove the control byte before adding this data
            // TODO: this isn't going to work for other TP data like string updates
            //vt.TemporaryObjectPoolData = vt.TemporaryObjectPoolData.Concat(opBytes.Skip(1).Take(session.CurrentSession.ByteCount - 1).ToList()).ToList();

            var response = new EndOfMessage(m.SourceAddress)
            {
               TotalMessageSize = currentSession.ByteCount,
               TotalPacketCount = currentSession.PacketCount,
               Pgn = new byte[] { 0x00, 0xE7, 0x00 },
            };
            // dispose the current session so we can have another one
            //session.CurrentSession = null;

            // send EOM ACK
            //return Task.FromResult<IResponse>(response);
            return false;
         }
         else if (d.SequenceNumber % currentSession.PacketsPerPart == 0)
         {
            System.Diagnostics.Debug.WriteLine("Sending next CTS");
            // time for another cts
            //return Task.FromResult<IResponse>(new TransportClearToSendMessage(m.SourceAddress)
            //{
            //   NumberOfPackets = Math.Min(currentSession.PacketCount, currentSession.PacketsPerPart),
            //   NextPacketNumberToSend = d.SequenceNumber + 1,
            //   Pgn = new byte[] { 0x00, 0xE7, 0x00 },
            //});
         }
         //return Task.FromResult<IResponse>(null);
         return true;
      }
   }
}
