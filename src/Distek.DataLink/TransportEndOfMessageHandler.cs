﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
    public class TransportEndOfMessageHandler : MessageHandler
    {
      public TransportEndOfMessageHandler(INameTable nameTable, SessionManager session)
      {
         Filter = m => (m.Data[0] == TP.CM_EndofMsgACK) &&  (m.PGN == Constants.TransportConnectionManagement) && (m.DestinationAddress == nameTable.SourceAddress);
         //Filter = m => ((m.Header & 0x00FF0000) == 0x00EC0000) && m.Data[0] == 19;
         AsyncHandler = m =>
         {
            Console.WriteLine("TP EoMA");
			 // shut down the current TP session
			 var s = session.CurrentTransmitSession;
			 if(s != null)
			 {
				 s.IsComplete = true;
			 }
            return Task.FromResult<IResponse>(null);
         };
      }
    }
}
