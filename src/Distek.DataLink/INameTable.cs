﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public interface INameTable
   {
      bool ClaimComplete { get; set; }

      /// <summary>
      /// This CFs NAME
      /// </summary>
      Name LocalName { get; set; }
      /// <summary>
      /// All CAN names of connected clients, indexed by source address
      /// </summary>
      IDictionary<int, Name> CanNames { get; }

      /// <summary>
      /// Gets the next claimable address from the name table.  Defaults to
      /// the VT preferred address of 0x26.  Will choose another address
      /// if that address has been claimed by another CF
      /// </summary>
      /// <returns></returns>
      int GetVacantAddress();

      /// <summary>
      /// Reads the source address for this CF
      /// </summary>
      int SourceAddress { get; }

      /// <summary>
      /// Adds a new control function to the name table
      /// </summary>
      /// <param name="sourceAddress"></param>
      /// <param name="canName"></param>
      void AddressClaimed(int sourceAddress, Name canName);
   }
}
