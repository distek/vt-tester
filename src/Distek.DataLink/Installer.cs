﻿#if USE_AUTOFAC
using Autofac;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class Installer : Module
   {
      protected override void Load(ContainerBuilder builder)
      {
         builder.RegisterType<NameTable>().As<INameTable>().SingleInstance();
         builder.RegisterType<SessionManager>().SingleInstance();
         builder.RegisterType<RequestForAddressClaimedHandler>().As<MessageHandler>();
         builder.RegisterType<AddressClaimedHandler>().As<MessageHandler>();

         //builder.RegisterType<TransportRequestToSendHandler>().As<MessageHandler>();
         //builder.RegisterType<TransportDataTransferHandler>().As<MessageHandler>();
         builder.RegisterType<TransportClearToSendHandler>().As<MessageHandler>();
         builder.RegisterType<ExtendedDataPacketOffsetHandler>().As<MessageHandler>();
         builder.RegisterType<ExtendedDataTransferHandler>().As<MessageHandler>();
         builder.RegisterType<ExtendedRequestToSendHandler>().As<MessageHandler>();
         builder.RegisterType<ExtendedTransportAbortHandler>().As<MessageHandler>();
      }
   }
}
#endif