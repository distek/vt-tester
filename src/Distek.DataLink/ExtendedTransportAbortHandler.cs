﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Distek.DataLink
{
   public class ExtendedTransportAbortHandler : MessageHandler
   {
      public ExtendedTransportAbortHandler(SessionManager session, INameTable nameTable, ILogger log)
      {
         Filter = m => (m.Data[0] == ETP.Conn_Abort) &&  (m.PGN == Constants.ExtendedTransportConnectionManagement) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            // TODO: we should log what happened or something
            log.Debug("ETP aborted from client");
            session.CurrentSession = null;
            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
