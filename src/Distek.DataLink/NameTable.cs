﻿using Distek.DataLink.Messages;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class NameTable : INameTable
   {
      public bool ClaimComplete { get; set; }
      public Name LocalName { get; set; }

      public NameTable()
      {
         ClaimComplete = false;
         CanNames = new Dictionary<int, Name>();
         _sourceAddress = 0x80;
      }

      int INameTable.SourceAddress {  get { return _sourceAddress; } }

      public IDictionary<int, Name> CanNames { get; }

      void INameTable.AddressClaimed(int sourceAddress, Name canName)
      {
         if (CanNames.ContainsKey(sourceAddress) == false)
            CanNames.Add(sourceAddress, canName);
      }

      public int GetVacantAddress()
      {
         if(CanNames.ContainsKey(0x80))
         {
            var start = 0x81;
            var done = false;
            while(done == false)
            {
               if(CanNames.ContainsKey(start) == false)
               {
                  done = true;
                  _sourceAddress = start;
               }
               start++;
               // No addresses left! That's crazy!
               if(start == 248)
               {
                  done = true;
                  _sourceAddress = 0xFE;
               }
            }

         }
         return _sourceAddress;
      }

      private int _sourceAddress;
   }
}
