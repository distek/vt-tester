﻿using Distek.DataLink.Messages;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Distek.DataLink
{
   public class ExtendedRequestToSendHandler : MessageHandler
   {
      public ExtendedRequestToSendHandler(SessionManager session, INameTable nameTable, ILogger log)
      {
         Filter = m => (m.Data[0] == ETP.CM_RTS) &&  (m.PGN == Constants.ExtendedTransportConnectionManagement) && (m.DestinationAddress == nameTable.SourceAddress);
         //Filter = m => ((m.Header & 0x00FF0000) == 0x00C80000) && m.Data[0] == 20;
         AsyncHandler = m =>
         {
            log.Debug("ETP RTS handler"); 
            if (session.CurrentSession == null)
            {
               log.Debug("Creating new ETP session");
               var messageSize = (int)BitConverter.ToUInt32(m.Data, 1);
               var padded = m.Data.Concat(new byte[] { 0x00 }).ToArray();
               var pgn = BitConverter.ToUInt32(padded, 5);
               session.CurrentSession = new ExtendedTransportReceiveSession()
               {
                  ByteCount = messageSize,
                  //PacketsPerPart = 16,
                  //PacketsPerPart = 128,
                  //PacketsPerPart = 50,
                  PacketsPerPart = 255,
               };
               return Task.FromResult<IResponse>(new ExtendedClearToSendMessage(m.SourceAddress)
               {
                  NumberOfPackets = session.CurrentSession.PacketsPerPart,
                  NextPacketNumberToSend = session.CurrentSession.CurrentPacket + 1,
                  Pgn = new byte[] { 0x00, 0xE7, 0x00 },
               });
            }
            else
            {
               return Task.FromResult<IResponse>(new TransportAbortMessage(m.SourceAddress)
               {
                  ConnectionAbortReason = 1,
                  Pgn = new byte[] { 0x00, 0xE7, 0x00 },
               });
            }
         };
      }
   }
}
