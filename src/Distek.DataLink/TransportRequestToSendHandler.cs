﻿#if DEAD_CODE
using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class TransportRequestToSendHandler : MessageHandler
   {
      //1CEC2678  10 D6 02 68 10 00 E7 00 
      public IResponse HandleRts(CanMessage m, Session session)
      {
         var messageSize = BitConverter.ToUInt16(m.Data, 1);
         var packetCount = (int)m.Data[3];
         var packetsPerCts = (int)m.Data[4];
         var padded = m.Data.Concat(new byte[] { 0x00 }).ToArray();
         var pgn = (int)BitConverter.ToUInt32(padded, 5);
         //session = new Session()
         //{
         //   PacketCount = packetCount,
         //   ByteCount = messageSize,
         //   PacketsPerPart = packetsPerCts,
         //   TargetPgn = pgn,
         //};
         return new TransportClearToSendMessage(m.SourceAddress)
         {
            NumberOfPackets = Math.Min(packetCount, packetsPerCts),
            NextPacketNumberToSend = session.CurrentPacket + 1,
            Pgn = new byte[] { 0x00, 0xE7, 0x00 },
         };
      }
   }
}
#endif
