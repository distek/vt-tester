﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.DataLink.Messages
{
    public class TransportRequestToSendMessage : CanMessage
    {
      public TransportRequestToSendMessage(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xEC00;
         DestinationAddress = destination;
         Control = 16;
      }

      [ByteOrder(0, 1)]
      public int Control { get; private set; }

      [ByteOrder(1, 2)]
      public int TotalMessageSize { get; set; }

      [ByteOrder(3, 1)]
      public int TotalPackets { get; set; }

      [ByteOrder(4, 1)]
      public int PacketsPerCts { get; set; }

      [ByteOrder(5, 3)]
      public byte[] TransportPgn { get; set; }
    }
}
