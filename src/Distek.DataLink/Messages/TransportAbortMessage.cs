﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   [VtFunction((IsobusMessageId)255)]
   public class TransportAbortMessage : CanMessage, IResponse
   {
      public TransportAbortMessage(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xEC00;
         DestinationAddress = destination;
         Pgn = new byte[] { 0x00, 0x00, 0x00 };
      }

      [ByteOrder(1, 1)]
      public int ConnectionAbortReason { get; set; }

      [ByteOrder(2, 3)]
      public byte[]  Reserved { get { return new byte[] { 0xFF, 0xFF, 0xFF }; } }

      [ByteOrder(5, 3)]
      public byte[] Pgn { get; set; }
   }
}
