﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   [VtFunction((IsobusMessageId)17)]
   public class TransportClearToSendMessage : CanMessage, IResponse
   {
      public TransportClearToSendMessage(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xEC00;
         DestinationAddress = destination;
         Reserved = new byte[] { 0xFF, 0xFF };
      }

      // byte 2
      [ByteOrder(1, 1)]
      public int NumberOfPackets { get; set; }

      // byte 3
      [ByteOrder(2, 1)]
      public int NextPacketNumberToSend { get; set; }

      [ByteOrder(3, 2)]
      public byte[] Reserved { get; set; }

      // bytes 6,7,8
      [ByteOrder(5, 3)]
      public byte[] Pgn { get; set; }
   }
}
