﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   [VtFunction((IsobusMessageId)21)]
   public class ExtendedClearToSendMessage : CanMessage, IResponse
   {
      public ExtendedClearToSendMessage(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xC800;
         DestinationAddress = destination;
      }

      [ByteOrder(1, 1)]
      public int NumberOfPackets { get; set; }

      [ByteOrder(2, 3)]
      public int NextPacketNumberToSend { get; set; }

      [ByteOrder(5, 3)]
      public byte[] Pgn { get; set; }
   }
}
