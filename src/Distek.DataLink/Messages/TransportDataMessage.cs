﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.DataLink.Messages
{
    public class TransportDataMessage : CanMessage
    {
      public TransportDataMessage(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xEB00;
         DestinationAddress = destination;
      }

      [ByteOrder(0, 1)]
      public int SequenceNumber { get; set; }

      [ByteOrder(1, 7)]
      public byte[] TransportData { get; set; }
    }
}
