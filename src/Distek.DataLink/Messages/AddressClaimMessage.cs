﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   public class AddressClaimMessage : CanMessage, IResponse
   {
      public AddressClaimMessage(int localSourceAddress, Name localName)
      {
         Priority = 6;
         Header = (uint)0x18EEFF00 | (byte)localSourceAddress;
         Data = localName.ToBytes();
      }
   }
}
