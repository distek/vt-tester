﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   [VtFunction((IsobusMessageId)23)]
   public class ExtendedEndOfMessageAcknowledgement : CanMessage, IResponse
   {
      public ExtendedEndOfMessageAcknowledgement(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xC800;
         DestinationAddress = destination;
      }

      [ByteOrder(1, 4)]
      public int NumberOfBytesTransferred { get; set; }
      [ByteOrder(5, 3)]
      public byte[] Pgn { get; set; }
   }
}
