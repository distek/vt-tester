﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   public class RequestForAddressClaimedMessage : CanMessage
   {
      //[ByteOrder(0, 3)]
      //public byte[] RequestData
      //{
      //   get { return new byte[] { 0x00, 0xEE, 0x00 }; }
      //}

      // 18EAFFFE  3  00 EE 00
      public RequestForAddressClaimedMessage()
      {
         Header = 0x18EAFFFE;
         Data = new byte[] { 0x00, 0xEE, 0x00 };
      }
   }
}
