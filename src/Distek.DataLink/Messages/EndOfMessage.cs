﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink.Messages
{
   [VtFunction((IsobusMessageId)19)]
   public class EndOfMessage : CanMessage, IResponse
   {
      public EndOfMessage(int destination)
      {
         Priority = 6;
         ParameterGroupNumber = 0xEC00;
         DestinationAddress = destination;
         Reserved = 0xFF;
      }

      [ByteOrder(1, 2)]
      public int TotalMessageSize { get; set; }

      [ByteOrder(3, 1)]
      public int TotalPacketCount { get; set; }

      [ByteOrder(4, 1)]
      public int Reserved { get; set; }

      [ByteOrder(5, 3)]
      public byte[] Pgn { get; set; }
   }
}
