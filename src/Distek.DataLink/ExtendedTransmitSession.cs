﻿using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Distek.DataLink
{
   enum EtpState
   {
      Idle,
      SentRts,
      SendingData,
      Complete
   }

   public class ExtendedTransmitSession
   {
      // returns first RTS
      public CanMessage Start(CanMessage etpMessage)
      {
         _outgoing = etpMessage;
         _currentOffset = 0;
         _state = EtpState.SentRts;
         var len = BitConverter.GetBytes((UInt32)etpMessage.Data.Length);
         return new CanMessage(0x18C82680, new byte[] { 0x14, len[0], len[1], len[2], len[3], 0x00, 0xE7, 0x00 });
      }

      public void HandleMessage(CanMessage incoming, INetwork network)
      {
         // cts
         if(incoming.Data[0] == 21)
         {
            _state = EtpState.SendingData;
            var numToSend = incoming.Data[1];
            // send DPO
            var nexxt_packet = BitConverter.ToUInt32(incoming.Data, 2) - 1;
            Console.WriteLine("next packet: {0}", nexxt_packet);
            var np = BitConverter.GetBytes(nexxt_packet);
            var dpo = new CanMessage(0x1CC82680);
            dpo.Data = new byte[] { 22, 0xFF, np[0], np[1], np[2], 0x00, 0xE7, 0x00 };
            network.Send(dpo);
            // send the current offset's packets
            var segmentData = _outgoing.Data
               .Skip((int)nexxt_packet)
               .Take(numToSend * 7).ToList();
            var parts = segmentData
             .Select((x, i) => new { Index = i, Value = x })
             .GroupBy(x => x.Index / 7)
             .Select((x, i) => new CanMessage(0x1CC72680, new byte[] { (byte)(i + 1) }.Concat(x.Select(v => v.Value).ToArray()).ToArray()))//, TransportData = x.Select(v => v.Value).ToArray() })
             .ToList();
            foreach (var p in parts)
            {
               network.Send(p);
            }

            //var rand = new Random();
            //foreach (var p in parts)
            //{
            //   // randomly miss packets
            //   if (rand.Next(500) != 1)
            //   {
            //      Console.WriteLine("{0}", p.ToString());
            //      network.Send(p);
            //   }
            //   else
            //      Console.WriteLine("MISSING PACKET!");
            //}
         }

      }

      private EtpState _state;
      private int _currentOffset;
      private CanMessage _outgoing;
   }

   //public class ExtendedTransportSegment
   //{
   //   public IList<TransportDataMessage> Messages { get; set; }
   //   public int Offset { get; set; }
   //}
   
   //public class ExtendedClearToSendHandler : MessageHandler
   //{
   //   public ExtendedClearToSendHandler(SessionManager session, INameTable nameTable)
   //   {
   //      Filter = m => (m.Data[0] == TP.CM_CTS) && (m.PGN == Constants.TransportConnectionManagement) && (m.DestinationAddress == nameTable.SourceAddress);
   //      //Filter = m => ((m.Header & 0x00FF0000) == 0x00EC0000) && m.Data[0] == (byte)DataLinkControl.TransportClearToSend;
   //      AsyncHandler = m =>
   //      {
   //         Console.WriteLine("TP CTS received");

   //         // injecting INetowrk here because using IConnection causes a circular dependency
   //         // it'd probably be better to have message handlers return IEnumerable<CanMessage>
   //         // and let the IConnection queue them for the network


   //         var s = session.CurrentTransmitSession;
   //         foreach (var message in s.OutgoingMessages)
   //         {
   //            message.Header = message.Header | (uint)nameTable.SourceAddress;
   //            message.Data = serialization.SerializeCanMessage(message);
   //            network.Send(message);
   //            Console.WriteLine(message.ToString());
   //            Console.WriteLine(string.Format("Sending TP data message {0}", message.Data[0]));
   //         }

   //         return Task.FromResult<IResponse>(null);
   //      };
   //   }
   //}
   //}
}
