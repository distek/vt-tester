﻿using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Distek.DataLink
{
   public class TransportReceiveSession : Session
   {
      public override IResponse HandleMessage(ref CanMessage msg)
      {
         var response = default(IResponse);
         // TP.CM: Transport protocol connection management
         if (msg.PGN == Constants.TransportConnectionManagement)
         {
            switch (msg.Data[0])
            {
               // TP.CM_RTS
               case 16:
                  response = HandleRts(msg);
                  break;
			   case 19:
			      Console.WriteLine("End of msg ack");
				  break;
               // TP.Conn_Abort
               case 255:
                  // TODO: this isn't right - need to reset this session
                  response = new TransportAbortMessage(msg.SourceAddress)
                  {
                     // Already in one or more sessions and cannot support another
                     ConnectionAbortReason = 0x01
                  };
                  break;
               default:
                  break;
            }
         }
         // TP Data Message
         if (msg.PGN == Constants.TransportDataTransfer)
         {
            var d = new DataMessage(msg);
            Debug.WriteLine(string.Format("TP data - sequence #{0}/{1}", d.SequenceNumber, this.PacketCount));
            Debug.WriteLine(string.Format("packets per part: {0}", this.PacketsPerPart));
            this.Buffer.Add(d);
            if (d.SequenceNumber == this.PacketCount)
            {
               Debug.WriteLine("TP RX complete");
               // TP rx session complete
               var bytes = this.Reassemble();
               // fill in pgn etc
               msg = new CanMessage()
               {
                  Header = msg.Header,
                  ParameterGroupNumber = TargetPgn,
                  Data = bytes.ToArray()
               };

               response = new EndOfMessage(msg.SourceAddress)
               {
                  TotalMessageSize = ByteCount,
                  TotalPacketCount = PacketCount,
                  Pgn = new byte[] { 0x00, 0xE7, 0x00 },
               };
               this.IsComplete = true;
            }
            else if((d.SequenceNumber % this.PacketsPerPart) == 0)
            {
               Debug.WriteLine("TP CTS");
               response = new TransportClearToSendMessage(msg.SourceAddress)
               {
                  NumberOfPackets = this.PacketsPerPart,
                  NextPacketNumberToSend = d.SequenceNumber + 1,
                  Pgn = new byte[] { 0x00, 0xE7, 0x00 },
               };
            }
         }
         return response;
      }

      private IResponse HandleRts(CanMessage m)
      {
         var messageSize = BitConverter.ToUInt16(m.Data, 1);
         var packetCount = (int)m.Data[3];
         var packetsPerCts = (int)m.Data[4];
         var padded = m.Data.Concat(new byte[] { 0x00 }).ToArray();
         var pgn = (int)BitConverter.ToUInt32(padded, 5);

         PacketCount = packetCount;
         ByteCount = messageSize;
         PacketsPerPart = packetsPerCts;
         TargetPgn = pgn;

         Console.WriteLine("packet count {0}", PacketCount);
         Console.WriteLine("Packets per part {0}", PacketsPerPart);

         return new TransportClearToSendMessage(m.SourceAddress)
         {
            NumberOfPackets = Math.Min(packetCount, packetsPerCts),
            NextPacketNumberToSend = CurrentPacket + 1,
            Pgn = new byte[] { 0x00, 0xE7, 0x00 },
         };
      }
   }
}
