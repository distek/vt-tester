﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class Name
   {
      public int IdentityNumber { get; set; }
      public int ManufacturerCode { get; set; }
      public int EcuInstance { get; set; }
      public int FunctionInstance { get; set; }
      public int Function { get; set; }
      public int DeviceClass { get; set; }
      public int DeviceClassInstance { get; set; }
      public IndustryGroup IndustryGroup { get; set; }
      public bool SelfConfigurableAddress { get; set; }

      public Name()
      { }

      public Name(byte[] data)
      {
         _rawValue = data;

         IdentityNumber = ((data[2] & 0x1F) << 16) | (data[1] << 8) | (data[0]);
         ManufacturerCode = ((data[3] << 8 | data[2]) & 0xFFE0) >> 5;
         EcuInstance = data[4] & 0x07;
         FunctionInstance = (data[4] & 0xF8) >> 3;
         Function = data[5];
         DeviceClass = (data[6] & 0xFE) >> 1;
         DeviceClassInstance = data[7] & 0x0F;
         IndustryGroup = (IndustryGroup)((data[7] & 0x70) >> 4);
         SelfConfigurableAddress = ((data[7] & 0x80) >> 7) == 0x01;
      }

      public byte[] ToBytes()
      {
         var b = new byte[8];
         var f = (this.ManufacturerCode << 21) | (this.IdentityNumber & 0x15);
         var firstFour = BitConverter.GetBytes(f);
         b[0] = firstFour[0];
         b[1] = firstFour[1];
         b[2] = firstFour[2];
         b[3] = firstFour[3];

         b[4] = (byte)((this.FunctionInstance << 3) | (this.EcuInstance & 0x07));
         b[5] = (byte)this.Function;
         b[6] = (byte)(this.DeviceClass << 1);
         b[7] = (byte)(((this.SelfConfigurableAddress ? 1 : 0) << 7) |
            (((byte)this.IndustryGroup & 0x07) << 4) |
            (this.DeviceClassInstance & 0x0F));

         return b;
      }

      public override string ToString()
      {
         return BitConverter.ToString(_rawValue);
      }

      private byte[] _rawValue;
   }
}
