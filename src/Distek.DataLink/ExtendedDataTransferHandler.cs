﻿using Distek.DataLink.Messages;
using Distek.Platform;
using Distek.Vt;
using System;
using Serilog;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Subjects;

namespace Distek.DataLink
{
   public class ExtendedDataTransferHandler : MessageHandler
   {
      public ExtendedDataTransferHandler(SessionManager session, VirtualTerminal vt, INameTable nameTable, Subject<int> progress, ILogger log)
      {
         Filter = m => (m.PGN == Constants.ExtendedTransportDataTransfer) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            if (session.CurrentSession != null)
            {
               var d = new DataMessage(m, session.CurrentSession.PacketOffset);
               session.CurrentSession.Buffer.Add(d);

               //var total = (session.CurrentSession.Buffer.Count * 7 - 1);
               var total = (session.CurrentSession.Buffer.Count * 7);
               //var s = string.Format("buffer count: {0}, bytes received {1}", session.CurrentSession.Buffer.Count, session.CurrentSession.ByteCount);
               if (total % 100 == 0)
               {
                  //var s = string.Format("{0} / {1}", total, session.CurrentSession.ByteCount);
                  var percent = (int)(((float)total / (float)session.CurrentSession.ByteCount) * 100);
                  log.Debug("ETP percent complete: " + percent);
                  progress.OnNext(percent);
               }
               if (total >= session.CurrentSession.ByteCount)
               {
                  log.Debug("ETP complete");
                  // TODO: This TP implementation only works for object pool transfer
                  try
                  {
                     log.Debug("Reassembling..");
                     var sw = new Stopwatch();
                     sw.Start();
                     var opBytes = session.CurrentSession.Reassemble();
                     sw.Stop();
                     log.Debug("reassembly took: " + sw.ElapsedMilliseconds);
                     // For some reason I get a 0x11 at the start byte and an extra byte at the end
                     // TODO: i don't think i need to do this any longer - delete and see if it still works
                     //vt.TemporaryObjectPoolData = opBytes.Skip(1).Take(session.CurrentSession.ByteCount - 1).ToList();
                     log.Debug(string.Format("ETP: adding {0} bytes to object pool xfer", opBytes.Count() - 2));

                     vt.TemporaryObjectPoolData = vt.TemporaryObjectPoolData.Concat(opBytes.Skip(1).Take(session.CurrentSession.ByteCount - 1).ToList()).ToList();
                  }
                  catch (Exception e)
                  {
                     log.Debug("Error reassembling object pool" + e.Message);
                  }
                  var rval = new ExtendedEndOfMessageAcknowledgement(m.SourceAddress)
                  {
                     NumberOfBytesTransferred = session.CurrentSession.ByteCount,
                     Pgn = new byte[] { 0x00, 0xE7, 0x00 }
                  };

                  // report that we're finished with this transfer
                  progress.OnNext(100);

                  log.Debug("setting current session to null");
                  session.CurrentSession = null;
                  return Task.FromResult<IResponse>(rval);
               }
               else if (d.SequenceNumber % session.CurrentSession.PacketsPerPart == 0)
               {
                  // time for another cts
                  log.Debug("Sequence #: {0}", d.SequenceNumber);
                  var nextPacket = d.SequenceNumber + 1;
                  log.Debug("Sending next CTS " + nextPacket);
                  var numPackets = session.CurrentSession.PacketsPerPart;
                  // had this removed, bringing it back to see if it makes the jd transfers stop aborting from client
                  //if (session.CurrentSession.ByteCount - session.CurrentSession.PacketsPerPart < 0)
                  //   numPackets = session.CurrentSession.ByteCount - nextPacket;
                  var bytesLeft = session.CurrentSession.ByteCount - (session.CurrentSession.Buffer.Count * 7);
                  log.Debug("{0} bytes left in ETP", bytesLeft);
                  if(bytesLeft < (session.CurrentSession.PacketsPerPart * 7))
                  {
                     log.Debug("don't need a full CTS");
                     numPackets = (int)Math.Ceiling((float)bytesLeft / 7.0);
                     log.Debug("CTS-ing {0} packets", numPackets);
                  }

                  // NextPacketNumberToSend is current session Buffer.Count + 1
                  // if something didn't make it into the buffer we'll return an incorrect value
                  // which i think is causing the client to abort
                  var resp = new ExtendedClearToSendMessage(m.SourceAddress)
                  {
                     NumberOfPackets = numPackets,
                     //NextPacketNumberToSend = session.CurrentSession.CurrentPacket + 1,
                     NextPacketNumberToSend = nextPacket,
                     Pgn = new byte[] { 0x00, 0xE7, 0x00 },
                  };
                  return Task.FromResult<IResponse>(resp);
               }
            }
            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
