﻿using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class TransportClearToSendHandler : MessageHandler
   {
      public TransportClearToSendHandler(SessionManager session, INetwork network, ISerialization serialization, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == TP.CM_CTS) &&  (m.PGN == Constants.TransportConnectionManagement) && (m.DestinationAddress == nameTable.SourceAddress);
         //Filter = m => ((m.Header & 0x00FF0000) == 0x00EC0000) && m.Data[0] == (byte)DataLinkControl.TransportClearToSend;
         AsyncHandler = m =>
         {
            Console.WriteLine("TP CTS received");

            // injecting INetowrk here because using IConnection causes a circular dependency
            // it'd probably be better to have message handlers return IEnumerable<CanMessage>
            // and let the IConnection queue them for the network


            var s = session.CurrentTransmitSession;
            foreach (var message in s.OutgoingMessages)
            {
               message.Header = message.Header | (uint)nameTable.SourceAddress;
               message.Data = serialization.SerializeCanMessage(message);
               network.Send(message);
               Console.WriteLine(message.ToString());
               Console.WriteLine(string.Format("Sending TP data message {0}", message.Data[0]));
            }

            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
