﻿using Distek.DataLink.Messages;
using Distek.Platform;
using Distek.Vt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{

   public class RequestForAddressClaimedHandler : MessageHandler
   {
      public RequestForAddressClaimedHandler(INameTable nameTable)
      {
         // TODO: allow request from cfs that have claimed an address -- see agco logs
         //Filter = m => ((m.Header & 0x00EAFF00) == 0x00EAFF00) && (m.Data[0] == 0x00 && m.Data[1] == 0xEE && m.Data[2] == 0x00);
         //Filter = m => m.PGN == 0xEAFF;
         Filter = m => m.ParameterGroupNumber == 0xEAFF00 && (m.Data[0] == 0x00 && m.Data[1] == 0xEE && m.Data[2] == 0x00);
         AsyncHandler = m =>
         {
            var localName = new Name()
            {
               IdentityNumber = 1,
               ManufacturerCode = 514,
               EcuInstance = 0,
               FunctionInstance = 0,
               Function = 29,
               DeviceClass = 0,
               DeviceClassInstance = 0,
               IndustryGroup = IndustryGroup.AgricultureAndForestryEquipment,
               SelfConfigurableAddress = true
            };
            nameTable.LocalName = localName;

            return Task.FromResult<IResponse>(new AddressClaimMessage(nameTable.SourceAddress, localName));
         };
      }
   }
}
