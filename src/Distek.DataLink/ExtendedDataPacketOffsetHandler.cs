﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class ExtendedDataPacketOffsetHandler : MessageHandler
   {
      public ExtendedDataPacketOffsetHandler(SessionManager session, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == ETP.CM_DP) &&  (m.PGN == Constants.ExtendedTransportConnectionManagement) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            if(session != null)
            {
               var packetCount = (int)m.Data[1];
               var padded = m.Data.Skip(2).Take(3).Concat(new byte[] { 0x00 }).ToArray();
               var dataPacketOffset = BitConverter.ToUInt32(padded, 0);

               session.CurrentSession.PacketOffset = (int)dataPacketOffset;

            }

            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
