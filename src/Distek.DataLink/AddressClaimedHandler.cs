﻿using Distek.DataLink.Messages;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class AddressClaimedHandler : MessageHandler
   {
      public AddressClaimedHandler(INameTable nameTable, ILogger log/*, SocketCanConfiguration config*/)
      {
         Filter = m => m.ParameterGroupNumber == 0xEEFF00;
         AsyncHandler = m =>
         {
            log.Debug("{0} has claimed an address", m.SourceAddress);
            var n = new Name(m.Data);
            //if (config.MirrorMode == false)
            //{
               // if this message isn't from us and has our source address
               if ((n.Equals(nameTable.LocalName) == false) && (m.SourceAddress == nameTable.SourceAddress))
               {
                  log.Warning("Somebody wants our source address");
                  var theirName = BitConverter.ToInt64(n.ToBytes(), 0);
                  var myName = BitConverter.ToInt64(nameTable.LocalName.ToBytes(), 0);
                  if (theirName < myName)
                  {
                     log.Warning("We lost address arbitration, need to claim a new address");
                     // Put them in the name table so we get a new source address from GetVacantAddress()
                     nameTable.AddressClaimed(m.SourceAddress, n);
                     // lost arbitration, we need a new address
                     return Task.FromResult<IResponse>(new AddressClaimMessage(nameTable.GetVacantAddress(), nameTable.LocalName));
                  }

               }
            //}
            nameTable.AddressClaimed(m.SourceAddress, n);
            log.Debug(n.ToString());

            return Task.FromResult<IResponse>(null);
         };
      }
      //private readonly SocketCanConfiguration _config;
   }
}
