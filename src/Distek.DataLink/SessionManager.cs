﻿using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.DataLink
{
   public class SessionManager
   {
      public SessionManager(INameTable nameTable, ILogger log, INetwork network)
      {
         _nameTable = nameTable;
         _log = log;
         _network = network;
      }

      // this is pretty useless - just make sure to register as a singleton?  i don't know if we can have more than one session
      public Session CurrentSession { get; set; }

      public IResponse ProcessIncoming(ref CanMessage msg)
      {
         var response = default(IResponse);

         // transport connection mgmt
         if ((msg.PGN == 0xEC00) && (msg.DestinationAddress == _nameTable.SourceAddress))
         {
            _log.Debug(msg.ToString());
            if (msg.Data[0] == 16)
            {
               _log.Debug("TP rts");
               if (CurrentSession == null)
               {
                  _log.Debug("No current session, starting new TP receive");
                  CurrentSession = new TransportReceiveSession();
               }
               else
               {
                  _log.Debug("Already in a TP receive session, rejecting this one");
                  // we used to abort, but i think that's incorrect.  if we're in an ETP session and we get a TP
                  // rts, we could support it, but i don't think we have to.  either way, we don't want to
                  // abort an existing session just because somebody else wanted to start one.  need to finish
                  // the existing one
                  //CurrentSession = null;
                  response = new TransportAbortMessage(msg.SourceAddress)
                  {
                     // Already in one or more sessions and cannot support another
                     ConnectionAbortReason = 0x01
                  };
                  return response;
               }
            }
         }
         if ((CurrentSession != null) && (msg.DestinationAddress == _nameTable.SourceAddress))
         {
            response = CurrentSession.HandleMessage(ref msg);

            // If we're done, shut down this session
            if (CurrentSession.IsComplete)
            {
               System.Diagnostics.Debug.WriteLine("TP session complete");
               CurrentSession = null;
            }
         }
         if(msg.PGN == 0xC800)
         {
            // etp cm
            CurrentEtpTransmit.HandleMessage(msg, _network);

         }
         return response;
      }

      /// <summary>
      /// Initializes a TP/ETP session
      /// </summary>
      /// <param name="message">Message to transmit using TP/ETP</param>
      /// <returns>RTS Message</returns>
      public CanMessage CreateTransmit(CanMessage message)
      {
         if (message.Data.Length < 1785)
         {
            var parts = message.Data
               .Select((x, i) => new { Index = i, Value = x })
               .GroupBy(x => x.Index / 7)
               .Select((x, i) => new TransportDataMessage(message.DestinationAddress) { SequenceNumber = i + 1, TransportData = x.Select(v => v.Value).ToArray() })
               .ToList();
            CurrentTransmitSession = new TransmitSession()
            {
               OutgoingMessages = parts,
            };
            var transportPgn = BitConverter.GetBytes(message.ParameterGroupNumber);
            var extra = (message.Data.Count() % 7) == 0 ? 0 : 1;
            var packetCount = (message.Data.Count() / 7) + extra;
            return new TransportRequestToSendMessage(message.DestinationAddress)
            {
               TotalPackets = packetCount,
               TotalMessageSize = message.Data.Count(),
               // no limit on packets per CTS
               PacketsPerCts = 0xFF,
               TransportPgn = new byte[] { 0x00, transportPgn[2], 0x00 },
            };
         }
         else
         {
            var etpTx = new ExtendedTransmitSession();
            CurrentEtpTransmit = etpTx;
            return etpTx.Start(message);
         }

      }

      public TransmitSession CurrentTransmitSession { get; private set; }
      public ExtendedTransmitSession CurrentEtpTransmit { get; private set; }

      private readonly INameTable _nameTable;
      private readonly ILogger _log;
      private readonly INetwork _network;
   }

   public class TransmitSession
   {
      public IList<TransportDataMessage> OutgoingMessages { get; set; }
      public bool IsComplete { get; set; }

      public TransmitSession()
      {
         OutgoingMessages = new List<TransportDataMessage>();
         IsComplete = false;
      }
   }

   public abstract class Session
   {
      public abstract IResponse HandleMessage(ref CanMessage msg);

      public bool IsComplete { get; set; }
      public int PacketCount { get; set; }
      public int ByteCount { get; set; }
      public int CurrentPacket { get { return Buffer.Count; } }
      public int TargetPgn { get; set; }
      public int PacketsPerPart { get; set; }

      public TransportDataTransferHandler DataHandler { get; private set; }

      /// <summary>
      /// Current data packet offset
      /// </summary>
      public int PacketOffset { get; set; }
      public IList<DataMessage> Buffer { get; set; }

      public Session()
      {
         Buffer = new List<DataMessage>();
         DataHandler = new TransportDataTransferHandler();
         IsComplete = false;
      }

      public IList<byte> Reassemble()
      {
         var expectedSize = Buffer.Count * 8;
         var data = new List<byte>(expectedSize);
         var ordered = Buffer.OrderBy(d => d.SequenceNumber);
         foreach (var m in ordered)
         {
            data.AddRange(m.Data);
            //data = data.Concat(m.Data);
         }
         return data.Take(this.ByteCount).ToList();
      }
   }

   public class ExtendedTransportReceiveSession : Session
   {
      public override IResponse HandleMessage(ref CanMessage msg)
      {
         return null;
      }
   }

   public class DataMessage
   {
      public int SequenceNumber { get; set; }
      public byte[] Data { get; set; }

      public DataMessage(CanMessage msg) : this(msg, 0)
      { }

      public DataMessage(CanMessage msg, int packetOffset)
      {
         this.SequenceNumber = (int)msg.Data[0] + packetOffset;
         this.Data = msg.Data.Skip(1).ToArray();
      }
   }
}
