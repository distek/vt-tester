﻿using CommandLine;
using Distek.BusLog;
using Distek.BusLog.File;
using Distek.DataLink;
using Distek.DataLink.Messages;
using Distek.Network;
using Distek.Network.Pcan;
using Distek.Network.Udp;
using Distek.Network.ValueCan;
using Distek.Platform;
using Distek.Platform.Impl;
using Distek.Platform.NetCore;
using Distek.Vt;
using Distek.Vt.Impl;
using Distek.Vt.Messages;
using DryIoc;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Distek.VtClient
{  
   class Program
   {
      static void Main(string[] args)
      {

         CommandLine.Parser.Default.ParseArguments<BusLoadOptions, UploadOptions>(args)
          .MapResult(
            (BusLoadOptions opts) => StartBusLoad(opts),
            (UploadOptions opts) => StartPoolUpload(opts),
            errs => 1);

/*
         Console.WriteLine("Connecting");
         var tasks = c.ResolveMany<IRepeatedTask>();
         StartPeriodicTasks(tasks);

         var wsm = c.Resolve<WorkingSetMaintenanceTask>();
         while (wsm.ConnectionReady == false)
         {
            System.Threading.Thread.Sleep(1000);
         }
         var client = c.Resolve<VtClient>();

         // send etp pool
         //var longData = Enumerable.Repeat<byte>(0, 15000);
         //var iop = File.ReadAllBytes(@"..\..\..\view-test.iop");
         //var longData = new byte[] { 0x11 }.Concat(iop);
         //var txMsg = new CanMessage(0x18e72680, longData.ToArray());
         //var conn = c.Resolve<IConnection>();
         //conn.Send(txMsg);

         // send pool file
         client.SendObjectPool(file);

         var t = c.Resolve<PeriodicValueChange>();
         t.Start();

         Console.WriteLine("Press a key to exit");
         Console.Read();
         //bl.Stop();
         //var conn = c.Resolve<IConnection>();
         //conn.Send(new CanMessage(0x18E72680, new byte[] { 178, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }));
         //c.Resolve<IBusLogger>().Save();
         */
      }

      static void StartPeriodicTasks(IEnumerable<IRepeatedTask> tasks)
      {
         foreach (var t in tasks)
         {
            Observable
               .Interval(TimeSpan.FromMilliseconds(t.Interval))
               .Subscribe(x => t.Tick());
         }
      }

      static int StartBusLoad(BusLoadOptions opts)
      {
         Console.WriteLine("Configuring client");
         var c = Configure(opts.Adapter);

         var bl = c.Resolve<BusLoad>();
         bl.Start(opts.Percent, opts.Quiet);
         Console.WriteLine("Press a key to exit");
         Console.Read();
         bl.Stop();
         return 0;
      }

      static int StartPoolUpload(UploadOptions opts)
      {
         return 0;
      }

      static IContainer Configure(string adapter)
      {
         var container = new Container();
         container.Register<IConnection, DefaultConnection>(Reuse.Singleton);

         container.Register<BusLoad>();
         container.Register<PeriodicValueChange>();

         if (adapter == "pcan")
         {
            container.Register<INetwork, PcanNetwork>(Reuse.Singleton);
         }
         else if (adapter == "udp")
         {
            container.Register<INetwork, UdpMulticastNetwork>(Reuse.Singleton);
            var config = new UdpConfiguration()
            {
               RemotePort = 25000,
               RemoteAddress = "239.0.0.222"
            };
            container.RegisterDelegate<UdpConfiguration>(f => config, Reuse.Singleton);
         }
         else if (adapter == "vc")
         {
            Console.WriteLine("VC adapter is weird, build for 32-bit");
            //container.Register<INetwork, ValueCanNetwork>(Reuse.Singleton);
         }

         container.Register<INameTable, NameTable>(Reuse.Singleton);
         container.Register<SessionManager>(Reuse.Singleton);
         container.Register<MessageHandler, RequestForAddressClaimedHandler>();
         container.Register<MessageHandler, AddressClaimedHandler>();

         container.Register<MessageHandler, TransportClearToSendHandler>();
         container.Register<MessageHandler, TransportEndOfMessageHandler>();
         container.Register<MessageHandler, ExtendedDataPacketOffsetHandler>();
         container.Register<MessageHandler, ExtendedDataTransferHandler>();
         container.Register<MessageHandler, ExtendedRequestToSendHandler>();
         container.Register<MessageHandler, ExtendedTransportAbortHandler>();

         container.Register<ISerialization, Serializer>();
         container.Register<IDeserialization, ObjectPoolReader>();
         container.Register<VirtualTerminal>(Reuse.Singleton);

         container.Register<IRepeatedTask, AddressClaimTask>();
         container.Register<WorkingSetMaintenanceTask>(Reuse.Singleton);
         container.RegisterDelegate<IRepeatedTask>(f => container.Resolve<WorkingSetMaintenanceTask>());
         //container.Register<IRepeatedTask, WorkingSetMaintenanceTask>(Reuse.Singleton);

         var logger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .WriteTo.Trace()
            .CreateLogger();
         container.RegisterDelegate<ILogger>(f => logger, Reuse.Singleton);

         var progressSubject = new Subject<int>();
         container.RegisterDelegate<Subject<int>>(f => progressSubject, Reuse.Singleton);

         container.Register<IBusLogger, FileLog>(Reuse.Singleton);
         container.Register<IFileSystem, FileSystem>();
         container.RegisterDelegate<PlatformConfig>(f => new PlatformConfig() { BaseDirectory = "data" }, Reuse.Singleton);

         container.Register<VtClient>();

         return container;
      }
   }
}
