﻿using Distek.Platform;
using Distek.Vt.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distek.VtClient
{
   public class PeriodicValueChange
   {
      public PeriodicValueChange(IConnection connection)
      {
         _connection = connection;
      }

      public void Start()
      {
         _tokenSource = new CancellationTokenSource();
         var ct = _tokenSource.Token;
         var count = 0;
         var task = new Task(async () =>
         {
            var delay = 1000;
            while(true)
            {
               if(ct.IsCancellationRequested)
               {
                  Console.WriteLine("Shutting down value change test");
                  return;
               }
               var msg = new ChangeNumericValue()
               {
                  ObjectId = 12000,
                  Value = count,
               };
               _connection.Send(msg);
               Console.WriteLine("Value on screen should be {0}", count);
               count++;
               await Task.Delay(delay);
            }
         }, _tokenSource.Token);
         task.Start();
      }

      public void Stop()
      {
         _tokenSource.Cancel();
      }

      private IConnection _connection;
      private CancellationTokenSource _tokenSource;
   }
}
