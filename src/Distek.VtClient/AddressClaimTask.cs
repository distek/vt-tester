﻿using Distek.DataLink;
using Distek.DataLink.Messages;
using Distek.Platform;
using Distek.Vt;
using Distek.Vt.Commands;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.VtClient
{
	public class AddressClaimTask : IRepeatedTask
	{
		int IRepeatedTask.Interval { get { return 250; } }

		public AddressClaimTask(IConnection connection, INameTable nameTable, ILogger log)
		{
			_connection = connection;
			_nameTable = nameTable;
			_log = log;
			_state = ClaimState.Startup;
		}

		void IRepeatedTask.Tick()
		{
			switch (_state)
			{
				case ClaimState.Startup:
					_connection.Send(new RequestForAddressClaimedMessage());
					_state = ClaimState.SentRequestForClaims;
					break;
				case ClaimState.SentRequestForClaims:
					var localName = new Name()
					{
						IdentityNumber = 1,
						ManufacturerCode = 514,
						EcuInstance = 0,
						FunctionInstance = 0,
						Function = 19,
						DeviceClass = 0,
						DeviceClassInstance = 0,
						IndustryGroup = IndustryGroup.AgricultureAndForestryEquipment,
						SelfConfigurableAddress = true
					};
					_nameTable.LocalName = localName;

					_connection.Send(new AddressClaimMessage(_nameTable.GetVacantAddress(), localName));
					_state = ClaimState.Claimed;
					break;
				case ClaimState.Claimed:
					_nameTable.ClaimComplete = true;
					_state = ClaimState.Done;
					break;
				case ClaimState.Done:
					// we could shut down the task here if we need to
					break;
				default:
					break;
			}
		}

		private IConnection _connection;
		private INameTable _nameTable;
		private readonly ILogger _log;
		private ClaimState _state;

		private enum ClaimState
		{
			Startup,
			SentRequestForClaims,
			Claimed,
			Done
		}
	}
}
