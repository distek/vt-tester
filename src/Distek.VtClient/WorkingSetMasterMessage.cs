﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using Distek.Vt.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.VtClient
{
	[VtFunction((IsobusMessageId)1)]
    public class WorkingSetMasterMessage : IsoVtMessage
    {
		//[ByteOrder(1, 6)]
		//public byte[] Reserved {  get { return new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; } }

		public WorkingSetMasterMessage()
		{
			this.ParameterGroupNumber = 0xFE0D;
			this.Priority = 7;
			this.DataPage = 0;
			// todo: need to look up this message's data meaning
			this.Data = new byte[] { 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
		}
    }

	public class WorkingSetMaintenanceTask : IRepeatedTask
	{
		public int Interval => 1000;

		public bool ConnectionReady { get; private set; }

		public WorkingSetMaintenanceTask(INameTable nameTable, IConnection connection)
		{
			_nameTable = nameTable;
			_connection = connection;
			ConnectionReady = false;
		}

		public void Tick()
		{
			if(_nameTable.ClaimComplete)
			{
				// send ws master
				if(!_sentMaster)
				{
					var msg = new WorkingSetMasterMessage();
					Console.WriteLine("Sending ws master {0}", msg.ToString());
					_connection.Send(msg);
					_sentMaster = true;
               _firstTime = true;
				}
				else
				{
					var msg = new WorkingSetMaintenanceMessage()
					{
						FirstTime = _firstTime,
						VersionNumber = 3,
						DestinationAddress = 0x26,
					};
					Console.WriteLine("Sending ws maintenance");
					if (_firstTime)
					{
						ConnectionReady = true;
					}
					_connection.Send(msg);
					_firstTime = false;
				}
			}
		}

		private readonly INameTable _nameTable;
		private readonly IConnection _connection;
		private bool _sentMaster = false;
		private bool _firstTime = true;
	}
}
