﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using Distek.Vt;
using Distek.Vt.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Linq;
using System.Text;

namespace Distek.VtClient
{
    public class VtClient
    {
		public VtClient(IConnection connection, SessionManager session, IEnumerable<IRepeatedTask> tasks, WorkingSetMaintenanceTask wsMaintenance)
		{
			_connection = connection;
			_session = session;
         _tasks = tasks;
         _wsMaintenance = wsMaintenance;
      }

      public bool StartTestClient()
      {
         foreach (var t in _tasks)
         {
            Observable
               .Interval(TimeSpan.FromMilliseconds(t.Interval))
               .Subscribe(x => t.Tick());
         }

         var count = 0;
         while (_wsMaintenance.ConnectionReady == false)
         {
            System.Threading.Thread.Sleep(1000);
            count++;
            if (count > 10)
               return false;
         }
         return true;
      }

		public void SendObjectPool(string filename)
		{
			// send get memory
			//var getMemory = new GetMemoryMessage
			var getMem = new CanMessage(0x1CE72600, new byte[] { 0xC0, 0xFF, 0xC4, 0x9D, 0x03, 0x00, 0xFF, 0xFF });
			_connection.Send(getMem);
			System.Threading.Thread.Sleep(500);

         var iop = File.ReadAllBytes(filename);
         Console.WriteLine("Sending {0} byte object pool", iop.Length);
         var poolData = new ObjectPoolTransferMessage()
         {
            Objects = iop
         };
         _connection.Send(poolData);

         var txSession = _session.CurrentTransmitSession;
         while (txSession.IsComplete == false)
         {
            System.Threading.Thread.Sleep(500);
         }

         var eop = new CanMessage(0x18E72600, new byte[] { 18, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF });
         _connection.Send(eop);

         Console.WriteLine("Sent first pool");
         //System.Threading.Thread.Sleep(1500);

         ////iop = File.ReadAllBytes(@"..\..\..\object-pool-2.iop");
         //iop = File.ReadAllBytes(@"..\..\..\object-pool\pool-2\Output\pool-2.iop");
         //Console.WriteLine("Sending {0} byte object pool", iop.Length);
         //poolData = new ObjectPoolTransferMessage()
         //{
         //   Objects = iop
         //};
         //_connection.Send(poolData);

         //txSession = _session.CurrentTransmitSession;
         //while (txSession.IsComplete == false)
         //{
         //   System.Threading.Thread.Sleep(500);
         //}

         //_connection.Send(eop);
         //Console.WriteLine("Finished sending 2nd pool");
      }

		private IConnection _connection;
		private SessionManager _session;
      private IEnumerable<IRepeatedTask> _tasks;
      private WorkingSetMaintenanceTask _wsMaintenance;
    }
}
