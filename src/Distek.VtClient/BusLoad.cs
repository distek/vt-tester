﻿using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distek.VtClient
{
   public class BusLoad
   {
      public BusLoad(INetwork network)
      {
         _network = network;
         _network.Connect();
      }

      public void Start(int busLoad, bool quiet)
      {
         _tokenSource = new CancellationTokenSource();
         var ct = _tokenSource.Token;
         _task = new Task(async () =>
         {
            // (250k / (128)) = 0.000512 -> frames/sec for 100% bus load
            // convert to delay between frames in ms.  we can probably only get to about 40%
            var delay = (int)(((100.0f / busLoad) * 0.0005) * 1000);
            Console.WriteLine("Bus load of {0}% -- cycle time: {1}ms", busLoad, delay);
            while(!ct.IsCancellationRequested)
            {
               var msg = new CanMessage(0x0CDEBAFE, new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 });
               if (!quiet)
                  Console.WriteLine(msg.ToString());
               _network.Send(msg);
               await Task.Delay(delay);
            }
         }, _tokenSource.Token);
         _task.Start();
      }

      public void Stop()
      {
         _tokenSource.Cancel();
      }

      private readonly INetwork _network;
      private Task _task;
      private CancellationTokenSource _tokenSource;
   }
}
