﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Distek.VtClient
{
   public class BaseOptions
   {
      [Option('a', "adapter", Required = true)]
      public string Adapter { get; set; }
      [Option('q', "quiet", HelpText = "Disables console CAN logging", Default = false)]
      public bool Quiet { get; set; }
   }

   [Verb("load", HelpText = "Create load on the bus")]
   public class BusLoadOptions : BaseOptions
   {
      [Option('p', "percent", Default = 10, HelpText = "Percent bus load to create", Required = true)]
      public int Percent { get; set; }
   }

   [Verb("upload", HelpText = "Upload an object pool from an iop file")]
   public class UploadOptions : BaseOptions
   {
      [Option('f', "file", HelpText = "IOP file name to send", Required = true)]
      public string IopFile { get; set; }
   }
}
