import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: require('@/components/basic.vue').default
    },
    {
      path: '/canlog',
      name: 'CanLog',
      component: require('@/components/canlog.vue').default
    },
    {
      path: '/diagnostics',
      name: 'Diagnostics',
      component: require('@/components/diagnostics.vue').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
