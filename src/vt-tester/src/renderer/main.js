import Vue from 'vue'
import axios from 'axios'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import App from './App'
import router from './router'

var config = {
   baseURL: 'http://localhost:53427/api/',
   timeout: 30000,
}
Vue.http = Vue.prototype.$http = axios.create(config)
Vue.config.productionTip = false

Vue.use(Vuetify)


/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>'
}).$mount('#app')

// pretty sure this is a bad idea, but i don't like all of the $attrs is readonly warnings
Vue.config.silent = true;

