﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class PreferredAssignmentHandler : MessageHandler
    {
      public PreferredAssignmentHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == 34) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var fakeResponse = new CanMessage()
            {
               Priority = 7,
               ParameterGroupNumber = 0xE600,
               DestinationAddress = m.SourceAddress,
               Data = new byte[] { 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }
            } as IResponse;
            return Task.FromResult<IResponse>(fakeResponse);
         };
      }
    }
}
