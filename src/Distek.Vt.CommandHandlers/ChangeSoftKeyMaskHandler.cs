﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class ChangeSoftKeyMaskHandler : MessageHandler
    {
      public ChangeSoftKeyMaskHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeSoftKeyMask) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var maskType = m.Data[1];
            var dataMask = BitConverter.ToUInt16(m.Data, 2);
            var skMask = BitConverter.ToUInt16(m.Data, 4);

            var response = new ChangeSoftKeyMaskResponse()
            {
               DestinationAddress = m.SourceAddress,
               ErrorCode = ChangeSoftKeyMaskError.NoError,
               DataAlarmMaskObjectId = dataMask,
               SoftKeyMaskObjectId = skMask,
            };
            return Task.FromResult<IResponse>(response);
         };

      }
    }
}
