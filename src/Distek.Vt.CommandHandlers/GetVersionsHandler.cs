﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for E.2 Get Versions message
   /// </summary>
   public class GetVersionsHandler : MessageHandler
   {
      public GetVersionsHandler(INonVolatileMemory nvMemory, ILogger log, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.GetVersions) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            log.Debug("GetVersionsHandler {0:X}", m.Header);
            if(string.IsNullOrEmpty(m.SourceCanName))
            {
               log.Error("GetVersionsHandler called from null source can name");
               return Task.FromResult<IResponse>(null);
            }
            var storedVersions = nvMemory.GetVersionLabels(m.SourceCanName);
            var versionBytes = new List<byte[]>();
            foreach(var s in storedVersions)
            {
               var parts = s.Split(new[] { '-' });
               var bytes = parts.Select(p => Convert.ToByte(p, 16));
               versionBytes.Add(bytes.ToArray());
            }
            return Task.FromResult<IResponse>(new GetVersionsResponse(m.SourceAddress)
            {
               VersionLabels = versionBytes,
               DestinationAddress = m.SourceAddress,
            });
         };
      }
   }
}
