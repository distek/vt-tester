﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class SelectInputObjectHandler : MessageHandler
   {
      public SelectInputObjectHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.SelectInputObject) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var option = m.Data[3];

            var response = new SelectInputObjectResponse()
            {
               DestinationAddress = m.SourceAddress,
               ObjectId = objectId,
               Response = SelectInputObjectObjectResponses.ObjectIsSelected,
               ErrorCode = SelectInputObjectError.NoError
            };
            return Task.FromResult<IResponse>(response);
         };
      }
   }
}
