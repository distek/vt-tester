﻿/*using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class ChangeBackgroundColourHandler : MessageHandler
   {
      public ChangeBackgroundColourHandler(VirtualTerminal vt)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeBackgroundColour) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         Handler = m =>
         {
            var changedObject = BitConverter.ToUInt16(m.Data, 1);

            if (vt.CurrentClient != null)
            {
               var pool = vt.CurrentClient.ObjectPool;
               if (pool != null)
               {
                  if (pool.Objects.Any(o => o.ObjectId == changedObject) == false)
                     return new ChangeBackgroundColourResponse() { DestinationAddress = m.SourceAddress, ObjectId = changedObject, ErrorCode = ChangeBackgroundColourError.InvalidObjectId };

                  var message = new ChangeBackgroundColourCommand()
                  {
                     BackgroundColour = new IsoColour(m.Data[3])
                  };

                  var responses = pool.FindHandlerById<ChangeBackgroundColourCommand>(changedObject)
                      .Select(o => o.OnMessage(message));
                  return responses.First();
               }
            }
            return null;
         };
      }
   }
}*/
