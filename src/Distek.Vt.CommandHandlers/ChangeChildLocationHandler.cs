﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using Distek.Vt.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class ChangeChildLocationHandler : MessageHandler
    {
      public ChangeChildLocationHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeChildLocation) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var cmd = new ChangeChildLocation()
            {
               ParentObjectId = BitConverter.ToUInt16(m.Data, 1),
               ChildObjectId = BitConverter.ToUInt16(m.Data, 3),
               XLocation = (int)m.Data[5],
               YLocation = (int)m.Data[6],
            };

            var resp = new ChangeChildLocationResponse()
            {
               Priority = 6,
               
               DestinationAddress = m.SourceAddress,
               ParentObjectId = cmd.ParentObjectId,
               ChildObjectId = cmd.ChildObjectId,
               ErrorCode = ChangeChildLocationPositionError.NoError,
            };
            return Task.FromResult<IResponse>(resp);
         };
        /* AsyncHandler = m =>
         {
            var fakeResponse = new CanMessage()
            {
               Priority = 7,
               ParameterGroupNumber = 0xE600,
               DestinationAddress = m.SourceAddress,
               Data = new byte[] { 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }
            } as IResponse;
            return Task.FromResult<IResponse>(fakeResponse);
         };*/
      }
    }
}
