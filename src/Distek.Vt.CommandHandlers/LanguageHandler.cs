﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using Distek.Vt.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class LanguageHandler : MessageHandler
   {
      public LanguageHandler(INameTable nameTable)
      {
         // Langauge request is a global message
         Filter = m => ((m.ParameterGroupNumber == 0xEAFF00) || ((m.PGN == 0xEA00) && m.DestinationAddress == nameTable.SourceAddress)) &&
            (m.Data[0] == 0x0F && m.Data[1] == 0xFE && m.Data[2] == 0x00);
         AsyncHandler = (m) =>
         {
            var response = new LanguageResponse()
            {
               LanguageCode = "en",
               DecimalSymbol = DecimalSymbol.PointIsUsed,
               TimeFormat = TimeFormat.TwelveHour,
               DistanceUnits = DistanceUnits.Imperial,
               AreaUnits = AreaUnits.Imperial,
               VolumeUnits = VolumeUnits.Imperial,
               MassUnits = MassUnits.US,
               TemperatureUnits = TemperatureUnits.Imperial,
               PressureUnits = PressureUnits.Imperial,
               ForceUnits = ForceUnits.Imperial,
               UnitsSystem = UnitsSystem.US
            }.ToBytes();
            return Task.FromResult<IResponse>((IResponse)response);
         };
      }
   }
}
