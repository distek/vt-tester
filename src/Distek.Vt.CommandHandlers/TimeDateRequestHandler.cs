﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /*
      Somebody i have a log for sent this response:
      93 36 0E 0B 16 20 7D 7D
      seconds = 36.75 ( 0x93 -> (147 / 4))
      minutes = 54
      hours = 14
      month = 11
      day = (0x16) -> 22 / 4 = 5.5
      year = 32 + 1985 = 2017
    */
   public class TimeDateMessage : CanMessage, IResponse
   {
      public DateTime CurrentTime { get; set; }

      public TimeDateMessage()
      {
         Priority = 6;
         ParameterGroupNumber = 0xFEE6;

         // Note that we're setting local hour offset to 0xFA so that we only have to
         // return the local time
         //CurrentTime = DateTime.UtcNow;
         CurrentTime = DateTime.Now;
      }

      public CanMessage ToBytes()
      {
         var data = new byte[8];
         data[0] = (byte)(CurrentTime.Second * 4);
         data[1] = (byte)(CurrentTime.Minute);
         data[2] = (byte)(CurrentTime.Hour);
         data[3] = (byte)(CurrentTime.Month);
         data[4] = (byte)(CurrentTime.Day * 4);
         data[5] = (byte)(CurrentTime.Year - 1985);

         // specify that we're returning local time (125 + 125offset)
         data[6] = 0x7D;
         data[7] = 0x7D;
         this.Data = data;
         return this;
      }

   }
    public class TimeDateRequestHandler : MessageHandler
    {
      public TimeDateRequestHandler(INameTable nameTable)
      {
         Filter = m => ((m.PGN == 0xEA00) && (m.DestinationAddress == nameTable.SourceAddress)) &&
            (m.Data[0] == 0xE6 && m.Data[1] == 0xFE && m.Data[2] == 0x00);
         AsyncHandler = (m) =>
         {
            var response = new TimeDateMessage().ToBytes();
            return Task.FromResult<IResponse>((IResponse)response);
         };
      }
    }
}
