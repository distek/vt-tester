﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class EnableDisableObjectHandler : MessageHandler
    {
      public EnableDisableObjectHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.EnableDisableObject) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var response = new EnableDisableObjectResponse()
            {
               DestinationAddress = m.SourceAddress,
               ObjectId = BitConverter.ToUInt16(m.Data, 1),
               EnableOrDisable = m.Data[3],
               ErrorCode = EnableDisableObjectError.NoError,
            };
            return Task.FromResult<IResponse>(response);
         };
      }
    }
}
