﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class ChangeActiveMaskHandler : MessageHandler
   {
      public ChangeActiveMaskHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeActiveMask) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var workingSetObjectId = BitConverter.ToUInt16(m.Data, 1);
            var newMaskId = BitConverter.ToUInt16(m.Data, 3);

            //log.Debug("Current working set name: {0}", vt.CurrentClient.Name);
            //var pool = vt.CurrentClient.ObjectPool;
            //if (pool != null)
            //{
            //   // todo: are values signed or unsigned?  i think they're unsigned
            //   var newMaskId = BitConverter.ToUInt16(m.Data, 3);

            //   // Check for invalid working set object id
            //   if (pool.Objects.Any(o => o.ObjectId == workingSetObjectId) == false)
            //      return new ChangeActiveMaskResponse() { DestinationAddress = m.SourceAddress, ActiveMaskObjectId = workingSetObjectId, ErrorCode = ChangeActiveMaskError.InvalidWorkingSetObjectId };

            //   // Check for invalid mask object id
            //   if (pool.Objects.Any(o => o.ObjectId == newMaskId) == false)
            //      return new ChangeActiveMaskResponse() { DestinationAddress = m.SourceAddress, ActiveMaskObjectId = newMaskId, ErrorCode = ChangeActiveMaskError.InvalidActiveMaskObjectId };

            //   // Create the specific message type to send to the vt objects
            //   var message = new ChangeActiveMaskCommand() { ActiveMaskObjectId = newMaskId };

            //   var client = vt.CurrentClient;
            //   var ws = vt.CurrentClient.ObjectPool.Objects.OfType<Objects.WorkingSet>().FirstOrDefault();

            //   // Check if this is an alarm mask object
            //   if (vt.CurrentClient.ObjectPool.Objects.OfType<Objects.AlarmMask>().FirstOrDefault(o => o.ObjectId == newMaskId) != null)
            //   {
            //      errorHandler.RaiseError(vt.CurrentClient.ObjectPool, newMaskId);
            //   }
            //   var response = ws.OnMessage(message);
            //   return response;
            //}
            //return null;
            var message = new ChangeActiveMaskResponse()
            {
               ActiveMaskObjectId = newMaskId,
               DestinationAddress = m.SourceAddress,
               ErrorCode = ChangeActiveMaskError.NoError,
            };
            return Task.FromResult<IResponse>(message);
         };
      }
   }
}
