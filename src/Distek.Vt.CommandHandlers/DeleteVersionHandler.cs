﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for E.8 Delete Version command
   /// </summary>
   public class DeleteVersionHandler : MessageHandler
   {
      public DeleteVersionHandler(INonVolatileMemory nvMemory, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.DeleteVersion) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            //var versionLabel = Encoding.UTF8.GetString(m.Data, 1, 7);
            var versionLabel = BitConverter.ToString(m.Data.Skip(1).ToArray());
            var deleted = nvMemory.DeleteObjectPoolVersion(m.SourceCanName, versionLabel);
            return Task.FromResult<IResponse>(new DeleteVersionResponse(m.SourceAddress)
            {
               ErrorCode = deleted ? DeleteVersionError.NoError : DeleteVersionError.IncorrectVersionLabel,
               DestinationAddress = m.SourceAddress,
            });
         };
      }
   }
}
