﻿/*using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class ChangeEndpointHandler : MessageHandler
   {
      public ChangeEndpointHandler(VirtualTerminal vt)
      {
         Filter = (m) => (m.Data[0] == (int)IsobusMessageId.ChangeEndPoint) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         Handler = m =>
         {
            var command = new ChangeEndPointCommand()
            {
               Width = BitConverter.ToInt16(m.Data, 3),
               Height = BitConverter.ToInt16(m.Data, 5),
               LineDirection = m.Data[7],
            };

            var changedObject = BitConverter.ToUInt16(m.Data, 1);
            var objects = vt.CurrentClient.ObjectPool;
            var responses = objects.FindHandlerById<ChangeEndPointCommand>(changedObject)
                   .Select(o => o.OnMessage(command));
            return responses.First();
         };
      }
   }
}*/
