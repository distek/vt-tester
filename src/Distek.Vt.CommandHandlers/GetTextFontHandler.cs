﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for D.6 Get Text Font message
   /// </summary>
   public class GetTextFontHandler : MessageHandler
   {
      public GetTextFontHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.GetTextFont) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            return Task.FromResult<IResponse>(new GetTextFontResponse()
            {
               // Supporting all fonts except the ones that have duplicate widths
               SmallFontSizes = 0x6D,
               LargeFontSizes = 0x5B,
               TypeAttribute = 0x7F,
               DestinationAddress = m.SourceAddress,
            });
         };
      }
   }
}
