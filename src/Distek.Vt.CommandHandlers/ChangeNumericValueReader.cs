﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for F.22 Change Numeric Value command
   /// </summary>
   public class NumericValueChangeHandler : MessageHandler
   {
      public NumericValueChangeHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeNumericValue) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            // Extract the values from the raw bytes here - see the standard for byte meanings
            var changedObject = BitConverter.ToUInt16(m.Data, 1);

            //// Check for message specific errors here.  The handler can also return errors
            //if (vt.CurrentClient != null)
            //      {
            //         //var pool = vt.CurrentClient.ObjectPool;
            //         //if (pool != null)
            //         //{
            //         //   if (pool.Objects.Any(o => o.ObjectId == changedObject) == false)
            //         //      return new ChangeNumericValueResponse() { DestinationAddress = m.SourceAddress, ObjectId = changedObject, ErrorCode = ChangeNumericValueError.InvalidObjectId };

            //      // todo: are values signed or unsigned?  i think they're unsigned
            //      var newValue = BitConverter.ToInt32(m.Data, 4);

            //      // Create the specific message type to send to the vt objects
            //      var message = new ChangeNumericValueCommand(m.SourceAddress) { Value = newValue };

            //      // i'd rather have the 'parent' object listen for changes to the number variable
            //      // but with how we're parsing i don't think that's possible.  we don't necessarily
            //      // have the number variable parsed when we get to the parent object.  also i don't
            //      // want parsing to be dependent on the rest of the pool
            //      var handlers = pool.FindHandlerById<ChangeNumericValueCommand>(changedObject);
            //            var responses = new List<IResponse>();
            //            foreach (var h in handlers)
            //               responses.Add(h.OnMessage(message));
            //      // TODO: figure out how to actually send the correct response
            //      return responses.FirstOrDefault();
            //         }
            //      }
            //      return null;
            var response = new ChangeNumericValueResponse()
            {
               DestinationAddress = m.SourceAddress,
               ObjectId = changedObject,
               ErrorCode = ChangeNumericValueError.NoError,
            };
            return Task.FromResult<IResponse>(response);
            //   };
         };

      }
   }
}