﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for D.2 Get Memory message
   /// </summary>
   public class GetMemoryHandler : MessageHandler
   {
      public GetMemoryHandler(ILogger logger, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.GetMemory) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            logger.Debug("GetMemory Handler {0:X}", m.Header);
            return Task.FromResult<IResponse>(
               new GetMemoryResponse()
               {
                  // VTv3
                  VersionNumber = 3,
                  // There can be enough memory -- hardcoded for now
                  Status = 0,
                  DestinationAddress = m.SourceAddress,
               }
            );
         };
      }
   }
}
