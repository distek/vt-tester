﻿using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Serilog;
using Distek.DataLink;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for E.6 Load Version command
   /// </summary>
   public class LoadVersionHandler : MessageHandler
   {
      public LoadVersionHandler(INonVolatileMemory nvMemory, VirtualTerminal vt, ILogger log, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.LoadVersion) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var response = default(IResponse);
            var versionLabel = BitConverter.ToString(m.Data.Skip(1).ToArray());
            log.Debug("LoadVersion: {0}", versionLabel);
            vt.Status = VtBusyCode.BusyExecutingACommand;
            var objectPool = nvMemory.LoadObjectPool(m.SourceCanName, versionLabel);
            if (objectPool != null)
            {
               var workingSet = vt.ConnectedClients.Where(ws => ws.Name == m.SourceCanName).FirstOrDefault();
               if (workingSet != null)
               {
                  // The horizon controller sends 2 load version requests, and if we pay attention to the second one we get 2 ws icons
                  // it might be becasue vt status isn't set to busy
                  if (workingSet.ObjectPool != null)
                  {
                     response = new LoadVersionResponse(m.SourceAddress) { ErrorCode = LoadVersionError.NoError };
                  }
                  else
                  {
                     var wsObject = objectPool.Objects.OfType<Objects.WorkingSet>().FirstOrDefault();
                     if (wsObject != null)
                     {
                        var ws = wsObject;// as Distek.Vt.WorkingSet;
                        workingSet.ObjectPool = objectPool;
                        workingSet.ActiveMaskId = ws.ActiveMaskId;
                        workingSet.State = WorkingSetState.ObjectPoolReady;
                        log.Debug("Loaded object pool from disk");
                        response = new LoadVersionResponse(m.SourceAddress)
                        {
                           ErrorCode = LoadVersionError.NoError,
                        };
                     }
                     else
                     {
                        throw new InvalidObjectPoolException("Object pool does not have a working set object");
                     }
                  }
               }
            }
            else
            {
               // TODO: can't really envision any other error besides this happening.
               // maybe need more resolution from the nvmem interface
               response = new LoadVersionResponse(m.SourceAddress)
               {
                  ErrorCode = LoadVersionError.IncorrectVersionLabel,
               };
            }

            vt.Status = VtBusyCode.NotBusy;
            return Task.FromResult<IResponse>(response);
      };
   }
}
}
