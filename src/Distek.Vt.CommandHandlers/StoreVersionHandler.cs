﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for E.4 Store Version command
   /// </summary>
   public class StoreVersionHandler : MessageHandler
   {
      /*
         The Store Version command allows a Working Set to store the copy of the actual object pool into the nonvolatile
         storage of the VT. This message can be sent at any time. The copy is stored as the version indicated
         by version label. If a copy with the same version label already exists in the non-volatile storage area, it is
         overwritten. All objects are stored as they are (with current attributes, input values etc.). If the version label
         contains no string (all blanks) the last stored version in non-volatile storage shall be overwritten; alternatively,
         if there is no version stored up to that point, an error shall be indicated by the VT.
       */
      public StoreVersionHandler(INonVolatileMemory nvMemory, VirtualTerminal vt, ILogger log, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.StoreVersion) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            log.Debug("StoreVersionHandler");
            var versionLabel = BitConverter.ToString(m.Data.Skip(1).ToArray());
            var workingSet = vt.ConnectedClients.FirstOrDefault(ws => ws.Name.Equals(m.SourceCanName));
            if (workingSet == null)
               return Task.FromResult<IResponse>(new StoreVersionResponse(m.SourceAddress) { ErrorCode = StoreVersionError.IncorrectVersionLabel });
            // Does the working set already have a version label?  we probably don't need to create a new one here
            var objectPool = new ObjectPool()
            {
               WorkingSetMasterName = m.SourceCanName,
               VersionLabel = versionLabel,
               Objects = workingSet.ObjectPool.Objects,
            };
            if (nvMemory.SaveObjectPool(objectPool) == true)
            {
               return Task.FromResult<IResponse>(new StoreVersionResponse(m.SourceAddress)
               {
                  ErrorCode = StoreVersionError.NoError,
               });
            }
            else
            {
               return Task.FromResult<IResponse>(new StoreVersionResponse(m.SourceAddress)
               {
                  ErrorCode = StoreVersionError.AnyOtherError
               });
            }
         };
      }
   }
}
