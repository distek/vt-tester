﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class ChangeAttributeHandler : MessageHandler
    {
      public ChangeAttributeHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeAttribute) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var attrId = m.Data[3];

            var response = new ChangeAttributeResponse()
            {
               DestinationAddress = m.SourceAddress,
               ErrorCode = ChangeAttributeError.NoError,
               ObjectId = objectId,
               AttributeId = attrId,
            };
            return Task.FromResult<IResponse>(response);
         };
      }
    }
}
