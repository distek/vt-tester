﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for D.4 Get Number of Soft Keys message
   /// </summary>
   public class GetNumberOfSoftKeysHandler : MessageHandler
   {
      public GetNumberOfSoftKeysHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.GetNumberOfSoftKeys) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            // Hardcoding this for now.  It'd be nice to have this in a config file
            return Task.FromResult<IResponse>(new GetNumberOfSoftKeysResponse()
            {
               // for v4
               NavigationSoftKeys = 0xFF,
               XDots = 80,
               YDots = 80,
               VirtualSoftKeys = 64,
               PhysicalSoftKeys = 10,
               DestinationAddress = m.SourceAddress,
            });
         };
      }
   }
}
