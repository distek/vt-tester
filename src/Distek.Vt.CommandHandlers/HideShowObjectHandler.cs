﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class HideShowObjectHandler : MessageHandler
    {
      public HideShowObjectHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.HideShowObject) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var response = new HideShowObjectResponse(m.SourceAddress)
            {
               ObjectId = BitConverter.ToUInt16(m.Data, 1),
               Hidden = Convert.ToBoolean(m.Data[3]),
               ErrorCode = HideShowObjectError.NoError
            };
            return Task.FromResult<IResponse>(response);
         };
      }
    }
}
