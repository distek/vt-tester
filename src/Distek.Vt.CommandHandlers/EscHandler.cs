﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class EscHandler : MessageHandler
   {
      public EscHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.Esc) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var objectId = BitConverter.ToUInt16(m.Data, 1);

            var response = new EscResponse()
            {
               DestinationAddress = m.SourceAddress,
               ObjectId = objectId,
               ErrorCode = EscError.NoError
            };
            return Task.FromResult<IResponse>(response);
         };
      }
   }
}
