﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for D.8 Get Hardware message
   /// </summary>
   public class GetHardwareHandler : MessageHandler
   {
      /// <summary>
      /// Handles D.8 Get Hardware Message
      /// </summary>
      public GetHardwareHandler(VirtualTerminal vt, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.GetHardware) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            return Task.FromResult<IResponse>(new GetHardwareResponse()
            {
               BootTime = 0xFF,
               GraphicType = 2,
               Hardware = 0xFD,    // has a touch screen, no pointing device, multiple freq audio, adjustable audio
               XPixels = vt.MaskSize,
               YPixels = vt.MaskSize,
               DestinationAddress = m.SourceAddress,
            });
         };
      }
   }
}
