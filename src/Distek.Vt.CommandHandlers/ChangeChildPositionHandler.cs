﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class ChangeChildPositionHandler : MessageHandler
   {
      public ChangeChildPositionHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeChildPosition) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var cmd = new ChangeChildPositionCommand()
            {
               ParentObjectId = BitConverter.ToUInt16(m.Data, 1),
               ChildObjectId = BitConverter.ToUInt16(m.Data, 3),
               XPosition = BitConverter.ToUInt16(m.Data, 5),
               YPosition = BitConverter.ToUInt16(m.Data, 7)
            };
            var changedObject = cmd.ParentObjectId;
            //var objects = vt.CurrentClient.ObjectPool;
            //var response = objects.FindHandlerById<ChangeChildPositionCommand>(changedObject)
            //   .Select(o => o.OnMessage(cmd));
            //return response.First();
            var resp = new ChangeChildPositionResponse()
            {
               DestinationAddress = m.SourceAddress,
               ParentObjectId = cmd.ParentObjectId,
               ChildObjectId = cmd.ChildObjectId,
               ErrorCode = ChangeChildLocationPositionError.NoError,
            };
            return Task.FromResult<IResponse>(resp);
         };
      }
   }
}
