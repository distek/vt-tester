﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for F.44 Delete Object Pool Command
   /// </summary>
   public class DeleteObjectPoolHandler : MessageHandler
   {
      public DeleteObjectPoolHandler(VirtualTerminal vt, IErrorHandler errorHandler, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.DeleteObjectPool) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var ws = vt.ConnectedClients.Where(w => w.MasterSourceAddress == m.SourceAddress).FirstOrDefault();

            // If this working set doesn't exist
            if (ws == null)
               return Task.FromResult<IResponse>(new DeleteObjectPoolResponse(m.SourceAddress)
               {
                  ErrorCode = DeleteObjectPoolError.DeletionError,
               });

            // if the current working set is deleted, go back to the internal default one
            if (vt.CurrentClient == ws)
               vt.CurrentClient.State = WorkingSetState.ObjectPoolDeleted;
            // Delete the object pool from the working set
            ws.ObjectPool = null;
            // TODO: Assuming I should delete the working set from a delete object pool command
            lock (vt.ConnectedClients)
            {
               vt.ConnectedClients.Remove(ws);
            }
            errorHandler.RaiseError(new WorkingSetDisconnect()
            {
               MasterSourceAddress = m.SourceAddress
            });
            return Task.FromResult<IResponse>(new DeleteObjectPoolResponse(m.SourceAddress)
            {
               ErrorCode = DeleteObjectPoolError.NoError,
            });
         };
      }
   }
}
