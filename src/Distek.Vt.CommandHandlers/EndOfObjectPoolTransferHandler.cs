﻿using Distek.DataLink;
using Distek.Platform;
using Distek.Vt.Messages;
using Serilog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class EndOfObjectPoolTransferHandler : MessageHandler
   {
      public EndOfObjectPoolTransferHandler(VirtualTerminal vt, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.EndOfObjectPool) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            Console.WriteLine("End of object pool transfer");
            var client = vt.ConnectedClients.FirstOrDefault(w => w.MasterSourceAddress == m.SourceAddress);
            // TODO: do something if we get this message for a working set that we don't know about
            if (client != default(VtClient))
            {
               client.State = WorkingSetState.PoolTransferred;
            }

            // Just set the vt status to busy until we're done parsing.  the end of object pool
            // response isn't sent until we're done parsing the pool
            vt.Status = VtBusyCode.BusyParsingObjectPool;

            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
