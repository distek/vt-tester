﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   /// <summary>
   /// Handler for F.18 Change Size command
   /// </summary>
   public class ChangeSizeHandler : MessageHandler
   {
      public ChangeSizeHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeSize) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var command = new ChangeSizeCommand()
            {
               Width = BitConverter.ToUInt16(m.Data, 2),
               Height = BitConverter.ToUInt16(m.Data, 4)
            };
            if (m.Data.Length >= 2)
            {
               var changedObject = BitConverter.ToUInt16(m.Data, 1);

               //var objects = vt.CurrentClient.ObjectPool;
               //var responses = objects.FindHandlerById<ChangeSizeCommand>(changedObject)
               //       .Select(o => o.OnMessage(command));
               //return Task.FromResult<IResponse>(responses.First());
               var response = new ChangeSizeResponse() { DestinationAddress = m.SourceAddress, ObjectId = changedObject, ErrorCode = ChangeSizeError.NoError };
               return Task.FromResult<IResponse>(response);
            }

            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
