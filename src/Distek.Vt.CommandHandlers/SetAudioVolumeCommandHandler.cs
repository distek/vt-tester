﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class SetAudioVolumeCommandHandler : MessageHandler
   {
      public SetAudioVolumeCommandHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.SetAudioVolume) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {

            var resp = new SetAudioVolumeResponse()
            {
               ErrorCode = SetAudioVolumeError.NoError,
            };
            return Task.FromResult<IResponse>(resp);
         };
      }
   }
}
