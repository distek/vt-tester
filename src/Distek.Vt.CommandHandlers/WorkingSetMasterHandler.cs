﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   // 18FE0D80   8   01 FF FF FF FF FF FF FF
   public class WorkingSetMasterHandler : MessageHandler
   {
      public WorkingSetMasterHandler(VirtualTerminal vt, IFileSystem fileSystem, ILogger logger, INameTable nameTable)
      {
         Filter = (m) => m.PGN == 0xFE0D;
         AsyncHandler = (m) =>
         {
            logger.Debug("WS master: {0:X}, {1}", m.Header, m.SourceAddress);
            if (nameTable.CanNames.ContainsKey(m.SourceAddress) == false)
            {
               return Task.FromResult<IResponse>(new Acknowledge()
               {
                  ControlByte = Acknowledegments.NegativeAcknowledgement,
                  GroupFunctionValue = 0xFF,              // TODO need to figure out how to populate this if it's even needed
                  AddressAcknowledgement = m.SourceAddress,
                  RequestingPGN = m.PGN,
                  DestinationAddress = m.SourceAddress,
               });
            }
            var ws = vt.ConnectedClients.Where(w => w.MasterSourceAddress == m.SourceAddress);
            if (ws.Count() == 0)
            {
               logger.Debug("Adding working set with name {0} and address {1}", m.SourceCanName, m.SourceAddress);
               lock (vt.ConnectedClients)
               {
                  vt.ConnectedClients.Add(
                     new VtClient(-1)
                     {
                        MasterSourceAddress = m.SourceAddress,
                        LastSeen = DateTime.Now,
                        Name = m.SourceCanName,
                        State = WorkingSetState.New,
                     });
               }
               // Create the directory for the iop if it doesn't exist
               fileSystem.CreateDirectory(m.SourceCanName);
            }
            // Do we need to handle anything for this?  At least parse out the number of working set members probably.
            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
