﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class ControlAudioSignalHandler : MessageHandler
    {
      public ControlAudioSignalHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ControlAudioSignal) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var resp = new ControlAudioSignalResponse()
            {
               DestinationAddress = m.SourceAddress,
               ErrorCode = ControlAudioSignalError.NoError,
            };
            return Task.FromResult<IResponse>(resp);
         };
      }
    }
}
