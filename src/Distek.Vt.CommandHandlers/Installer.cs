﻿
#if USE_AUTOFAC
using Autofac;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class Installer : Module
   {
      protected override void Load(ContainerBuilder builder)
      {
         // bringing these back to test with d-j
         builder.RegisterType<ChangeChildPositionHandler>().As<MessageHandler>();
         builder.RegisterType<ChangeSizeHandler>().As<MessageHandler>();
         builder.RegisterType<NumericValueChangeHandler>().As<MessageHandler>();
         builder.RegisterType<ChangeStringValueHandler>().As<MessageHandler>();
         builder.RegisterType<ChangeListItemHandler>().As<MessageHandler>();
         builder.RegisterType<ChangeActiveMaskHandler>().As<MessageHandler>();
         builder.RegisterType<ChangeAttributeHandler>().As<MessageHandler>();
         builder.RegisterType<ChangeSoftKeyMaskHandler>().As<MessageHandler>();

         builder.RegisterType<GetHardwareHandler>().As<MessageHandler>();
         builder.RegisterType<GetTextFontHandler>().As<MessageHandler>();
         builder.RegisterType<GetMemoryHandler>().As<MessageHandler>();
         builder.RegisterType<GetNumberOfSoftKeysHandler>().As<MessageHandler>();
         builder.RegisterType<DeleteVersionHandler>().As<MessageHandler>();
         builder.RegisterType<GetVersionsHandler>().As<MessageHandler>();
         builder.RegisterType<LoadVersionHandler>().As<MessageHandler>();
         builder.RegisterType<StoreVersionHandler>().As<MessageHandler>();
         // TESTING: remove this for debugging
         builder.RegisterType<WorkingSetMaintenanceHandler>().As<MessageHandler>();
         builder.RegisterType<WorkingSetMasterHandler>().As<MessageHandler>();
         builder.RegisterType<LanguageHandler>().As<MessageHandler>();
         //builder.RegisterType<DeleteObjectPoolHandler>().As<MessageHandler>();
         //builder.RegisterType<ChangeEndpointHandler>().As<MessageHandler>();
         //builder.RegisterType<ChangeBackgroundColourHandler>().As<MessageHandler>();
         //builder.RegisterType<SetAudioVolumeCommandHandler>().As<MessageHandler>();

         builder.RegisterType<ObjectPoolTransferHandler>().As<MessageHandler>();
         builder.RegisterType<EndOfObjectPoolTransferHandler>().As<MessageHandler>();

      }
   }
}
#endif
