﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class ObjectPoolTransferHandler : MessageHandler
   {
      public ObjectPoolTransferHandler(IDeserialization reader, VirtualTerminal vt, ILogger logger, INameTable nameTable)
      {
         Filter = m => (m.Data[0] == 17) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            Console.WriteLine("Object pool transfer handler");
            // skip the command byte
            var objectPoolBytes = m.Data.Skip(1);
            Debug.WriteLine(string.Format("Adding {0} bytes to object pool", objectPoolBytes.Count()));
            vt.TemporaryObjectPoolData = vt.TemporaryObjectPoolData.Concat(objectPoolBytes).ToList();
            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
