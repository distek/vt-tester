﻿using Distek.DataLink;
using Distek.Network;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class ChangeStringValueHandler : MessageHandler
   {
      public ChangeStringValueHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeStringValue) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            //var byteCount = BitConverter.ToUInt16(m.Data, 3);
            //var val = Encoding.UTF8.GetString(m.Data, 5, byteCount);

            //var pool = vt.CurrentClient.ObjectPool;
            //var handlers = pool.FindHandlerById<ChangeStringValue>(objectId);
            //var responses = new List<IResponse>();

            //var message = new ChangeStringValueCommand() { Value = val, TotalBytes = byteCount };
            //foreach (var h in handlers)
            //   responses.Add(h.OnMessage(message));
            var response = new ChangeStringValueResponse()
            {
               DestinationAddress = m.SourceAddress,
               ObjectId = objectId,
               ErrorCode = ChangeStringValueError.NoError
            };
            Console.WriteLine(string.Format("string change response: {0}", ((CanMessage)response).ToString()));
            return Task.FromResult<IResponse>(response);
         };
      }
   }
}
