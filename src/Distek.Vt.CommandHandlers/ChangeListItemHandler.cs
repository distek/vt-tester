﻿using Distek.DataLink;
using Distek.Platform;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
    public class ChangeListItemHandler : MessageHandler
    {
      public ChangeListItemHandler(INameTable nameTable)
      {
         Filter = m => (m.Data[0] == (int)IsobusMessageId.ChangeListItem) && (m.PGN == Constants.EcuToVt) && (m.DestinationAddress == nameTable.SourceAddress);
         AsyncHandler = m =>
         {
            var objectId = BitConverter.ToUInt16(m.Data, 1);
            var idx = m.Data[3];
            var newId = BitConverter.ToUInt16(m.Data, 4);

            var response = new ChangeListItemResponse()
            {
               DestinationAddress = m.SourceAddress,
               ErrorCode = ChangeListItemError.NoError,
               ObjectId = objectId,
               ListIndex = idx,
               NewObjectId = newId,
            };
            return Task.FromResult<IResponse>(response);

         };
      }
   }
}
