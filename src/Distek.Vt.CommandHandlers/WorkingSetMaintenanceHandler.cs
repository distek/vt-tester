﻿using Distek.DataLink;
using Distek.Platform;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Vt.CommandHandlers
{
   public class WorkingSetMaintenanceHandler : MessageHandler
   {
      public WorkingSetMaintenanceHandler(VirtualTerminal vt, ILogger logger, INameTable nameTable)
      {
         Filter = (m) =>
         {
            return ((m.PGN == Constants.EcuToVt) && (m.Data[0] == 0xFF)) && (m.DestinationAddress == nameTable.SourceAddress);
         };
         AsyncHandler = (m) =>
         {
            var ws = vt.ConnectedClients.Where(w => w.MasterSourceAddress == m.SourceAddress).FirstOrDefault();

            logger.Debug("WS Maintenance from {0}", m.SourceAddress);
            // If the working set isn't in our list of working sets
            if(ws == null)
            {
               logger.Warning("Got working set maintenance message from an unknown client.  SA: {0}", m.SourceAddress);
               // Send a NACK
               return Task.FromResult<IResponse>(new Acknowledge()
               {
                  ControlByte = Acknowledegments.NegativeAcknowledgement,
                  GroupFunctionValue = 0xFF,              // TODO need to figure out how to populate this if it's even needed
                  AddressAcknowledgement = m.SourceAddress,
                  RequestingPGN = m.PGN,
                  DestinationAddress = m.SourceAddress,
               });
            }

            ws.LastSeen = DateTime.Now;
            return Task.FromResult<IResponse>(null);
         };
      }
   }
}
