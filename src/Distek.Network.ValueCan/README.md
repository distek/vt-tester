﻿ValueCAN3 Network
-----------------

This is an implementation of `INetwork` to directly interface with a ValueCAN3 adapter.


## Running

This code uses p/invoke to talk to the hardware driver, so you'll have to copy `icsneo40.dll` to the output directory where the application runs from.
This .dll should come from the driver installation.

Since `icsneo40.dll` is a 32-bit native dll we have to run the .net core application in 32-bit mode.  Unfortunately, we can't just switch the project platform to x86 because of VS/dotnet problems.

So, instead you have to install the 32-bit version of `dotnet` from here: https://www.microsoft.com/net/download/core.  Once that's installed, you can run the CAN log program from the command line with:

```
"c:\Program Files (x86)\dotnet\dotnet.exe" Distek.CanLog.dll
```


note: i got this to build and run with the driver by publishing in 32-bit mode which seems to work now:
```
dotnet publish -c release -r win10-x86
```
