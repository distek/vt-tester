﻿using Distek.Network;
using neoCSNet2003;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Distek.Network.ValueCan
{
   public class ValueCanNetwork : INetwork
   {
      string INetwork.Key
      {
         get { return "vc3"; }
      }

      void INetwork.Connect()
      {
         _vc = new ValueCan();
      }

      async void INetwork.Receive(Action<CanMessage> handler)
      {
         await Task.Run(async () =>
         {
            while (true)
            {
               var m = _vc.Read();
               foreach (var msg in m)
                  handler(msg);
                  //Console.WriteLine(msg.ToString());
               await Task.Delay(0);
            }
         });
      }

      void INetwork.Send(CanMessage message)
      {
         var vcMessage = new icsSpyMessage()
         {
            ArbIDOrHeader = (int)message.Header,
            StatusBitField = 4,
            StatusBitField2 = 0,
            NumberBytesHeader = 4,
            NumberBytesData = (byte)message.Data.Length,
         };
         if (message.Data.Length > 0) vcMessage.Data1 = message.Data[0];
         if (message.Data.Length > 1) vcMessage.Data2 = message.Data[1];
         if (message.Data.Length > 2) vcMessage.Data3 = message.Data[2];
         if (message.Data.Length > 3) vcMessage.Data4 = message.Data[3];
         if (message.Data.Length > 4) vcMessage.Data5 = message.Data[4];
         if (message.Data.Length > 5) vcMessage.Data6 = message.Data[5];
         if (message.Data.Length > 6) vcMessage.Data7 = message.Data[6];
         if (message.Data.Length > 7) vcMessage.Data8 = message.Data[7];
         _vc.SendPacket(vcMessage);
      }

      private ValueCan _vc;
   }

   internal class ValueCan : IDisposable
   {
      public ValueCan()
      {
         var device = new NeoDevice();
         var device_count = 1;
         if (icsNeoDll.icsneoFindNeoDevices((uint)eHardwareTypes.NEODEVICE_ALL, ref device, ref device_count) != 1)
         {
            PrintLastError();
            throw new Exception("Failed to search for ICS devices");
         }
         Console.WriteLine("Found {0} ICS devices.  Device type: {1}", device_count, device.DeviceType);

         _handle = device.Handle;
         var networks = new byte[64]; //List of hardware IDs
         networks = Enumerable.Range(0, 63).Select(i => Convert.ToByte(i)).ToArray();

         //Open the first found device, ndNeoToOpen acquired from FindNeoDevices 
         var result = icsNeoDll.icsneoOpenNeoDevice(ref device, ref _handle, ref networks[0], 1, 0);
         if (result != 1)
         {
            PrintLastError();
            throw new Exception("Failed to connect to ValueCan3");
         }
      }

      /// <summary>
      /// Gets all messages from the device, and stores them in a thread safe collection for row units to consume
      /// </summary>
      public IEnumerable<CanMessage> Read()
      {
         var messages = new icsSpyMessage[128];
         var msgCount = 0;
         var errorCount = 0;
         var result = icsNeoDll.icsneoGetMessages(_handle, ref messages[0], ref msgCount, ref errorCount);
         if (errorCount > 0)
            Console.WriteLine("Error");
         var canMessages = from m in messages
                           where m.ArbIDOrHeader != 0x00
                           select new CanMessage()
                           {
                              Header = (uint)m.ArbIDOrHeader,
                              Data = new byte[] { m.Data1, m.Data2, m.Data3, m.Data4, m.Data5, m.Data6, m.Data7, m.Data8 },
                           };
         return canMessages.Take(msgCount);
      }

      private void PrintLastError()
      {
         uint errorNumber = 0;
         if (icsNeoDll.icsneoGetLastAPIError(_handle, ref errorNumber) == 1)
         {
            Console.WriteLine("Error code: {0}", errorNumber);
         }
      }

      public void SendPacket(icsSpyMessage message)
      {
         var result = icsNeoDll.icsneoTxMessages(_handle, ref message, _canBus, 1);
         if (result == 0)
         {
            //Console.WriteLine("Error sending packet");
            PrintLastError();
         }
      }

      public void Dispose()
      {
         var errorCount = 0;
         if (icsNeoDll.icsneoClosePort(_handle, ref errorCount) != 1)
         {
            throw new Exception("Failed to close ValueCan3");
         }
      }

      private Int32 _handle;
      private readonly int _canBus = 1;
   }
}
