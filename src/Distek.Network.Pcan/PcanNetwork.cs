﻿using Peak.Can.Basic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Distek.Network.Pcan
{
   public class PcanNetwork : INetwork
   {
      string INetwork.Key => "pcan";

      void INetwork.Connect()
      {
         var res = PCANBasic.Initialize(PCANBasic.PCAN_USBBUS1, TPCANBaudrate.PCAN_BAUD_250K);
         Console.WriteLine("init response: {0}", res);
         var s = (uint)PCANBasic.PCAN_PARAMETER_ON;
         PCANBasic.SetValue(PCANBasic.PCAN_USBBUS1, TPCANParameter.PCAN_BUSOFF_AUTORESET, ref s, 1);
      }

      private Action<CanMessage> _handler;

      void INetwork.Receive(Action<CanMessage> handler)
      {
         _handler = handler;
         _receiveEvent = new System.Threading.AutoResetEvent(false);

         var readThread = new System.Threading.Thread(() => Read(handler));
         readThread.IsBackground = true;
         readThread.Start();
      }

      private void Read(Action<CanMessage> handler)
      {
         UInt32 iBuffer;
         TPCANStatus stsResult;

         iBuffer = Convert.ToUInt32(_receiveEvent.SafeWaitHandle.DangerousGetHandle().ToInt32());

         stsResult = PCANBasic.SetValue(PCANBasic.PCAN_USBBUS1, TPCANParameter.PCAN_RECEIVE_EVENT, ref iBuffer, sizeof(UInt32));

         if (stsResult != TPCANStatus.PCAN_ERROR_OK)
         {
            Console.WriteLine("Failed to read from PCAN adapter");
            return;
         }

         while (true)
         {
            // Waiting for Receive-Event
            if (_receiveEvent.WaitOne(50))
            {
               TPCANMsg CANMsg;
               TPCANTimestamp CANTimeStamp;

               // Read all available messages
               do
               {
                  stsResult = PCANBasic.Read(PCANBasic.PCAN_USBBUS1, out CANMsg, out CANTimeStamp);
                  if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                  {
                     // We process the received message
                     //
                     var msg = new CanMessage(CANMsg.ID, CANMsg.DATA);
                     handler(msg);
                  }
               } while (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY);
            }
         }
      }

      void INetwork.Send(CanMessage message)
      {
         var msg = new TPCANMsg();
         msg.ID = message.Header;
         msg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED;
         // Keep the DLC as the value it was before we start padding the data
         msg.LEN = (byte)message.Data.Length;
         // pad message if necessary - the pcan pinvoke layer requires this
         if(message.Data.Length < 8)
         {
            message.Data = message.Data.Concat(Enumerable.Repeat<byte>(0xFF, 8 - message.Data.Length)).ToArray();
         }
         msg.DATA = message.Data;
         var status = PCANBasic.Write(PCANBasic.PCAN_USBBUS1, ref msg);
         if (status != TPCANStatus.PCAN_ERROR_OK)
         {
            Console.WriteLine("Failed to write to PCAN {0}", status);
         }
      }

      private System.Threading.AutoResetEvent _receiveEvent;
   }
}
